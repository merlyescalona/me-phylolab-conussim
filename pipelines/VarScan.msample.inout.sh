#!/bin/bash

# Testing multiple sample SAM/BAM to be introduced in VarScan
module load samtools
module load jdk-oracle/1.8.0


st=1
gt=001
ind=1
RGID="000${ind}"
SMID="S.${st}-G.${gt}-I.${ind}"

reference="$referencesFolder/REF_${prefixRef}_${prefixLoci}_${st}_${gt}.fasta"
infile1="$readsFolder/fq/${st}/${gt}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}_"

# Mapping 1 sample
bwa mem -t 1 -R "@RG\tID:${RGID}\tSM:${SMID}\tPL:Illumina\tLB:${RGID}\tPU:HiSeq2500" ${ref} ${infile1}R1.fq ${infile1}R2.fq >test.sam

ind=0
SMID="S.${st}-G.${gt}-I.${ind}"
RGID="000${ind}"
infile1="$readsFolder/fq/${st}/${gt}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}_"
# Mapping 2 sample
bwa mem -t 1 -R "@RG\tID:${RGID}\tSM:${SMID}\tPL:Illumina\tLB:${RGID}\tPU:HiSeq2500" ${ref} ${infile1}R1.fq ${infile1}R2.fq > test0.sam

# merging maps
java -Xmx4G -jar $PICARD MergeSamFiles I=test.sam I=test0.sam O=merged.v2.sam

# Sorting
samtools view -bSh merged.v2.sam | samtools sort - merged.v2.sorted

# index
samtools index merged.v2.sorted.bam


echo -e "0001\n0000" >test1.names # RG names
echo -e "S.1-G.001-I.1\nS.1-G.001-I.0" >test0.names # Sample Names (SM)

# Varcalling
samtools mpileup -D -I -f $reference merged.v2.sorted.bam | java -jar $VarScan2 mpileup2snp --output-vcf --vcf-sample-list test1.names > test1.v2.vcf
samtools mpileup -D -I -f $reference merged.v2.sorted.bam | java -jar $VarScan2 mpileup2snp --output-vcf --vcf-sample-list test0.names > test2.v2.vcf
samtools mpileup -D -I -f $reference merged.v2.sorted.bam | java -jar $VarScan2 mpileup2snp  > test.v2.txt


 diff test1.vcf test0.vcf
<<RES
24c24
< #CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	0001	0000
---
> #CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	S.1-G.001-I.1	S.1-G.001-I.0
RES


cat test.v2.txt
<<RES
RES



echo "0001" >RG1.txt
samtools view -bShR RG1.txt merged.v2.sam | samtools sort - mergedrg1.sorted
echo "0000" >RG2.txt
samtools view -bShR RG2.txt merged.v2.sam | samtools sort - mergedrg2.sorted
samtools mpileup -D -I -f $reference mergedrg1.sorted mergedrg2.sorted | java -jar $VarScan2 mpileup2snp --output-vcf  > DIFF.vcf
