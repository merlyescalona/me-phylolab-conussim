<<NOTES
TODO: Need to pay attention to the number of digits the number of replicates has
so the number are written properly
NOTES
<<TESTCODE
# jobID=$(qsub -pe threaded 4 $job | awk '{ print $3}')
# monitor <outputFile> <jobID/name> 0
# $monitor $WD/usage/conusSim.$pipelinesName.1.usage $jobID1 0 &
# INDELIble_wrapper.pl <output_folder_from_simphy> <configuration_file> <seed> <numberOfCores>`
TESTCODE
################################################################################
# Directories
################################################################################
WD="/home/merly/conusSim"
monitor="/home/merly/src/usage_monitorization.sh"
wrapper="/home/merly/src/INDELIble_wrapper.pl"
filterEven="/home/merly/src/conusSim.filtering.even.R"
filterEven2="/home/merly/src/conusSim.filtering.trees.even.inds.sh"
matingProgram="/home/merly/src/me-phylolab-mating/mating.py"
pipelineBash="/home/merly/src/me-phylolab-conussim/conusSim-pipeline"
LociFiltering="/home/merly/src/me-phylolab-mating/LociFiltering.py"
profilePath="/home/merly/conusSim/csNGSProfile"
pair1Path="/home/merly/conusSim/csNGSProfile/pair1"
pair2Path="/home/merly/conusSim/csNGSProfile/pair2"
################################################################################
# Naming Variables
################################################################################
pipelinesName="csBullet"
stReplicates=10
numDigits=${#stReplicates}
numGeneTrees=1750
numDigitsGTs=${#numGeneTrees}
prefix="LOC"
jobsSent="$WD/output/$pipelinesName/$pipelinesName.jobs"
################################################################################
# SimPhy
################################################################################
echo -e "
#! /bin/bash
#$ -m bea
#$ -o /home/merly/conusSim/output/conusSim.$pipelinesName.1.simphy.o.txt
#$ -e /home/merly/conusSim/output/conusSim.$pipelinesName.1.simphy.e.txt
#$ -N $pipelinesName.1

cd $WD

module load bio/simphy/1.0.0

simphy -rs $stReplicates -rl f:1750 -sb l:-15,1 -st u:2000000,20000000 -sl u:3,20 -so f:1 -sp f:100000 -su e:10000000 -si u:2,21 -hh l:1.2,1 -hl l:1.4,1 -hg f:200 -v 1 -o $pipelinesName -cs $RANDOM -om 1 -od 1 -op 1 -oc 1 -on 1

module unload  bio/simphy/1.0.0" > $WD/scripts/conusSim.$pipelinesName.1.sh

jobID=$(qsub $WD/scripts/conusSim.$pipelinesName.1.sh | awk '{ print $3}')
# monitor <outputFile> <jobID/name> 0
bash $monitor $WD/usage/conusSim.$pipelinesName.1.usage $jobID &

# ------------------------------------------------------------------------------
echo "Step    JobID" > $jobsSent
echo "$pipelinesName"".1    $jobID" >> $jobsSent
ls -Rl $WD/$pipelinesName > $WD/$pipelinesName/$pipelinesName.1.files

################################################################################
# INDELible wrapper
################################################################################
# After the running of SimPhy, it is necessary to run the INDELIble_wrapper
# to obtain the control files for INDELible. Since, is not possible to
# run it for all the configurations, it is necessary to modify the name of the
# output files in order to keep track of every thing
################################################################################
echo -e "
#! /bin/bash
#$ -m bea
#$ -o /home/merly/conusSim/output/conusSim.$pipelinesName.2.wrapper.o.txt
#$ -e /home/merly/conusSim/output/conusSim.$pipelinesName.2.wrapper.e.txt
#$ -N $pipelinesName.2

cd $WD
controlFolder=\"/home/merly/conusSim/controlFiles\"
c500=\"\$controlFolder/INDELible_csHAE500_control.txt\"
c1000=\"\$controlFolder/INDELible_csHAE1000_control.txt\"
c2000=\"\$controlFolder/INDELible_csHAE2000_control.txt\"

simphyOutput=\"$WD/$pipelinesName\"
mkdir \"$WD/output/$pipelinesName/\"

$wrapper \$simphyOutput \$c500 $RANDOM 1 &> $WD/output/$pipelinesName/wrapper.500.output
for item in \$(seq -f \"%0$numDigits""g\" 1 $stReplicates); do
  mv \$simphyOutput/\$item/control.txt \$simphyOutput/\$item/control500.txt
done

$wrapper \$simphyOutput \$c1000 $RANDOM 1 &> $WD/output/$pipelinesName/wrapper.1000.output
for item in \$(seq -f \"%0$numDigits""g\" 1 $stReplicates); do
  mv \$simphyOutput/\$item/control.txt \$simphyOutput/\$item/control1000.txt
done

$wrapper \$simphyOutput \$c2000 $RANDOM 1 &> $WD/output/$pipelinesName/wrapper.2000.output
for item in \$(seq -f \"%0$numDigits""g\" 1 $stReplicates); do
  mv \$simphyOutput/\$item/control.txt \$simphyOutput/\$item/control2000.txt
done

module unload perl/5.20.1 bio/indelible/1.03 bio/simphy/1.0.0
" >  $WD/scripts/conusSim.$pipelinesName.2.sh

jobID=$(qsub $WD/scripts/conusSim.$pipelinesName.2.sh | awk '{ print $3}')
bash $monitor $WD/usage/conusSim.$pipelinesName.2.usage $jobID

# ------------------------------------------------------------------------------

echo "$pipelinesName"".2    $jobID" >> $jobsSent
ls -Rl $WD/$pipelinesName > $WD/$pipelinesName/$pipelinesName.2.files


################################################################################
# Filtering species tree replicates with even number of individuals per species
################################################################################
echo -e "
#! /bin/bash
#$ -m bea
#$ -o /home/merly/conusSim/output/conusSim.$pipelinesName.3.filterEven.o.txt
#$ -e /home/merly/conusSim/output/conusSim.$pipelinesName.3.filterEven.e.txt
#$ -N $pipelinesName.3

cd $WD/$pipelinesName
module load R/3.2.2
Rscript $filterEven $pipelinesName.db $WD/$pipelinesName/$pipelinesName.evens $WD/$pipelinesName/$pipelinesName.odds
module unload R/3.2.2
">  $WD/scripts/conusSim.$pipelinesName.3.sh

jobID=$(qsub $WD/scripts/conusSim.$pipelinesName.3.sh | awk '{ print $3}')
bash $monitor $WD/usage/conusSim.$pipelinesName.3.usage $jobID &

echo "$pipelinesName"".3    $jobID" >> $jobsSent
ls -Rl $WD/$pipelinesName > $WD/$pipelinesName/$pipelinesName.3.files

# ------------------------------------------------------------------------------
cd $WD/$pipelinesName
mkdir -p $WD/$pipelinesName/even $WD/$pipelinesName/odd
bash $filterEven2 $WD/$pipelinesName  $WD/$pipelinesName/$pipelinesName.even.ids $stReplicates



################################################################################
# Individual INDELible calls
################################################################################
sizes=("500bp" "1000bp" "2000bp")
mkdir -p $WD/scripts/$pipelinesName/indelible

for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
echo -e "
#! /bin/bash
#$ -o $WD/output/$pipelinesName.4.$st.o
#$ -e $WD/output/$pipelinesName.4.$st.e
#$ -N $pipelinesName"".$st

module load  bio/indelible/1.03
cd $WD/$pipelinesName/$st
indelible
module unload  bio/indelible/1.03
" >  $WD/scripts/$pipelinesName/indelible/conusSim.$pipelinesName.4.$st.sh
done


################################################################################
# Running indelible for 500bp loci
#-------------------------------------------------------------------------------
for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  rm $WD/$pipelinesName/$st/control.txt
  ln -s $WD/$pipelinesName/$st/control500.txt $WD/$pipelinesName/$st/control.txt
done
for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  jobID=$(qsub $WD/scripts/$pipelinesName/indelible/conusSim.$pipelinesName.4.$st.sh | awk '{ print $3}')
  echo "$pipelinesName"".4.$st.$item    $jobID" >> $jobsSent
  bash $monitor $WD/usage/conusSim.$pipelinesName.4.$st.500bp.usage $jobID &
done
ls -Rl $WD/$pipelinesName > $WD/$pipelinesName/$pipelinesName.4.500bp.files
# Running indelible for 1000bp loci
#-------------------------------------------------------------------------------
for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  mv $WD/$pipelinesName/$st/LOG.txt $WD/$pipelinesName/$st/LOG500.txt
  mv $WD/$pipelinesName/$st/trees.txt $WD/$pipelinesName/$st/trees500.txt
  st=$(printf "%0$numDigits""g" $line)
  rm $WD/$pipelinesName/$st/control.txt
  ln -s $WD/$pipelinesName/$st/control1000.txt $WD/$pipelinesName/$st/control.txt
done
for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  jobID=$(qsub $WD/scripts/$pipelinesName/indelible/conusSim.$pipelinesName.4.$st.sh | awk '{ print $3}')
  echo "$pipelinesName"".4.$st.$item    $jobID" >> $jobsSent
  bash $monitor $WD/usage/conusSim.$pipelinesName.4.$st.1000bp.usage $jobID &
done
ls -Rl $WD/$pipelinesName > $WD/$pipelinesName/$pipelinesName.4.1000bp.files
# Running indelible for 2000bp loci
#-------------------------------------------------------------------------------
for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  mv $WD/$pipelinesName/$st/LOG.txt $WD/$pipelinesName/$st/LOG1000.txt
  mv $WD/$pipelinesName/$st/trees.txt $WD/$pipelinesName/$st/trees1000.txt
  st=$(printf "%0$numDigits""g" $line)
  rm $WD/$pipelinesName/$st/control.txt
  ln -s $WD/$pipelinesName/$st/control2000.txt $WD/$pipelinesName/$st/control.txt
done

for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  jobID=$(qsub $WD/scripts/$pipelinesName/indelible/conusSim.$pipelinesName.4.$st.sh | awk '{ print $3}')
  echo "$pipelinesName"".4.$st.$item    $jobID" >> $jobsSent
  bash $monitor $WD/usage/conusSim.$pipelinesName.4.$st.2000bp.usage $jobID &
done
ls -Rl $WD/$pipelinesName > $WD/$pipelinesName/$pipelinesName.4.2000bp.files

for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  mv $WD/$pipelinesName/$st/LOG.txt $WD/$pipelinesName/$st/LOG2000.txt
  mv $WD/$pipelinesName/$st/trees.txt $WD/$pipelinesName/$st/trees2000.txt
done
cd $WD

################################################################################
# Generate file for filtering/generating the dataset
################################################################################
fileFiltering="$WD/$pipelinesName/$pipelinesName.dataset.csv"
for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  # directory1,prefix1,1000,10,finalprefix,outputdir1 (indexes will be 1-10)
  echo "$WD/$pipelinesName/$st,data,1750,1000,LOC,$WD/$pipelinesName/$st" >> $fileFiltering
done
for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  # directory1,prefix1,1000,10,finalprefix,outputdir1 (indexes will be 1-10)
  echo "$WD/$pipelinesName/$st,dataK,1750,500,LOC,$WD/$pipelinesName/$st" >> $fileFiltering
done
for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  # directory1,prefix1,1000,10,finalprefix,outputdir1 (indexes will be 1-10)
  echo "$WD/$pipelinesName/$st,data2K,1750,250,LOC,$WD/$pipelinesName/$st" >> $fileFiltering
done


################################################################################
# Running filtering, renaming and selection of loci
################################################################################
echo -e "
#! /bin/bash
#$ -m bea
#$ -o $WD/output/$pipelinesName/$pipelinesName.5.dataset.o
#$ -e $WD/output/$pipelinesName/$pipelinesName.5.dataset.e
#$ -N $pipelinesName.filter

module load python/2.7.8
cd $WD/$pipelinesName/

python $LociFiltering -i $fileFiltering -o $WD/$pipelinesName/$pipelinesName.new.dataset


module unload python/2.7.8
" >  $WD/scripts/$pipelinesName/conusSim.$pipelinesName.5.sh

jobID=$(qsub  $WD/scripts/$conusSim.$pipelinesName.5.sh | awk '{ print $3}')
bash $monitor $WD/usage/conusSim.$pipelinesName.5.usage $jobID &

ls -Rl $WD/$pipelinesName > $WD/$pipelinesName/$pipelinesName.5.datasets.files


################################################################################
# Mating
################################################################################

echo -e "
#! /bin/bash
#$ -m bea
#$ -o $WD/output/$pipelinesName/$pipelinesName.6.mating.o
#$ -e $WD/output/$pipelinesName/$pipelinesName.6.mating.e
#$ -N $pipelinesName.mating

module load python/2.7.8
cd $WD/$pipelinesName/

python $matingProgram -p $prefix -sf $WD/$pipelinesName/ -l DEBUG

module unload python/2.7.8
" >  $WD/scripts/$pipelinesName/conusSim.$pipelinesName.6.sh

jobID=$(qsub  $WD/scripts/$pipelinesName/conusSim.$pipelinesName.6.sh | awk '{ print $3}')
bash $monitor $WD/usage/conusSim.$pipelinesName.6.usage $jobID &


ls -Rl $WD/$pipelinesName > $WD/$pipelinesName/$pipelinesName.6.files

<<REMOVING_DIRS
for item in $(seq -f "%0$numDigits""g" 1 2000); do
  echo $item;
  rm <filaname>
done
REMOVING_DIRS


################################################################################
# NGS generation
################################################################################
# Set up data for profile
scp merly@dbxb5:/mnt/sdb1/merly/conus-fsu-rawdata/Posada1/Project_H7NRGADXX/Sample_lane1/* \
    merly@diploid:/home/merly/conusSim/csNGSProfile

numFiles=31
nDigitsProfile=3

mkdir /home/merly/conusSim/csNGSProfile
mkdir /home/merly/conusSim/csNGSProfile/pair1 /home/merly/conusSim/csNGSProfile/pair2
cd $profilePath
for item in $(seq -f "%0$nDigitsProfile""g" 1 $numFiles); do
  echo $item;
  mv "lane1_NoIndex_L001_R1_$item"".fastq.gz" "$pair1Path/L001_$item""_1.fq.gz"
  mv "lane1_NoIndex_L001_R2_$item"".fastq.gz" "$pair2Path/L001_$item""_2.fq.gz"
done

<<CHECKING_Renaming
ls $profilePath
-------------------------------------------------------------------------------
./pair1:
L001_001_1.fq.gz  L001_007_1.fq.gz  L001_013_1.fq.gz  L001_019_1.fq.gz  L001_025_1.fq.gz  L001_031_1.fq.gz
L001_002_1.fq.gz  L001_008_1.fq.gz  L001_014_1.fq.gz  L001_020_1.fq.gz  L001_026_1.fq.gz
L001_003_1.fq.gz  L001_009_1.fq.gz  L001_015_1.fq.gz  L001_021_1.fq.gz  L001_027_1.fq.gz
L001_004_1.fq.gz  L001_010_1.fq.gz  L001_016_1.fq.gz  L001_022_1.fq.gz  L001_028_1.fq.gz
L001_005_1.fq.gz  L001_011_1.fq.gz  L001_017_1.fq.gz  L001_023_1.fq.gz  L001_029_1.fq.gz
L001_006_1.fq.gz  L001_012_1.fq.gz  L001_018_1.fq.gz  L001_024_1.fq.gz  L001_030_1.fq.gz
./pair2:
L001_001_2.fq.gz  L001_007_2.fq.gz  L001_013_2.fq.gz  L001_019_2.fq.gz  L001_025_2.fq.gz  L001_031_2.fq.gz
L001_002_2.fq.gz  L001_008_2.fq.gz  L001_014_2.fq.gz  L001_020_2.fq.gz  L001_026_2.fq.gz
L001_003_2.fq.gz  L001_009_2.fq.gz  L001_015_2.fq.gz  L001_021_2.fq.gz  L001_027_2.fq.gz
L001_004_2.fq.gz  L001_010_2.fq.gz  L001_016_2.fq.gz  L001_022_2.fq.gz  L001_028_2.fq.gz
L001_005_2.fq.gz  L001_011_2.fq.gz  L001_017_2.fq.gz  L001_023_2.fq.gz  L001_029_2.fq.gz
L001_006_2.fq.gz  L001_012_2.fq.gz  L001_018_2.fq.gz  L001_024_2.fq.gz  L001_030_2.fq.gz
-------------------------------------------------------------------------------
CHECKING_Renaming
# Generating profile
pairsInfo=(1 2)
for item in ${pairsInfo[*]}; do
echo -e "
#! /bin/bash
#$ -m bea
#$ -o $WD/output/$pipelinesName.6.$item.profile.o
#$ -e $WD/output/$pipelinesName.6.$item.profile.e
#$ -N $pipelinesName.A.P.$item

module load perl/5.20.1 bio/art/030915
cd $profilePath

 art_profiler_illumina csNGSProfile_hiseq2500_$item $profilePath/pair$item fq.gz

module unload perl/5.20.1 bio/art/030915
">   $WD/scripts/$pipelinesName/conusSim.$pipelinesName.6.pair$item.sh
done

jobID=$(qsub -pe threaded 8 -l h=compute-0-30 $WD/scripts/$pipelinesName/conusSim.$pipelinesName.6.pair1.sh | awk '{ print $3}')
bash $monitor $WD/scripts/$pipelinesName/conusSim.$pipelinesName.6.pair1.usage  $jobID &
jobID=$(qsub -pe threaded 8 -l h=compute-0-30 $WD/scripts/$pipelinesName/conusSim.$pipelinesName.6.pair2.sh | awk '{ print $3}')
bash $monitor $WD/scripts/$pipelinesName/conusSim.$pipelinesName.6.pair2.usage  $jobID &

# -------------------------------------------------------------------------------
# Sequencing
mkdir $WD/$pipelinesName/reads
prefix="LOC"
numGeneTrees=1750
mkdir $WD/scripts/$pipelinesName/art/
for line in $(tail -n+2  $WD/$pipelinesName/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  inputFileS1="${pipelinesName}_${st}_${gt}_${prefix}_${array[2]}_${array[3]}_0_${array[4]}_S1"
  inputFileS2="${pipelinesName}_${st}_${gt}_${prefix}_${array[2]}_${array[3]}_0_${array[5]}_S2"
  echo "$st/$gt/${array[2]}"

echo -e "#! /bin/bash
#$ -m bea
#$ -o $WD/output/$pipelinesName/$pipelinesName.7.$st.$gt.S1.reads.o
#$ -e $WD/output/$pipelinesName/$pipelinesName.7.$st.$gt.S1.reads.e
#$ -N A.$st.$gt.1

module load perl/5.20.1 bio/art/030915

mkdir -p $WD/$pipelinesName/reads/$st/$gt/
art_illumina -sam  -1 $profilePath/csNGSProfile_hiseq2500_1.txt -2 $profilePath/csNGSProfile_hiseq2500_2.txt -amp -f 100 -l 150 -p -m 250 -s 50 -rs $RANDOM -ss HS25  -i $WD/$pipelinesName/individuals/$st/$gt/$inputFileS1"".fasta -o $WD/$pipelinesName/reads/$st/$gt/$inputFileS1.R

module unload perl/5.20.1 bio/art/030915
">   $WD/scripts/$pipelinesName/art/conusSim.$pipelinesName.7.reads.$st.$gt.S1.sh

echo -e "#! /bin/bash
#$ -m bea
#$ -o $WD/output/$pipelinesName/$pipelinesName.7.$st.$gt.S2.reads.o
#$ -e $WD/output/$pipelinesName/$pipelinesName.7.$st.$gt.S2.reads.e
#$ -N A.$st.$gt.2

module load perl/5.20.1 bio/art/030915

mkdir -p $WD/$pipelinesName/reads/$st/$gt/
art_illumina -sam  -1 $profilePath/csNGSProfile_hiseq2500_1.txt -2 $profilePath/csNGSProfile_hiseq2500_2.txt -amp -f 50 -l 150 -p -m 250 -s 50 -rs $RANDOM -ss HS25  -i $WD/$pipelinesName/individuals/$st/$gt/$inputFileS2"".fasta -o $WD/$pipelinesName/reads/$st/$gt/$inputFileS2.R

module unload perl/5.20.1 bio/art/030915
">   $WD/scripts/$pipelinesName/art/conusSim.$pipelinesName.7.reads.$st.$gt.S2.sh
done


for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  echo $st
  for gt in $(seq -f "%0$numDigitsGTs""g" 1 $numGeneTrees); do
    jobID=$(qsub $WD/scripts/$pipelinesName/conusSim.$pipelinesName.7.reads.$st.$gt.sh | awk '{ print $3}')
    bash $monitor $WD/scripts/$pipelinesName/conusSim.$pipelinesName.7.reads.$st.$gt.usage  $jobID &
done
done
<<SINGLEQUEUE
    jobID=$(qsub $WD/scripts/$pipelinesName/art/conusSim.$pipelinesName.7.reads.01.0001.S1.sh | awk '{ print $3}')
    bash $monitor $WD/scripts/$pipelinesName/art/conusSim.$pipelinesName.7.reads.01.0001.S1.usage  $jobID &
SINGLEQUEUE

################################################################################
# Reporting
################################################################################

<<RENAMING
for isize  in ${sizes[*]}; do
  for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
    st=$(printf "%0$numDigits""g" $line)
    mv conusSim.csBullet.4.$st.01.$isize.usage conusSim.csBullet.4.$st.$isize.usage
  done
done
RENAMING
for index in 1 2 3 5 6; do
  echo "round,date,cpu, mem, io, vmem, maxvmem" >  $WD/usage/conusSim.$pipelinesName.$index.post.usage
  cat $WD/usage/conusSim.$pipelinesName.usage  | tr '[:upper:]' '[:lower:]' | sed 's/n\/a/ /g' | awk -F "," '{ if (NF < 7)   print $1,",",$2,",",$3,",  ,  ,  ,  "; else   print $1,",",$2,",",$3,",",$4,",",$5,",",$6; }' >> $WD/usage/conusSim.$pipelinesName.$index.post.usage
done
for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for elem in ${sizes[*]}; do
    echo "round,date,cpu, mem, io, vmem, maxvmem" >  $WD/usage/conusSim.$pipelinesName.4.$st.$elem.post.usage
    $WD/usage/conusSim.$pipelinesName.4.$st.$elem.usage  | tr '[:upper:]' '[:lower:]' | sed 's/n\/a/ /g' | awk -F "," '{ if (NF < 7)   print $1,",",$2,",",$3,",  ,  ,  ,  "; else   print $1,",",$2,",",$3,",",$4,",",$5,",",$6; }' >> $WD/usage/conusSim.$pipelinesName.4.$st.$elem.post.usage
  done
done

for index in 1 2 3 5 6; do
  cat $WD/$pipelinesName/$pipelinesName.$index.files | awk '{print $9,$5}' > conusSim.$pipelinesName.$index.post.files
done


# ------------------------------------------------------------------------------
# Reporting mating DB
cat $WD/$pipelinesName/$pipelinesName.mating| awk -F "," 'NR==2{print "|---------|---------|-----------|---------|---------|"}{print "|",$1,"|",$2,"|",$3,"|",$4,"|",$5,"|"}' > $WD/$pipelinesName/report/$pipelinesName.mating.Rmd


# ------------------------------------------------------------------------------
# Reporting Loci to be filtered
echo "| Old folder | Old Prefix | Total Number of Loci for this prefix | Number of loci to take| New Prefix | New Folder |" > $WD/$pipelinesName/report/$pipelinesName.tofilter.dataset.Rmd
cat $WD/$pipelinesName/$pipelinesName.dataset.csv| awk -F "," 'NR==1{print "|---------|---------|-----------|---------|---------|---------|"}{print "|",$1,"|",$2,"|",$3,"|",$4,"|",$5,"|",$6,"|"}' >> $WD/$pipelinesName/report/$pipelinesName.tofilter.dataset.Rmd
# Reporting filtered loci
cat $WD/$pipelinesName/$pipelinesName.new.dataset | awk -F "," 'NR==2{print "|---------|---------|---------|-----------|---------|---------|"}{print "|",$1,"|",$2,"|",$3,"|",$4,"|",$5,"|",$6,"|"}' > $WD/$pipelinesName/report/$pipelinesName.filtered.dataset.Rmd


# ------------------------------------------------------------------------------
# Reporting Control Files for Indelible
controlFolder="/home/merly/conusSim/controlFiles"
c500="$controlFolder/INDELible_csHAE500_control.txt"
c1000="$controlFolder/INDELible_csHAE1000_control.txt"
c2000="$controlFolder/INDELible_csHAE2000_control.txt"

echo "### Control File (Partitions for 500bp sequences)" > $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
echo "\`\`\`" >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
cat $c500 >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
echo "\`\`\`" >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
echo "### Control File (Partitions for 1000bp sequences)" >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
echo "\`\`\`" >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
cat $c1000 >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
echo "\`\`\`" >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
echo "### Control File (Partitions for 2000bp sequences)" >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
echo "\`\`\`" >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
cat $c2000 >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
echo "\`\`\`" >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd

# ------------------------------------------------------------------------------
# Reporting Pipeline
echo "\`\`\`" > $pipelineBash.Rmd
cat $pipelineBash.sh >> $pipelineBash.Rmd
echo "\`\`\`" >> $pipelineBash.Rmd
