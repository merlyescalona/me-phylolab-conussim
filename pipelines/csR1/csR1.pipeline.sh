<<NOTES
TODO: Need to pay attention to the number of digits the number of replicates has
so the number are written properly
NOTES
################################################################################
# Generic Directories
################################################################################
WD="/home/merly/conusSim"
monitor="/home/merly/src/usage_monitorization.sh"
wrapper="/home/merly/src/INDELIble_wrapper.pl"
filterEven="/home/merly/src/conusSim.filtering.even.R"
filterEven2="/home/merly/src/conusSim.filtering.trees.even.inds.sh"
matingProgram="/home/merly/src/me-phylolab-mating/mating.py"
pipelineBash="/home/merly/src/me-phylolab-conussim/conusSim-pipeline"
LociFiltering="/home/merly/src/me-phylolab-mating/LociFiltering.py"
lociReferenceSelection="/home/merly/src/me-phylolab-conussim/LociRefSelection/LociReferenceSelection.py"
profilePath="/home/merly/conusSim/csNGSProfile"
pair1Path="/home/merly/conusSim/csNGSProfile/pair1"
pair2Path="/home/merly/conusSim/csNGSProfile/pair2"
################################################################################
# Naming Variables
################################################################################
pipelinesName="csR1"
stReplicates=1
numDigits=${#stReplicates}
numGeneTrees=500
numDigitsGTs=${#numGeneTrees}
sizeLoci="1000bp"
prefix="LOC"
################################################################################
outputFolder="$WD/$pipelinesName/report/output"
usageFolder="$WD/$pipelinesName/report/usage"
scriptsFolder="$WD/$pipelinesName/report/scripts"
filesFolder="$WD/$pipelinesName/report/files"
controlFolder="$WD/$pipelinesName/report/controlFiles"
jobsSent="$outputFolder/$pipelinesName.jobs"
################################################################################
# SimPhy
################################################################################
echo -e "#! /bin/bash
#$ -m bea
#$ -o $outputFolder/$pipelinesName.1.o
#$ -e $outputFolder/$pipelinesName.1.e
#$ -N $pipelinesName.1

cd $WD

module load bio/simphy/1.0.0

simphy -rs $stReplicates -rl f:$numGeneTrees -sb l:-15,1 -st u:2000000,20000000 -sl u:3,20 -so f:1 -sp f:100000 -su e:10000000 -si f:20 -hh l:1.2,1 -hl l:1.4,1 -hg f:200 -v 1 -o $pipelinesName -cs $RANDOM -om 1 -od 1 -op 1 -oc 1 -on 1

module unload  bio/simphy/1.0.0" > $scriptsFolder/$pipelinesName.1.sh

jobID=$(qsub $scriptsFolder/$pipelinesName.1.sh | awk '{ print $3}')
echo "Step    JobID" > $jobsSent
echo "$pipelinesName"".1    $jobID" >> $jobsSent
# monitor <outputFile> <jobID/name> 0
bash $monitor $usageFolder/$pipelinesName.1.usage $jobID &

ls -Rl $WD/$pipelinesName > $filesFolder/$pipelinesName.1.files

################################################################################
# INDELible wrapper
################################################################################
# After the running of SimPhy, it is necessary to run the INDELIble_wrapper
# to obtain the control files for INDELible. Since, is not possible to
# run it for all the configurations, it is necessary to modify the name of the
# output files in order to keep track of every thing
################################################################################
echo -e "#! /bin/bash
#$ -m bea
#$ -o $outputFolder/$pipelinesName.2.o
#$ -e $outputFolder/$pipelinesName.2.e
#$ -N $pipelinesName.2

cd $WD
controlFile=$controlFolder/$pipelinesName.indelible.control.txt

$wrapper $WD/$pipelinesName \$controlFile $RANDOM 1 &> $outputFolder/$pipelinesName.2.wrapper.txt
for item in \$(seq -f \"%0$numDigits""g\" 1 $stReplicates); do
  cp $WD/$pipelinesName/\$item/control.txt $WD/$pipelinesName/\$item/control500.txt
done

module unload perl/5.20.1 bio/indelible/1.03 bio/simphy/1.0.0
" >  $scriptsFolder/$pipelinesName.2.sh

jobID=$(qsub $scriptsFolder/$pipelinesName.2.sh | awk '{ print $3}')
echo "$pipelinesName"".2   $jobID" >> $jobsSent
# monitor <outputFile> <jobID/name> 0
bash $monitor $usageFolder/$pipelinesName.2.usage $jobID &

ls -Rl $WD/$pipelinesName > $filesFolder/$pipelinesName.2.files

################################################################################
# Filtering species tree replicates with even number of individuals per species
################################################################################
echo -e "#! /bin/bash
#$ -m bea
#$ -o $outputFolder/$pipelinesName.3.o
#$ -e $outputFolder/$pipelinesName.3.e
#$ -N $pipelinesName.3

cd $WD/$pipelinesName
module load R/3.2.2
Rscript $filterEven $pipelinesName.db $WD/$pipelinesName/$pipelinesName.evens $WD/$pipelinesName/$pipelinesName.odds
module unload R/3.2.2
">  $scriptsFolder/$pipelinesName.3.sh

jobID=$(qsub $scriptsFolder/$pipelinesName.3.sh | awk '{ print $3}')
echo "$pipelinesName"".3   $jobID" >> $jobsSent
# monitor <outputFile> <jobID/name> 0
bash $monitor $usageFolder/$pipelinesName.3.usage $jobID &

ls -Rl $WD/$pipelinesName > $filesFolder/$pipelinesName.3.files

################################################################################
# Individual INDELible calls
################################################################################

mkdir -p $scriptsFolder/indelible

for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.4.$st.o
#$ -e $outputFolder/$pipelinesName.4.$st.e
#$ -N $pipelinesName"".4.$st

module load  bio/indelible/1.03
cd $WD/$pipelinesName/$st
indelible
module unload  bio/indelible/1.03
" >  $scriptsFolder/indelible/$pipelinesName.4.$st.sh
done


################################################################################
# Running indelible for 500bp loci
#-------------------------------------------------------------------------------
for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  rm $WD/$pipelinesName/$st/control.txt
  ln -s $WD/$pipelinesName/$st/control500.txt $WD/$pipelinesName/$st/control.txt
done
for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  jobID=$(qsub $scriptsFolder/indelible/$pipelinesName.4.$st.sh | awk '{ print $3}')
  echo "$pipelinesName"".4.$st.$item    $jobID" >> $jobsSent
  bash $monitor $usageFolder/$pipelinesName.4.$st.$sizeLoci.usage $jobID &
done

ls -Rl $WD/$pipelinesName > $filesFolder/$pipelinesName.4.$sizeLoci.files
mv $WD/$pipelinesName/$st/LOG.txt $WD/$pipelinesName/$st/LOG500.txt
mv $WD/$pipelinesName/$st/trees.txt $WD/$pipelinesName/$st/trees500.txt
rm $WD/$pipelinesName/$st/control.txt

################################################################################
# Mating
################################################################################

echo -e "#! /bin/bash
#$ -m bea
#$ -o $outputFolder/$pipelinesName.5.o
#$ -e $outputFolder/$pipelinesName.5.e
#$ -N $pipelinesName.5

module load python/2.7.8
cd $WD/$pipelinesName/

python $matingProgram -p $prefix -sf $WD/$pipelinesName/ -l DEBUG

module unload python/2.7.8
" >  $scriptsFolder/$pipelinesName.5.sh

jobID=$(qsub  $scriptsFolder/$pipelinesName.5.sh | awk '{ print $3}')
echo "$pipelinesName"".5    $jobID" >> $jobsSent
bash $monitor $usageFolder/$pipelinesName.5.usage $jobID &

ls -Rl $WD/$pipelinesName > $filesFolder/$pipelinesName.5.files

################################################################################
# Reference Loci Selection
################################################################################
echo -e "#! /bin/bash
#$ -m bea
#$ -o $outputFolder/$pipelinesName.6.o
#$ -e $outputFolder/$pipelinesName.6.e
#$ -N $pipelinesName.6

module load python/2.7.8
cd $WD/$pipelinesName/

python $lociReferenceSelection -sf  $WD/$pipelinesName -m 1 -p $prefix -o $WD/$pipelinesName/reference

module unload python/2.7.8
" >  $scriptsFolder/conusSim.$pipelinesName.6.sh

jobID=$(qsub  $scriptsFolder/conusSim.$pipelinesName.6.sh | awk '{ print $3}')
echo "$pipelinesName"".6    $jobID" >> $jobsSent
bash $monitor $usageFolder/$pipelinesName.6.usage $jobID &

ls -Rl $WD/$pipelinesName > $filesFolder/$pipelinesName.6.files



################################################################################
# Diversity Statistics
################################################################################

mkdir $WD/$pipelinesName/report/stats/
sgeCounter=1
for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for indexGT in $(seq 1 $numGeneTrees); do
    gt=$(printf "%0$numDigitsGTs""g" $indexGT)
    echo -e "$sgeCounter\t$st\t$gt\t$WD/$pipelinesName/$st/${prefix}_${gt}_TRUE.phy" >>  $scriptsFolder/$pipelinesName.diversity.rel
    echo -e "$WD/$pipelinesName/$st/${prefix}_${gt}_TRUE.phy" >>  $scriptsFolder/$pipelinesName.diversity.files
    let sgeCounter=sgeCounter+1
  done
done
# This file has the relation between the jobs being sent and the files used in the
# job
SEEDFILE=$scriptsFolder/$pipelinesName.diversity.files
echo -e "#! /bin/bash
#$ -m bea
#$ -e $outputFolder/$pipelinesName.7.e
#$ -o $outputFolder/$pipelinesName.7.o
#$ -N $pipelinesName.7

module load bio/diversity/2.0.0
cd $WD/$pipelinesName/

SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE )
INPUTBASE=\$(basename \$SEED .phy)

diversity \$SEED -g 1> $WD/$pipelinesName/report/stats/JOB_\${SGE_TASK_ID}_\$INPUTBASE.sum.stats
diversity \$SEED -g -m -p 2> $WD/$pipelinesName/report/stats/JOB_\${SGE_TASK_ID}_\$INPUTBASE.full.stats
cat $WD/$pipelinesName/report/stats/JOB_\${SGE_TASK_ID}_\$INPUTBASE.full.stats | sed 's/\.0000/\\t/g' | tail -n+15 >  $WD/$pipelinesName/report/stats/JOB_\${SGE_TASK_ID}_\$INPUTBASE.format.stats

module load bio/diversity/2.0.0
" >  $scriptsFolder/$pipelinesName.7.sh

<<INFO_DIVERSITY
numTaxa = 261
numSites = 1000
numVarSites = 999
numInfSites = 996
numMissSites = 0
numGapSites = 0
numAmbiguousSites  = 0
base frequencies (ACGT) = 0.29 0.21 0.15 0.35
mean pairwise distance = 528.7943
mean pairwise distance per site = 0.5288
INFO_DIVERSITY
totalJobs7=$(wc -l $SEEDFILE | awk '{print $1}')
qsub -t 1-$totalJobs7 $scriptsFolder/$pipelinesName.7.sh
ls -Rl $WD/$pipelinesName > $filesFolder/$pipelinesName.7.files



################################################################################
# NGS Simulation
################################################################################
mkdir $WD/$pipelinesName/reads
mkdir $outputFolder/art/
mkdir $scriptsFolder/art/
for line in $(tail -n+2  $WD/$pipelinesName/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  inputFileS1="${pipelinesName}_${st}_${gt}_${prefix}_${ind}_${array[3]}_0_${array[4]}_S1.fasta"
  inputFileS2="${pipelinesName}_${st}_${gt}_${prefix}_${ind}_${array[3]}_0_${array[5]}_S2.fasta"
  echo "$st $gt $ind"
  echo -e "$WD/$pipelinesName/individuals/$sts/$gt/$inputFileS1" >>  $scriptsFolder/$pipelinesName.8.art.files
  echo -e "$WD/$pipelinesName/individuals/$sts/$gt/$inputFileS2" >>  $scriptsFolder/$pipelinesName.8.art.files
  echo -e "${st}\t${gt}\t${prefix}\t${ind}\t${array[3]}\t${array[4]}$inputFileS1" >>$scriptsFolder/$pipelinesName.8.art.rel
  echo -e "${st}\t${gt}\t${prefix}\t${ind}\t${array[3]}\t${array[5]}$inputFileS2" >>$scriptsFolder/$pipelinesName.8.art.rel
done

SEEDFILE="$scriptsFolder/$pipelinesName.8.art.files"
for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)

  echo -e "#! /bin/bash
#$ -m bea
#$ -o $outputFolder/art/$pipelinesName.8.$st.o
#$ -e $outputFolder/art/$pipelinesName.8.$st.e
#$ -N $pipielinesName.8.$st

SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE )
INPUTBASE=\$(basename \$SEED .fasta)

module load perl/5.20.1 bio/art/030915

mkdir -p $WD/$pipelinesName/reads/$st/$gt/
art_illumina -sam  -1 $profilePath/csNGSProfile_hi2500_1.txt
  -2 $profilePath/csNGSProfile_hiseq2500_2.txt
  -amp -f 100 -l 150 -p -m 250 -s 50 -rs $RANDOM -ss HS25
  -i $WD/$pipelinesName/individuals/$st/$gt/\${INPUTBASE}.fasta
  -o $WD/$pipelinesName/reads/$st/$gt/\${INPUTBASE}.R

module unload perl/5.20.1 bio/art/030915
">   $scriptsFolder/art/$pipelinesName.8art/.$st.sh
done
totalJobs8=$(wc -l $SEEDFILE | awk '{print $1}')
#qsub -t 1-2 $scriptsFolder/art/$pipelinesName.8.$st.sh
qsub -t 1-$totalJobs8 $scriptsFolder/art/$pipelinesName.8.$st.sh
ls -Rl $WD/$pipelinesName > $filesFolder/$pipelinesName.8.$st.files


<<SINGLEQUEUE

    jobID=$(qsub $scriptsFolder/art/csR1.8.reads.1.001.104.S2.sh | awk '{ print $3}')
    bash $monitor $usageFolder/csR1.8.reads.1.001.104.S2.usage  $jobID &
SINGLEQUEUE

################################################################################
# Reporting
################################################################################

<<RENAMING
for isize  in ${sizes[*]}; do
  for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
    st=$(printf "%0$numDigits""g" $line)
    mv conusSim.csBullet.4.$st.01.$isize.usage conusSim.csBullet.4.$st.$isize.usage
  done
done
RENAMING
for index in 1 2 3 5 6; do
  echo "round,date,cpu, mem, io, vmem, maxvmem" >  $usageFolder/$pipelinesName.$index.post.usage
  cat $usageFolder/$pipelinesName.usage  | tr '[:upper:]' '[:lower:]' | sed 's/n\/a/ /g' | awk -F "," '{ if (NF < 7)   print $1,",",$2,",",$3,",  ,  ,  ,  "; else   print $1,",",$2,",",$3,",",$4,",",$5,",",$6; }' >> $usageFolder/$pipelinesName.$index.post.usage
done
for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for elem in ${sizes[*]}; do
    echo "round,date,cpu, mem, io, vmem, maxvmem" >  $usageFolder/$pipelinesName.4.$st.$elem.post.usage
    $usageFolder/$pipelinesName.4.$st.$elem.usage  | tr '[:upper:]' '[:lower:]' | sed 's/n\/a/ /g' | awk -F "," '{ if (NF < 7)   print $1,",",$2,",",$3,",  ,  ,  ,  "; else   print $1,",",$2,",",$3,",",$4,",",$5,",",$6; }' >> $usageFolder/$pipelinesName.4.$st.$elem.post.usage
  done
done

for index in 1 2 3 5 6; do
  cat $WD/$pipelinesName/$pipelinesName.$index.files | awk '{print $9,$5}' > conusSim.$pipelinesName.$index.post.files
done


# ------------------------------------------------------------------------------
# Reporting mating DB
cat $WD/$pipelinesName/$pipelinesName.mating| awk -F "," 'NR==2{print "|---------|---------|-----------|---------|---------|"}{print "|",$1,"|",$2,"|",$3,"|",$4,"|",$5,"|"}' > $WD/$pipelinesName/report/$pipelinesName.mating.Rmd


# ------------------------------------------------------------------------------
# Reporting Loci to be filtered
echo "| Old folder | Old Prefix | Total Number of Loci for this prefix | Number of loci to take| New Prefix | New Folder |" > $WD/$pipelinesName/report/$pipelinesName.tofilter.dataset.Rmd
cat $WD/$pipelinesName/$pipelinesName.dataset.csv| awk -F "," 'NR==1{print "|---------|---------|-----------|---------|---------|---------|"}{print "|",$1,"|",$2,"|",$3,"|",$4,"|",$5,"|",$6,"|"}' >> $WD/$pipelinesName/report/$pipelinesName.tofilter.dataset.Rmd
# Reporting filtered loci
cat $WD/$pipelinesName/$pipelinesName.new.dataset | awk -F "," 'NR==2{print "|---------|---------|---------|-----------|---------|---------|"}{print "|",$1,"|",$2,"|",$3,"|",$4,"|",$5,"|",$6,"|"}' > $WD/$pipelinesName/report/$pipelinesName.filtered.dataset.Rmd


# ------------------------------------------------------------------------------
# Reporting Control Files for Indelible
controlFolder="$WD/$pipelinesName/report/controlFiles"
c500="$controlFolder/csR1.indelible.control.txt"
echo "### Control File (Partitions for 1000bp sequences)" > $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
echo "\`\`\`" >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
cat $c500 >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
echo "\`\`\`" >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd

# ------------------------------------------------------------------------------
# Reporting Pipeline
echo "\`\`\`" > $pipelineBash.Rmd
cat $pipelineBash.sh >> $pipelineBash.Rmd
echo "\`\`\`" >> $pipelineBash.Rmd
