#!/bin/bash -l
################################################################################
# Naming Variables
################################################################################
export pipelinesName="csSim1"
export stReplicates=1
export numDigits=${#stReplicates}
export numGeneTrees="f:500"
export numDigitsGTs=${#numGeneTrees}
export sizeLoci="1000bp"
export prefix="LOC"
export timeRange="u:20000,20000000" # 200k to 20My
export nIndsTaxa="f:10" # Number of individuals per taxa (actual num of ind will be 5)
export nTaxa="f:10" # Number of taxa
export subsRate="f:10000000" # 10^8

################################################################################
# Program paths
################################################################################
export filterEven="$HOME/src/conusSim.filtering.even.R"
export matingProgram="$HOME/src/me-phylolab-mating/mating.py"
export pipelineBash="$HOME/src/me-phylolab-conussim/conusSim-pipeline"
export LociFiltering="$HOME/src/me-phylolab-mating/LociFiltering.py"
export lociReferenceSelection="$HOME/src/me-phylolab-conussim/LociRefSelection/LociReferenceSelection.py"
export art="$HOME/bin/art_illumina"
export simphy="$HOME/bin/simphy"
export indelible="$HOME/bin/indelible"
export wrapper="$HOME/bin/INDELIble_wrapper.pl"

################################################################################
# Folders
################################################################################
export PHYLOLAB="/home/uvi/be/mef/mnt/phylolab"
export WD="$PHYLOLAB/$USER/$pipelinesName"
export profilePath="$HOME/data/csNGSProfile"
export pair1Path="$HOME/data/csNGSProfile/csNGSProfile_hiseq2500_1.txt"
export pair2Path="$HOME/data/csNGSProfile/csNGSProfile_hiseq2500_2.txt"
export outputFolder="$WD/report/output"
export usageFolder="$WD/report/usage"
export scriptsFolder="$WD/report/scripts"
export filesFolder="$WD/report/files"
export controlFolder="$WD/report/controlFiles"
export jobsSent="$outputFolder/$pipelinesName.jobs"
