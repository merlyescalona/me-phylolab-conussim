################################################################################
# (c) 2015-2016 Merly Escalona <merlyescalona@uvigo.es>
# Phylogenomics Lab. University of Vigo.
#
# Description:
# ============
# Pipeline organized to be run @CESGA's cluster.
# Version to check different approaches for the selection of reference loci.
################################################################################
#!/bin/bash -l
################################################################################
# Naming Variables
################################################################################
pipelinesName="csSim0"
stReplicates=1
numDigits=${#stReplicates}
nGTs=100
numGeneTrees="f:${nGTs}"
numDigitsGTs=${#nGTs}
sizeLoci="500bp"
prefix="data"
timeRange="u:20s000,20000000" # 200k to 20My
nIndsTaxa="f:6" # Number of individuals per taxa (actual num of ind will be 5)
nTaxa="f:5" # Number of taxa
subsRate="f:10000000" # 10^7
prefixLoci="data"
prefixRef="LOC1RND"
################################################################################
# Program paths
################################################################################
filterEven="$HOME/src/me-phylolab-conussim/conusSim.filtering.even.R"
matingProgram="$HOME/src/me-phylolab-mating/mating.py"
pipelineBash="$HOME/src/me-phylolab-conussim/conusSim-pipeline"
LociFiltering="$HOME/src/me-phylolab-mating/LociFiltering.py"
lociReferenceSelection="$HOME/src/me-phylolab-conussim/LociRefSelection/locirefsel.main.py"
art="$HOME/bin/art_illumina"
simphy="$HOME/bin/simphy"
indelible="$HOME/bin/indelible"
wrapper="$HOME/bin/INDELIble_wrapper.pl"
diversity="$HOME/bin/diversity"
fastqc="$HOME/apps/fastqc/0.11.5/fastqc"
################################################################################
# Folders
################################################################################
PHYLOLAB="/home/uvi/be/mef/mnt/phylolab"
WD="$PHYLOLAB/$USER/$pipelinesName"
profilePath="$HOME/data/csNGSProfile"
pair1Path="$HOME/data/csNGSProfile/csNGSProfile_hiseq2500_1.txt"
pair2Path="$HOME/data/csNGSProfile/csNGSProfile_hiseq2500_2.txt"
outputFolder="$WD/report/output"
usageFolder="$WD/report/usage"
scriptsFolder="$WD/report/scripts"
filesFolder="$WD/report/files"
controlFolder="$WD/report/controlFiles"
jobsSent="$outputFolder/$pipelinesName.jobs"
readsFolder="$WD/reads"
alnReadsFolder="$WD/reads/aln"
fqReadsFolder="$WD/reads/fq"
samReadsFolder="$WD/reads/sam"
referencesFolder="$WD/references"
qcFolder="$WD/report/qc"
################################################################################
# STEP 0. Setting environment
mkdir -p $WD $profilePath $outputFolder $usageFolder $scriptsFolder $filesFolder $controlFolder $readsFolder $fqReadsFolder $alnReadsFolder $referencesFolder $samReadsFolder
cp csSim1/report/controlFiles/INDELible_csSim1.txt $pipelinesName/report/controlFiles/INDELible_${pipelinesName}.txt
################################################################################
# STEP 1. SimPhy
################################################################################
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.1.o
#$ -e $outputFolder/$pipelinesName.1.e
#$ -N $pipelinesName.1

cd $PHYLOLAB/$USER/
$simphy -rs $stReplicates -rl $numGeneTrees -sb ln:-15,1 -st $timeRange -sl $nTaxa -so f:1 -sp f:100000 -su $subsRate -si $nIndsTaxa -hh ln:1.2,1 -hl ln:1.4,1 -hg f:200 -v 1 -o $pipelinesName -cs $RANDOM -om 1 -od 1 -op 1 -oc 1 -on 1
" > $scriptsFolder/$pipelinesName.1.sh

qsubLine="qsub -l num_proc=1,s_rt=1:00:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.1.sh"
jobID=$($qsubLine | awk '{ print $3}')
#
echo "Step    JobID" > $jobsSent
echo "$pipelinesName"".1    $jobID" >> $jobsSent
ls -Rl $WD > $filesFolder/$pipelinesName.1.files
echo "$pipelinesName"".1    $jobID" >> $usageFolder/$pipelinesName.1.usage
cat $outputFolder/$pipelinesName.1.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.1.usage
cat $outputFolder/$pipelinesName.1.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.1.usage
################################################################################
# STEP 2. INDELible wrapper
################################################################################
# After the running of SimPhy, it is necessary to run the INDELIble_wrapper
# to obtain the control files for INDELible. Since, is not possible to
# run it for all the configurations, it is necessary to modify the name of the
# output files in order to keep track of every thing
################################################################################
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.2.o
#$ -e $outputFolder/$pipelinesName.2.e
#$ -N $pipelinesName.2

cd $WD
controlFile=\"$controlFolder/INDELible_${pipelinesName}.txt\"
item=1
#Usage: ./INDELIble_wrapper.pl directory input_config seed numberofcores
perl $wrapper $WD \$controlFile $RANDOM 1 &> $outputFolder/$pipelinesName.2.txt
" >  $scriptsFolder/$pipelinesName.2.sh

qsubLine="qsub -l num_proc=1,s_rt=1:00:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.2.sh"
jobID=$($qsubLine | awk '{ print $3}')
echo "$pipelinesName"".2    $jobID" >> $jobsSent
ls -Rl $WD > $filesFolder/$pipelinesName.2.files
echo "$pipelinesName"".2    $jobID" >> $usageFolder/$pipelinesName.2.usage
cat $outputFolder/$pipelinesName.2.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.2.usage
cat $outputFolder/$pipelinesName.2.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.2.usage
################################################################################
# STEP 2.1. Filtering STs
################################################################################
# Filtering species tree replicates with even number of individuals per species
################################################################################
echo -e "
#! /bin/bash
#$ -o $outputFolder/$pipelinesName.2.1.o
#$ -e $outputFolder/$pipelinesName.2.1.e
#$ -N $pipelinesName.2.1

cd $WD/
module load R/3.2.3
Rscript $filterEven $pipelinesName.db $WD/$pipelinesName.evens $WD/$pipelinesName.odds
module unload R/3.2.3
">  $scriptsFolder/$pipelinesName.2.1.sh

qsubLine="qsub -l num_proc=1,s_rt=1:00:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.2.1.sh"
jobID=$($qsubLine | awk '{ print $3}')

echo "$pipelinesName"".2.1    $jobID" >> $jobsSent
ls -Rl $WD > $filesFolder/$pipelinesName.2.1.files
echo "$pipelinesName"".2.1    $jobID" >> $usageFolder/$pipelinesName.2.1.usage
cat $outputFolder/$pipelinesName.2.1.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.2.1.usage
cat $outputFolder/$pipelinesName.2.1.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.2.1.usage



################################################################################
# STEP 3. INDELible calls
################################################################################

echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.3.o
#$ -e $outputFolder/$pipelinesName.3.e
#$ -N ${pipelinesName}.3

cd $WD/1
$indelible
" >  $scriptsFolder/$pipelinesName.3.sh
jobID=$(qsub -l num_proc=1,s_rt=2:00:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.3.sh | awk '{ print $3}')
echo "$pipelinesName"".3    $jobID" >> $jobsSent
ls -Rl $WD > $filesFolder/$pipelinesName.3.files
echo "$pipelinesName"".3    $jobID" >> $usageFolder/$pipelinesName.3.usage
cat $outputFolder/$pipelinesName.3.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.3.usage
cat $outputFolder/$pipelinesName.3.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.3.usage

################################################################################
# STEP 4. Mating
################################################################################

echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.4.o
#$ -e $outputFolder/$pipelinesName.4.e
#$ -N $pipelinesName.4

module load python/2.7.8
cd $WD/$pipelinesName/

python $matingProgram -p $prefixLoci -sf $WD/ -l DEBUG

module unload python/2.7.8
" >  $scriptsFolder/$pipelinesName.4.sh

jobID=$(qsub -l num_proc=1,s_rt=2:00:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.4.sh | awk '{ print $3}')
echo "$pipelinesName"".4    $jobID" >> $jobsSent
ls -Rl $WD > $filesFolder/$pipelinesName.4.files
echo "$pipelinesName"".4    $jobID" >> $usageFolder/$pipelinesName.4.usage
cat $outputFolder/$pipelinesName.4.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.4.usage
cat $outputFolder/$pipelinesName.4.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.4.usage

################################################################################
# STEP 5. Reference Loci Selection
################################################################################
echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.5.o
#$ -e $outputFolder/$pipelinesName.5.e
#$ -N $pipelinesName.5

module load python/2.7.8
cd $WD/
python $lociReferenceSelection -ip $prefixLoci -op $prefixRef -sf $WD -o $WD/references/ -m 1
module unload python/2.7.8
" >  $scriptsFolder/$pipelinesName.5.sh

jobID=$(qsub -l num_proc=1,s_rt=0:10:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.5.sh | awk '{ print $3}')
echo "$pipelinesName"".5    $jobID" >> $jobsSent
ls -Rl $WD > $filesFolder/$pipelinesName.5.files
echo "$pipelinesName"".5    $jobID" >> $usageFolder/$pipelinesName.5.usage
cat $outputFolder/$pipelinesName.5.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.5.usage
cat $outputFolder/$pipelinesName.5.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.5.usage


################################################################################
# Step 5.1. Create file with information of the reference loci selected
################################################################################
for item in $(find $referencesFolder -name *.fasta); do
  head -1 $item >> $WD/${pipelinesName}.references.txt
done

################################################################################
# STEP 6. Diversity Statistics
################################################################################
# Creating an index file
mkdir $WD/report/stats/
sgeCounter=1
for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for indexGT in $(seq 1 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $indexGT)
    echo -e "$sgeCounter\t$st\t$gt\t$WD/$st/${prefixLoci}_${gt}_TRUE.phy" >>  $scriptsFolder/$pipelinesName.diversity.re
    echo -e "$WD/$st/${prefix}_${gt}_TRUE.phy" >>  $scriptsFolder/$pipelinesName.diversity.files
    let sgeCounter=sgeCounter+1
  done
done
################################################################################
# Running diversity
counter=1
divFile="$scriptsFolder/$pipelinesName.diversity.files"
#divFile="mnt/phylolab/uvibemef/csSim0/report/scripts/csSim0.diversity.files"
totalFiles=$(wc -l $divFile)
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.6.o
#$ -e $outputFolder/$pipelinesName.6.e
#$ -N $pipelinesName.6

cd $WD/1

" > $scriptsFolder/$pipelinesName.6.sh
for line in $(cat $divFile); do
  echo $counter/$totalFiles
  INPUTBASE=$(basename $line .phy)
  filename=$(basename $line)
  echo "diversity $filename &> $WD/report/stats/${INPUTBASE}.0.stats" >> $scriptsFolder/$pipelinesName.6.sh
  echo "diversity $filename -g &> $WD/report/stats/${INPUTBASE}.1.stats">> $scriptsFolder/$pipelinesName.6.sh
  echo "diversity $filename -g -m -p 2> $WD/report/stats/${INPUTBASE}.2.stats">> $scriptsFolder/$pipelinesName.6.sh
  echo "cat $WD/report/stats/${INPUTBASE}.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 >  $WD/report/stats/${INPUTBASE}.3.stats">> $scriptsFolder/$pipelinesName.6.sh
  let counter=counter+1
done

jobID=$(qsub -l num_proc=1,s_rt=0:30:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.6.sh | awk '{ print $3}')
echo "$pipelinesName"".6    $jobID" >> $jobsSent
ls -Rl $WD > $filesFolder/$pipelinesName.6.files
echo "$pipelinesName"".6    $jobID" >> $usageFolder/$pipelinesName.6.usage
cat $outputFolder/$pipelinesName.6.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.6.usage
cat $outputFolder/$pipelinesName.6.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.6.usage

################################################################################
# Parsing diversity
################################################################################
inputFolder="$WD/report/stats"
divOutputFolder="$WD/report/stats/csv"
mkdir $divOutputFolder
echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.6.1.o
#$ -e $outputFolder/$pipelinesName.6.1.e
#$ -N $pipelinesName.6.1

module load R/3.2.3
" > $scriptsFolder/$pipelinesName.6.1.sh
for item in $(seq 1 $nGTs); do
  gt=$(printf "%0$numDigitsGTs""g" $item)
  inputbase="${prefixLoci}_${gt}"
  echo $inputbase
  inputFolder="/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim0/report/stats"
  outputFolder="/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim0/report/stats/csv"
  echo "Rscript --vanilla $HOME/src/me-phylolab-conussim/diversityMatrices/parseDiversityMatrices.R $inputFolder $inputbase $outputFolder" >> $scriptsFolder/$pipelinesName.6.1.sh
done
echo "module unload R/3.2.3" >> $scriptsFolder/$pipelinesName.6.1.sh


jobID=$(qsub -l num_proc=1,s_rt=0:30:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.6.1.sh | awk '{ print $3}')
echo "$pipelinesName"".6.1    $jobID" >> $jobsSent
ls -Rl $WD > $filesFolder/$pipelinesName.6.1.files
echo "$pipelinesName"".6.1    $jobID" >> $usageFolder/$pipelinesName.6.1.usage
cat $outputFolder/$pipelinesName.6.1.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.6.1.usage
cat $outputFolder/$pipelinesName.6.1.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.6.1.usage

################################################################################
# STEP 7. NGS Simulation
################################################################################
for line in $(tail -n+2  $WD/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  inputFileS1="${pipelinesName}_${st}_${gt}_${prefix}_${ind}_${array[3]}_0_${array[4]}_S1.fasta"
  inputFileS2="${pipelinesName}_${st}_${gt}_${prefix}_${ind}_${array[3]}_0_${array[5]}_S2.fasta"
  echo "$st $gt $ind"
  echo -e "$WD/individuals/$st/$gt/$inputFileS1" >>  $scriptsFolder/$pipelinesName.7.art.files
  echo -e "$WD/individuals/$st/$gt/$inputFileS2" >>  $scriptsFolder/$pipelinesName.7.art.files
  echo -e "${st}\t${gt}\t${prefix}\t${ind}\t${array[3]}\t${array[4]}$inputFileS1" >>$scriptsFolder/$pipelinesName.7.art.rel
  echo -e "${st}\t${gt}\t${prefix}\t${ind}\t${array[3]}\t${array[5]}$inputFileS2" >>$scriptsFolder/$pipelinesName.7.art.rel
done

more  $scriptsFolder/$pipelinesName.7.art.files

SEEDFILE="$scriptsFolder/$pipelinesName.7.art.files"
for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.7.$st.o
#$ -e $outputFolder/$pipelinesName.7.$st.e
#$ -N $pipelinesName.7.$st

SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE )
INPUTBASE=\$(basename \$SEED .fasta)

cd $readsFolder/
art_illumina -sam  -1 $profilePath/csNGSProfile_hiseq2500_1.txt -2 $profilePath/csNGSProfile_hiseq2500_2.txt -amp -f 100 -l 150 -p -m 250 -s 50 -rs $RANDOM -ss HS25 -i \$SEED -o \${INPUTBASE}_R
">   $scriptsFolder/$pipelinesName.7.art.$st.sh
done
totalArtJobs=$(wc -l $SEEDFILE | awk '{print $1}')
for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-$totalArtJobs $scriptsFolder/$pipelinesName.7.art.$st.sh | awk '{ print $3}')
  echo "$pipelinesName"".7.art.$st    $jobID" >> $jobsSent
  echo "$pipelinesName"".7.art.$st    $jobID" >> $usageFolder/$pipelinesName.7.art.$st.usage
done
<<ITERATEJOBS
for i in $(seq 21001 1000 50000); do
  echo "Sending jobs from $i to $temp"
  qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t $i-$temp $scriptsFolder/$pipelinesName.7.art.1.sh
  let temp=temp+1000
  sleep 600
done
ITERATEJOBS
################################################################################
# STEP 8. Reorganize reads accroding to output file format and ST and GT.
################################################################################

# Just works for this case, will need to rearrange the folders.
# Need to think how would be the best way
# probably reads/ST/fq,aln,sam
for line in $(tail -n+2  $WD/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  mkdir -p $fqReadsFolder/$st/$gt/$ind $alnReadsFolder/$st/$gt/$ind $samReadsFolder/$st/$gt/$ind
  filePrefix="${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}_*."

  mv ${filePrefix}fq $fqReadsFolder/$st/$gt/$ind
  mv ${filePrefix}aln $alnReadsFolder/$st/$gt/$ind
  mv ${filePrefix}sam $samReadsFolder/$st/$gt/$ind
done


mv $readsFolder/*.fq $readsFolder/fq/
mv $readsFolder/*.aln $readsFolder/aln/
mv $readsFolder/*.sam $readsFolder/sam/

ls -Rl $WD > $filesFolder/$pipelinesName.7.art.files
cat $outputFolder/$pipelinesName.7.art.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.7.art.usage
cat $outputFolder/$pipelinesName.7.art.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.7.art.usage

for ext in fq aln sam; do
  for line in $(cat $WD/$pipelinesName.evens); do
    st=$(printf "%0$numDigits""g" $line)
    for item in $(seq 1 $nGTs); do
      gt=$(printf "%0$numDigitsGTs""g" $item)
      echo "$st $gt"
      inputFileS1="${pipelinesName}_${st}_${gt}_${prefixLoci}_"
      mv "$readsFolder/$ext/$inputFileS1"* $WD/reads/$ext/$st/$gt/
    done
  done
done

################################################################################
# STEP 9. FASTQC
################################################################################

fqFiles="$fqReadsFolder/${pipelinesName}.allfiles.fastq"
find $fqReadsFolder -name *.fq | xargs cat > $fqFiles
for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for item in $(seq 1 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $item)
    find $fqReadsFolder/$st/$gt -name *.fq | xargs cat > "$fqReadsFolder/${pipelinesName}.${st}.${gt}.fastq"
  done
done

SEEDFILE="$scriptsFolder/$pipelinesName.9.all.files"
find $fqReadsFolder/${pipelinesName}.*.*.fastq > $SEEDFILE
echo "$fqFiles" >> $SEEDFILE

for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.9.$st.o
#$ -e $outputFolder/$pipelinesName.9.$st.e
#$ -N $pipelinesName.9.$st

SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE )
INPUTBASE=\$(basename \$SEED .fastq)

mkdir $qcFolder/\$INPUTBASE
cd $qcFolder/\$INPUTBASE
$fastqc \$SEED -o $qcFolder/\$INPUTBASE

">   $scriptsFolder/$pipelinesName.9.$st.sh
done

totalJobs=$(wc -l $SEEDFILE | awk '{print $1}')
jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-$totalJobs $scriptsFolder/$pipelinesName.9.$st.sh | awk '{ print $3}')
################################################################################
# Reporting
################################################################################

<<RENAMING
for isize  in ${sizes[*]}; do
  for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
    st=$(printf "%0$numDigits""g" $line)
    mv conusSim.csBullet.4.$st.01.$isize.usage conusSim.csBullet.4.$st.$isize.usage
  done
done
RENAMING
for index in 1 2 3 5 6; do
  echo "round,date,cpu, mem, io, vmem, maxvmem" >  $usageFolder/$pipelinesName.$index.post.usage
  cat $usageFolder/$pipelinesName.usage  | tr '[:upper:]' '[:lower:]' | sed 's/n\/a/ /g' | awk -F "," '{ if (NF < 7)   print $1,",",$2,",",$3,",  ,  ,  ,  "; else   print $1,",",$2,",",$3,",",$4,",",$5,",",$6; }' >> $usageFolder/$pipelinesName.$index.post.usage
done
for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for elem in ${sizes[*]}; do
    echo "round,date,cpu, mem, io, vmem, maxvmem" >  $usageFolder/$pipelinesName.4.$st.$elem.post.usage
    $usageFolder/$pipelinesName.4.$st.$elem.usage  | tr '[:upper:]' '[:lower:]' | sed 's/n\/a/ /g' | awk -F "," '{ if (NF < 7)   print $1,",",$2,",",$3,",  ,  ,  ,  "; else   print $1,",",$2,",",$3,",",$4,",",$5,",",$6; }' >> $usageFolder/$pipelinesName.4.$st.$elem.post.usage
  done
done

for index in 1 2 3 5 6; do
  cat $WD/$pipelinesName/$pipelinesName.$index.files | awk '{print $9,$5}' > conusSim.$pipelinesName.$index.post.files
done


# ------------------------------------------------------------------------------
# Reporting mating DB
cat $WD/$pipelinesName/$pipelinesName.mating| awk -F "," 'NR==2{print "|---------|---------|-----------|---------|---------|"}{print "|",$1,"|",$2,"|",$3,"|",$4,"|",$5,"|"}' > $WD/$pipelinesName/report/$pipelinesName.mating.Rmd


# ------------------------------------------------------------------------------
# Reporting Loci to be filtered
echo "| Old folder | Old Prefix | Total Number of Loci for this prefix | Number of loci to take| New Prefix | New Folder |" > $WD/$pipelinesName/report/$pipelinesName.tofilter.dataset.Rmd
cat $WD/$pipelinesName/$pipelinesName.dataset.csv| awk -F "," 'NR==1{print "|---------|---------|-----------|---------|---------|---------|"}{print "|",$1,"|",$2,"|",$3,"|",$4,"|",$5,"|",$6,"|"}' >> $WD/$pipelinesName/report/$pipelinesName.tofilter.dataset.Rmd
# Reporting filtered loci
cat $WD/$pipelinesName/$pipelinesName.new.dataset | awk -F "," 'NR==2{print "|---------|---------|---------|-----------|---------|---------|"}{print "|",$1,"|",$2,"|",$3,"|",$4,"|",$5,"|",$6,"|"}' > $WD/$pipelinesName/report/$pipelinesName.filtered.dataset.Rmd


# ------------------------------------------------------------------------------
# Reporting Control Files for Indelible
controlFolder="$WD/$pipelinesName/report/controlFiles"
c500="$controlFolder/csR1.indelible.control.txt"
echo "### Control File (Partitions for 1000bp sequences)" > $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
echo "\`\`\`" >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
cat $c500 >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
echo "\`\`\`" >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd

# ------------------------------------------------------------------------------
# Reporting Pipeline
echo "\`\`\`" > $pipelineBash.Rmd
cat $pipelineBash.sh >> $pipelineBash.Rmd
echo "\`\`\`" >> $pipelineBash.Rmd
