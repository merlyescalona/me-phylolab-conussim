################################################################################
# (c) 2015-2016 Merly Escalona <merlyescalona@uvigo.es>
# Phylogenomics Lab. University of Vigo.
#
# Description:
# ============
# Pipeline organized to be run @CESGA's cluster.
################################################################################
#!/bin/bash -l
################################################################################
# Naming Variables
################################################################################
pipelinesName="csSim1"
stReplicates=1
numDigits=${#stReplicates}
nGTs=500
numGeneTrees="f:${nGTs}}"
numDigitsGTs=${#nGTs}
sizeLoci="1000bp"
prefix="data"
timeRange="u:20000,20000000" # 200k to 20My
nIndsTaxa="f:10" # Number of individuals per taxa (actual num of ind will be 5)
nTaxa="f:10" # Number of taxa
subsRate="f:10000000" # 10^8
################################################################################
# Program paths
################################################################################
filterEven="$HOME/src/me-phylolab-conussim/conusSim.filtering.even.R"
matingProgram="$HOME/src/me-phylolab-mating/mating.py"
pipelineBash="$HOME/src/me-phylolab-conussim/conusSim-pipeline"
LociFiltering="$HOME/src/me-phylolab-mating/LociFiltering.py"
lociReferenceSelection="$HOME/src/me-phylolab-conussim/LociRefSelection/locirefsel.main.py"
art="$HOME/bin/art_illumina"
simphy="$HOME/bin/simphy"
indelible="$HOME/bin/indelible"
wrapper="$HOME/bin/INDELIble_wrapper.pl"
diversity="$HOME/bin/diversity"
################################################################################
# Folders
################################################################################
PHYLOLAB="/home/uvi/be/mef/mnt/phylolab"
WD="$PHYLOLAB/$USER/$pipelinesName"
profilePath="$HOME/data/csNGSProfile"
pair1Path="$HOME/data/csNGSProfile/csNGSProfile_hiseq2500_1.txt"
pair2Path="$HOME/data/csNGSProfile/csNGSProfile_hiseq2500_2.txt"
outputFolder="$WD/report/output"
usageFolder="$WD/report/usage"
scriptsFolder="$WD/report/scripts"
filesFolder="$WD/report/files"
controlFolder="$WD/report/controlFiles"
jobsSent="$outputFolder/$pipelinesName.jobs"
readsFolder="$WD/reads"
alnReadsFolder="$WD/reads/aln"
fqReadsFolder="$WD/reads/fq"
################################################################################
# STEP 0. Setting environment
mkdir -p $WD $profilePath $outputFolder $usageFolder $scriptsFolder $filesFolder $controlFolder $readsFolder $fqReadsFolder $alnReadsFolder
################################################################################
# STEP 1. SimPhy
################################################################################
echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com.
#$ -o $outputFolder/$pipelinesName.1.o
#$ -e $outputFolder/$pipelinesName.1.e
#$ -N $pipelinesName.1

cd $PHYLOLAB/$USER/
$simphy -rs $stReplicates -rl $numGeneTrees -sb ln:-15,1 -st $timeRange -sl $nTaxa -so f:1 -sp f:100000 -su $subsRate -si $nIndsTaxa -hh ln:1.2,1 -hl ln:1.4,1 -hg f:200 -v 1 -o $pipelinesName -cs $RANDOM -om 1 -od 1 -op 1 -oc 1 -on 1
" > $scriptsFolder/$pipelinesName.1.sh

qsubLine="qsub -l num_proc=1,s_rt=1:00:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.1.sh"
jobID=$($qsubLine | awk '{ print $3}')
#
echo "Step    JobID" > $jobsSent
echo "$pipelinesName"".1    $jobID" >> $jobsSent
ls -Rl $WD > $filesFolder/$pipelinesName.1.files
cat $outputFolder/$pipelinesName.1.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.1.usage
cat $outputFolder/$pipelinesName.1.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.1.usage
################################################################################
# STEP 2. INDELible wrapper
################################################################################
# After the running of SimPhy, it is necessary to run the INDELIble_wrapper
# to obtain the control files for INDELible. Since, is not possible to
# run it for all the configurations, it is necessary to modify the name of the
# output files in order to keep track of every thing
################################################################################
echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.2.o
#$ -e $outputFolder/$pipelinesName.2.e
#$ -N $pipelinesName.2

cd $WD
controlFile=\"$controlFolder/INDELible_${pipelinesName}.txt\"
item=1
#Usage: ./INDELIble_wrapper.pl directory input_config seed numberofcores
perl $wrapper $WD \$controlFile $RANDOM 1 &> $outputFolder/$pipelinesName.2.txt
" >  $scriptsFolder/$pipelinesName.2.sh

jobID=$(qsub -l num_proc=1,s_rt=1:00:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.2.sh | awk '{ print $3}')
echo "$pipelinesName"".2   $jobID" >> $jobsSent
ls -Rl $WD/ > $filesFolder/$pipelinesName.2.files
cat $outputFolder/$pipelinesName.2.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.2.usage
cat $outputFolder/$pipelinesName.2.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.2.usage

################################################################################
# STEP 2.1. Filtering STs
################################################################################
# Filtering species tree replicates with even number of individuals per species
################################################################################
echo -e "
#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.2.1.o
#$ -e $outputFolder//$pipelinesName.2.1.e
#$ -N $pipelinesName.2.1

cd $WD/
module load R/3.2.3
Rscript $filterEven $pipelinesName.db $WD/$pipelinesName.evens $WD/$pipelinesName.odds
module unload R/3.2.3
">  $scriptsFolder/$pipelinesName.2.1.sh


jobID=$(qsub -l num_proc=1,s_rt=0:10:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.2.1.sh | awk '{ print $3}')
echo "$pipelinesName"".2.1    $jobID" >> $jobsSent
ls -Rl $WD/ > $filesFolder/$pipelinesName.2.1.files


################################################################################
# STEP 3. INDELible calls
################################################################################

echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.3.o
#$ -e $outputFolder/$pipelinesName.3.e
#$ -N ${pipelinesName}.3

cd $WD/1
$indelible
" >  $scriptsFolder/$pipelinesName.3.sh
jobID=$(qsub -l num_proc=1,s_rt=2:00:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.3.sh | awk '{ print $3}')
echo "${pipelinesName}.3   $jobID" >> $jobsSent
ls -Rl $WD/ > $filesFolder/$pipelinesName.3.files


################################################################################
# STEP 4. Mating
################################################################################

echo -e "#! /bin/bash
#$ -m bea
#$ -o $outputFolder/$pipelinesName.4.o
#$ -e $outputFolder/$pipelinesName.4.e
#$ -N $pipelinesName.4

module load python/2.7.8
cd $WD/$pipelinesName/

python $matingProgram -p $prefix -sf $WD/$pipelinesName/ -l DEBUG

module unload python/2.7.8
" >  $scriptsFolder/$pipelinesName.4.sh

jobID=$(qsub -l num_proc=1,s_rt=2:00:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.4.sh | awk '{ print $3}')
echo "$pipelinesName"".4    $jobID" >> $jobsSent
bash $monitor $usageFolder/$pipelinesName.4.usage $jobID &

ls -Rl $WD/ > $filesFolder/$pipelinesName.4.files

################################################################################
# STEP 5. Reference Loci Selection
################################################################################
echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.5.o
#$ -e $outputFolder/$pipelinesName.5.e
#$ -N $pipelinesName.5

module load python/2.7.8
cd $WD/
python $lociReferenceSelection -ip data -op LOCM4 -sf $WD -o $WD/references/ -m 4
module unload python/2.7.8
" >  $scriptsFolder/$pipelinesName.5.sh

jobID=$(qsub -l num_proc=1,s_rt=0:10:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.5.sh | awk '{ print $3}')
echo "$pipelinesName"".5    $jobID" >> $jobsSent

ls -Rl $WD/ > $filesFolder/$pipelinesName.5.files

################################################################################
# STEP 6. Diversity Statistics
################################################################################

mkdir $WD/report/stats/
sgeCounter=1
for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for indexGT in $(seq 1 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $indexGT)
    echo -e "$sgeCounter\t$st\t$gt\t$WD/$st/${prefix}_${gt}_TRUE.phy" >>  $scriptsFolder/$pipelinesName.diversity.re
    echo -e "$WD/$st/${prefix}_${gt}_TRUE.phy" >>  $scriptsFolder/$pipelinesName.diversity.files
    let sgeCounter=sgeCounter+1
  done
done
################################################################################
counter=1
divFile="mnt/phylolab/uvibemef/csSim1/report/scripts/csSim1.diversity.files"
totalFiles=$(wc -l $divFile)
for line in $(cat $divFile); do
  echo $counter/$totalFiles
  echo $line
  INPUTBASE=$(basename $line .phy)
  echo "divesity_node $line &> mnt/phylolab/uvibemef/csSim1/report/stats/${INPUTBASE}.0.stats" >> diversity.sh
  echo "divesity_node $line -g &> mnt/phylolab/uvibemef/csSim1/report/stats/${INPUTBASE}.1.stats">> diversity.sh
  echo "divesity_node $line -g -m -p 2> mnt/phylolab/uvibemef/csSim1/report/stats/${INPUTBASE}.2.stats">> diversity.sh
  echo "cat mnt/phylolab/uvibemef/csSim1/report/stats/${INPUTBASE}.2.stats | sed 's/\.0000/\\t/g' | tail -n+15 >  mnt/phylolab/uvibemef/csSim1/report/stats/${INPUTBASE}.3.stats">> diversity.sh
  let counter=counter+1
done

sbatch --time=00:10:00  $PHYLOLAB/$USER/$pipelinesName/report/scripts/$pipelinesName.6.1.sh

qsub -l num_proc=1,s_rt=0:30:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.6.1.sh
################################################################################

# This file has the relation between the jobs being sent and the files used in the
# job
SEEDFILE=$scriptsFolder/$pipelinesName.diversity.files
echo -e "#! /bin/bash
#$ -m bea
#$ -e $outputFolder/$pipelinesName.6.e
#$ -o $outputFolder/$pipelinesName.6.o
#$ -N $pipelinesName.6

cd $WD/

SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE )
INPUTBASE=\$(basename \$SEED .phy)
echo $SEED

diversity \$SEED -g 1> $WD/report/stats/JOB_\${SGE_TASK_ID}_\$INPUTBASE.sum.stats
diversity \$SEED -g -m -p 2> $WD/report/stats/JOB_\${SGE_TASK_ID}_\$INPUTBASE.full.stats
cat $WD/report/stats/JOB_\${SGE_TASK_ID}_\$INPUTBASE.full.stats | sed 's/\.0000/\\t/g' | tail -n+15 >  $WD/report/stats/JOB_\${SGE_TASK_ID}_\$INPUTBASE.format.stats

" >  $scriptsFolder/$pipelinesName.6.sh

<<INFO_DIVERSITY
numTaxa = 261
numSites = 1000
numVarSites = 999
numInfSites = 996
numMissSites = 0
numGapSites = 0
numAmbiguousSites  = 0
base frequencies (ACGT) = 0.29 0.21 0.15 0.35
mean pairwise distance = 528.7943
mean pairwise distance per site = 0.5288
INFO_DIVERSITY

totalJobs=$(wc -l $SEEDFILE | awk '{print $1}')
blockJobs=10
qsub -l num_proc=1,s_rt=0:30:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-$blockJobs $scriptsFolder/$pipelinesName.6.sh

# 4335711.1-10:1
#
ls -Rl $WD/ > $filesFolder/$pipelinesName.6.files
<<DIVERSITY_TRIPLOID
cd /home/merly/cesga/csSim1/1

for item in $(ls *.phy); do
inputbase=$(basename $item .phy)
echo $inputbase
diversity $item &> /home/merly/statsCSSIM1/${inputbase}.0.stats
diversity $item -g &> /home/merly/statsCSSIM1/${inputbase}.1.stats
diversity $item -g -m -p &> /home/merly/statsCSSIM1/${inputbase}.2.stats
cat /home/merly/statsCSSIM1/${inputbase}.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 >  /home/merly/statsCSSIM1/${inputbase}.3.stats
done
DIVERSITY_TRIPLOID

<<PARSING_DIVERSITY
for item in $(seq 1 500); do
  gt=$(printf "%0$numDigitsGTs""g" $item)
inputbase="data_$gt"
echo $inputbase
inputFolder="/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim1/report/stats"
outputFolder="/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim1/report/stats/csv"
Rscript --vanilla $HOME/src/me-phylolab-conussim/diversityMatrices/parseDiversityMatrices.R $inputFolder $inputbase $outputFolder
done
PARSING_DIVERSITY


################################################################################
# STEP 7. NGS Simulation
################################################################################


for line in $(tail -n+2  $WD/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  inputFileS1="${pipelinesName}_${st}_${gt}_${prefix}_${ind}_${array[3]}_0_${array[4]}_S1.fasta"
  inputFileS2="${pipelinesName}_${st}_${gt}_${prefix}_${ind}_${array[3]}_0_${array[5]}_S2.fasta"
  echo "$st $gt $ind"
  echo -e "$WD/individuals/$st/$gt/$inputFileS1" >>  $scriptsFolder/$pipelinesName.7.art.files
  echo -e "$WD/individuals/$st/$gt/$inputFileS2" >>  $scriptsFolder/$pipelinesName.7.art.files
  echo -e "${st}\t${gt}\t${prefix}\t${ind}\t${array[3]}\t${array[4]}$inputFileS1" >>$scriptsFolder/$pipelinesName.7.art.rel
  echo -e "${st}\t${gt}\t${prefix}\t${ind}\t${array[3]}\t${array[5]}$inputFileS2" >>$scriptsFolder/$pipelinesName.7.art.rel
done

more  $scriptsFolder/$pipelinesName.7.art.files

SEEDFILE="$scriptsFolder/$pipelinesName.7.art.files"
for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.7.$st.o
#$ -e $outputFolder/$pipelinesName.7.$st.e
#$ -N $pipelinesName.7.$st

SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE )
INPUTBASE=\$(basename \$SEED .fasta)

mkdir -p $readsFolder/$st
cd $readsFolder/$s
art_illumina -sam  -1 $profilePath/csNGSProfile_hiseq2500_1.txt -2 $profilePath/csNGSProfile_hiseq2500_2.txt -amp -f 100 -l 150 -p -m 250 -s 50 -rs $RANDOM -ss HS25 -i \$SEED -o \${INPUTBASE}_R
">   $scriptsFolder/$pipelinesName.7.art.$st.sh
done
totalArtJobs=$(wc -l $SEEDFILE | awk '{print $1}')
st=1
#qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 2501-3000 $scriptsFolder/$pipelinesName.7.art.$st.sh
#qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 38001-40000 /home/uvi/be/mef/mnt/phylolab/uvibemef/csSim1/report/scripts/csSim1.7.art.1.sh
temp=22000
for i in $(seq 21001 1000 50000); do
  echo "Sending jobs from $i to $temp"
  qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t $i-$temp $scriptsFolder/$pipelinesName.7.art.1.sh
  let temp=temp+1000
  sleep 600
done

ls -Rl $WD/ > $filesFolder/$pipelinesName.7.$st.files


################################################################################
# STEP 8. Reorganize reads per gts per sts
################################################################################
mv $readsFolder/*.aln $alnReadsFolder
mv $readsFolder/*.fq $fqReadsFolder


for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for item in $(seq 1 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $item)
    mkdir $WD/reads/fq/$st/$gt $WD/reads/aln/$st/$gt
  done
done
for ext in fq aln; do
  cd $WD/reads/$ext
  for line in $(tail -n+2  $WD/$pipelinesName.mating); do
    array=(${line//,/ })
    # Have 5 elems
    st=${array[0]}
    gt=${array[1]}
    echo "$st $gt"
    inputFileS1="${pipelinesName}_${st}_${gt}_${prefix}_"
    mv $inputFileS1* $WD/reads/$ext/$st/$gt/
  done
done
################################################################################
# Reporting
################################################################################

<<RENAMING
for isize  in ${sizes[*]}; do
  for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
    st=$(printf "%0$numDigits""g" $line)
    mv conusSim.csBullet.4.$st.01.$isize.usage conusSim.csBullet.4.$st.$isize.usage
  done
done
RENAMING
for index in 1 2 3 5 6; do
  echo "round,date,cpu, mem, io, vmem, maxvmem" >  $usageFolder/$pipelinesName.$index.post.usage
  cat $usageFolder/$pipelinesName.usage  | tr '[:upper:]' '[:lower:]' | sed 's/n\/a/ /g' | awk -F "," '{ if (NF < 7)   print $1,",",$2,",",$3,",  ,  ,  ,  "; else   print $1,",",$2,",",$3,",",$4,",",$5,",",$6; }' >> $usageFolder/$pipelinesName.$index.post.usage
done
for line in $(cat $WD/$pipelinesName/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for elem in ${sizes[*]}; do
    echo "round,date,cpu, mem, io, vmem, maxvmem" >  $usageFolder/$pipelinesName.4.$st.$elem.post.usage
    $usageFolder/$pipelinesName.4.$st.$elem.usage  | tr '[:upper:]' '[:lower:]' | sed 's/n\/a/ /g' | awk -F "," '{ if (NF < 7)   print $1,",",$2,",",$3,",  ,  ,  ,  "; else   print $1,",",$2,",",$3,",",$4,",",$5,",",$6; }' >> $usageFolder/$pipelinesName.4.$st.$elem.post.usage
  done
done

for index in 1 2 3 5 6; do
  cat $WD/$pipelinesName/$pipelinesName.$index.files | awk '{print $9,$5}' > conusSim.$pipelinesName.$index.post.files
done


# ------------------------------------------------------------------------------
# Reporting mating DB
cat $WD/$pipelinesName/$pipelinesName.mating| awk -F "," 'NR==2{print "|---------|---------|-----------|---------|---------|"}{print "|",$1,"|",$2,"|",$3,"|",$4,"|",$5,"|"}' > $WD/$pipelinesName/report/$pipelinesName.mating.Rmd


# ------------------------------------------------------------------------------
# Reporting Loci to be filtered
echo "| Old folder | Old Prefix | Total Number of Loci for this prefix | Number of loci to take| New Prefix | New Folder |" > $WD/$pipelinesName/report/$pipelinesName.tofilter.dataset.Rmd
cat $WD/$pipelinesName/$pipelinesName.dataset.csv| awk -F "," 'NR==1{print "|---------|---------|-----------|---------|---------|---------|"}{print "|",$1,"|",$2,"|",$3,"|",$4,"|",$5,"|",$6,"|"}' >> $WD/$pipelinesName/report/$pipelinesName.tofilter.dataset.Rmd
# Reporting filtered loci
cat $WD/$pipelinesName/$pipelinesName.new.dataset | awk -F "," 'NR==2{print "|---------|---------|---------|-----------|---------|---------|"}{print "|",$1,"|",$2,"|",$3,"|",$4,"|",$5,"|",$6,"|"}' > $WD/$pipelinesName/report/$pipelinesName.filtered.dataset.Rmd


# ------------------------------------------------------------------------------
# Reporting Control Files for Indelible
controlFolder="$WD/$pipelinesName/report/controlFiles"
c500="$controlFolder/csR1.indelible.control.txt"
echo "### Control File (Partitions for 1000bp sequences)" > $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
echo "\`\`\`" >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
cat $c500 >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd
echo "\`\`\`" >> $WD/$pipelinesName/report/$pipelinesName.report.controlfiles.Rmd

# ------------------------------------------------------------------------------
# Reporting Pipeline
echo "\`\`\`" > $pipelineBash.Rmd
cat $pipelineBash.sh >> $pipelineBash.Rmd
echo "\`\`\`" >> $pipelineBash.Rmd
