
SEEDMA="$WD/$pipelinesName.ma.stats.demux.vcf.txt"
SEEDMB="$WD/$pipelinesName.mb.stats.demux.vcf.txt"
SEEDMC="$WD/$pipelinesName.mc.stats.demux.vcf.txt"
SEEDMD="$WD/$pipelinesName.md.stats.demux.vcf.txt"
SEEDME="$WD/$pipelinesName.me.stats.demux.vcf.txt"
SEEDMF="$WD/$pipelinesName.mf.stats.demux.vcf.txt"
find $procVarcalling/methodA -name "*.demux.vcf" > $SEEDMA
find $procVarcalling/methodB -name "*.demux.vcf" > $SEEDMB
find $procVarcalling/methodC -name "*.demux.vcf" > $SEEDMC
find $procVarcalling/methodD -name "*.demux.vcf" > $SEEDMD
find $procVarcalling/methodE -name "*.demux.vcf" > $SEEDME
find $procVarcalling/methodF -name "*.demux.vcf" > $SEEDMF

################################################################################
# STEP 17. GET CONSENSUS SEQUENCES FOR VCF+BAM
################################################################################


###############################################################################
# step 18. Evaluation of variant calling
###############################################################################

mkdir -p $WD/proc/eval/methodA/1/ $WD/proc/eval/methodB/1/ $WD/proc/eval/methodC/1/ $WD/proc/eval/methodD/1/ $WD/proc/eval/methodE/1/ $WD/proc/eval/methodF/1/

SEEDMA="$WD/$pipelinesName.ma.estimated.varcalls.txt"
SEEDMB="$WD/$pipelinesName.mb.estimated.varcalls.txt"
SEEDMC="$WD/$pipelinesName.mc.estimated.varcalls.txt"
SEEDMD="$WD/$pipelinesName.md.estimated.varcalls.txt"
SEEDME="$WD/$pipelinesName.me.estimated.varcalls.txt"
SEEDMF="$WD/$pipelinesName.mf.estimated.varcalls.txt"

find $procVarcalling/methodA -name "*.demux.vcf" > $SEEDMA
find $procVarcalling/methodB -name "*.demux.vcf" > $SEEDMB
find $procVarcalling/methodC -name "*.demux.vcf" > $SEEDMC
find $procVarcalling/methodD -name "*.demux.vcf" > $SEEDMD
find $procVarcalling/methodE -name "*.demux.vcf" > $SEEDME
find $procVarcalling/methodF -name "*.demux.vcf" > $SEEDMF
referenceFile="$WD/$pipelinesName.references.files"

echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.a.18.o
#$ -e $outputFolder/$pipelinesName.a.18.e
#$ -N cs2.a.18

module load jdk-oracle/1.8.0
">$scriptsFolder/$pipelinesName.a.18.sh
for item in $(seq 1 100); do
  reference=$(awk "NR==$item" $referenceFile)
  seedA=$(awk "NR==$item" $SEEDMA);nameA=$(basename $seedA .vcf | sed 's/\.demux//g');
  seedB=$(awk "NR==$item" $SEEDMB);nameB=$(basename $seedB .vcf | sed 's/\.demux//g');
  seedC=$(awk "NR==$item" $SEEDMC);nameC=$(basename $seedC .vcf | sed 's/\.demux//g');
  seedD=$(awk "NR==$item" $SEEDMD);nameD=$(basename $seedD .vcf | sed 's/\.demux//g');
  seedE=$(awk "NR==$item" $SEEDME);nameE=$(basename $seedE .vcf | sed 's/\.demux//g');
  seedF=$(awk "NR==$item" $SEEDMF);nameF=$(basename $seedF .vcf | sed 's/\.demux//g');
  echo $item
  echo "java -Xmx2G -jar $GATK -T VariantEval -R $reference -eval $WD/trueVariants/1/${nameA}_TRUE.demux.vcf -comp $seedA  -EV MetricsCollection -o $WD/proc/eval/methodA/1/$nameA.eval.2.grp" >>$scriptsFolder/$pipelinesName.a.18.sh
  echo "java -Xmx2G -jar $GATK -T VariantEval -R $reference -eval $WD/trueVariants/1/${nameB}_TRUE.demux.vcf -comp $seedB  -EV MetricsCollection -o $WD/proc/eval/methodF/1/$nameB.eval.2.grp" >>$scriptsFolder/$pipelinesName.a.18.sh
  echo "java -Xmx2G -jar $GATK -T VariantEval -R $reference -eval $WD/trueVariants/1/${nameC}_TRUE.demux.vcf -comp $seedC  -EV MetricsCollection -o $WD/proc/eval/methodC/1/$nameC.eval.2.grp" >>$scriptsFolder/$pipelinesName.a.18.sh
  echo "java -Xmx2G -jar $GATK -T VariantEval -R $reference -eval $WD/trueVariants/1/${nameD}_TRUE.demux.vcf -comp $seedD  -EV MetricsCollection -o $WD/proc/eval/methodD/1/$nameD.eval.2.grp" >>$scriptsFolder/$pipelinesName.a.18.sh
  echo "java -Xmx2G -jar $GATK -T VariantEval -R $reference -eval $WD/trueVariants/1/${nameE}_TRUE.demux.vcf -comp $seedE  -EV MetricsCollection -o $WD/proc/eval/methodE/1/$nameE.eval.2.grp" >>$scriptsFolder/$pipelinesName.a.18.sh
  echo "java -Xmx2G -jar $GATK -T VariantEval -R $reference -eval $WD/trueVariants/1/${nameF}_TRUE.demux.vcf -comp $seedF  -EV MetricsCollection -o $WD/proc/eval/methodF/1/$nameF.eval.2.grp" >>$scriptsFolder/$pipelinesName.a.18.sh
done
echo -e "module unload jdk-oracle/1.8.0" >>  $scriptsFolder/$pipelinesName.a.18.sh

jobID=$(qsub -l num_proc=1,s_rt=5:00:00,s_vmem=4G,h_fsize=1G,arch=haswell  $scriptsFolder/$pipelinesName.a.18.sh | awk '{ print $3}')

###############################################################################
# step 19. Extracting sensitivity test information from the  eval.grp tables.
###############################################################################
methods=("A" "B" "C" "D" "E" "F")
for met in ${methods[@]};do
  find $WD/proc/eval/method${met}/ -name "*.eval.2.grp" > $WD/report/scripts/$pipelinesName.method${met}.eval.varcalls.txt
done

for met in ${methods[@]};do
    echo "rm $WD/proc/eval/method${met}/${pipelinesName}.eval.2.summary"
    rm $WD/proc/eval/method${met}/${pipelinesName}.eval.2.summary
done

for met in ${methods[@]};do
  for line in $(cat $WD/report/scripts/$pipelinesName.method${met}.eval.2.varcalls.txt); do
    array=(${line//_/ })
    st=${array[1]}
    gt=${array[2]}
    gtarray=(${gt//\./ })
    echo "method$met - $st/${gtarray[0]}"
    values=$(cat $line | grep ValidationReport | tail -1 | awk '{print $6","$7","$8","$9","$10}')
    echo "$st,${gtarray[0]},$values" >> $WD/proc/eval/method${met}/${pipelinesName}_${st}.eval.2.summary
  done
done

<<REPORTDOWNLOAD
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/proc/eval/methodA/csSim2.eval.2.summary csSim2.ma.eval.2.summary
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/proc/eval/methodB/csSim2.eval.2.summary csSim2.mb.eval.2.summary
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/proc/eval/methodC/csSim2.eval.2.summary csSim2.mc.eval.2.summary
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/proc/eval/methodD/csSim2.eval.2.summary csSim2.md.eval.2.summary
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/proc/eval/methodE/csSim2.eval.2.summary csSim2.me.eval.2.summary
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/proc/eval/methodF/csSim2.eval.2.summary csSim2.mf.eval.2.summary
REPORTDOWNLOAD







SEEDFILE="$scriptsFolder/$pipelinesName.ma.vcftools.files"
find $procVarcalling/methodA/ -name "*.vcf" > $SEEDFILE
SEEDFILE="$scriptsFolder/$pipelinesName.mb.vcftools.files"
find $procVarcalling/methodB/ -name "*.vcf" > $SEEDFILE
SEEDFILE="$scriptsFolder/$pipelinesName.mc.vcftools.files"
find $procVarcalling/methodC/ -name "*.vcf" > $SEEDFILE
SEEDFILE="$scriptsFolder/$pipelinesName.md.vcftools.files"
find $procVarcalling/methodD/ -name "*.vcf" > $SEEDFILE


for line in $(tail -n+2  $WD/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  # mkdir -p $WD/proc/cmp/methodA/$st/$gt
  # mkdir -p $WD/proc/cmp/methodB/$st/$gt
  # mkdir -p $WD/proc/cmp/methodC/$st/$gt
  # mkdir -p $WD/proc/cmp/methodD/$st/$gt
  mkdir -p $WD/proc/cmp/methodE/$st/$gt
done
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.17a.o
#$ -e $outputFolder/$pipelinesName.a.17a.e
#$ -N cs2.a.17a
"> $scriptsFolder/$pipelinesName.a.17a.sh
for line in $(cat $WD/$pipelinesName.references.txt);do
  refText=$line
  array=(${line//:/ })
  st=${array[1]}
  prefix=${array[4]}
  gt=${array[5]}
  ref=${array[6]}
  echo "$st/$gt"
  echo "mkdir -p $WD/trueVariants/${st}/"  >> $scriptsFolder/$pipelinesName.a.16.sh
  TRUEVariants="$WD/trueVariants/${st}/${pipelinesName}_${st}_${gt}_TRUE.vcf"
  ESTIMATED="$WD/proc/varcalling/methodA/${st}/${gt}/${pipelinesName}_${st}_${gt}.vcf"
  cmp="$WD/proc/cmp/methodA/${st}/${gt}/${pipelinesName}_${st}_${gt}"
  echo "$vcftools --vcf $TRUEVariants --gzdiff $ESTIMATED --diff-site --out $cmp &> ${cmp}.out" >> $scriptsFolder/$pipelinesName.a.17a.sh
done
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.17b.o
#$ -e $outputFolder/$pipelinesName.a.17b.e
#$ -N cs2.a.17b
"> $scriptsFolder/$pipelinesName.a.17b.sh
for line in $(cat $WD/$pipelinesName.references.txt);do
  refText=$line
  array=(${line//:/ })
  st=${array[1]}
  prefix=${array[4]}
  gt=${array[5]}
  ref=${array[6]}
  echo "$st/$gt"
  echo "mkdir -p $WD/trueVariants/${st}/"  >> $scriptsFolder/$pipelinesName.a.16.sh
  TRUEVariants="$WD/trueVariants/${st}/${pipelinesName}_${st}_${gt}_TRUE.vcf"
  ESTIMATED="$WD/proc/varcalling/methodB/${st}/${gt}/${pipelinesName}_${st}_${gt}.vcf"
  cmp="$WD/proc/cmp/methodB/${st}/${gt}/${pipelinesName}_${st}_${gt}"
  echo "$vcftools --vcf $TRUEVariants --gzdiff $ESTIMATED --diff-site --out $cmp &> ${cmp}.out" >> $scriptsFolder/$pipelinesName.a.17b.sh
done

echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.17c.o
#$ -e $outputFolder/$pipelinesName.a.17c.e
#$ -N cs2.a.17c
"> $scriptsFolder/$pipelinesName.a.17c.sh
for line in $(cat $WD/$pipelinesName.references.txt);do
  refText=$line
  array=(${line//:/ })
  st=${array[1]}
  prefix=${array[4]}
  gt=${array[5]}
  ref=${array[6]}
  echo "$st/$gt"
  echo "mkdir -p $WD/trueVariants/${st}/"  >> $scriptsFolder/$pipelinesName.a.16.sh
  TRUEVariants="$WD/trueVariants/${st}/${pipelinesName}_${st}_${gt}_TRUE.vcf"
  ESTIMATED="$WD/proc/varcalling/methodC/${st}/${gt}/${pipelinesName}_${st}_${gt}.vcf"
  cmp="$WD/proc/cmp/methodC/${st}/${gt}/${pipelinesName}_${st}_${gt}"
  echo "$vcftools --vcf $TRUEVariants --gzdiff $ESTIMATED --diff-site --out $cmp &> ${cmp}.out" >> $scriptsFolder/$pipelinesName.a.17c.sh
done

echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.17d.o
#$ -e $outputFolder/$pipelinesName.a.17d.e
#$ -N cs2.a.17d
"> $scriptsFolder/$pipelinesName.a.17d.sh
for line in $(cat $WD/$pipelinesName.references.txt);do
  refText=$line
  array=(${line//:/ })
  st=${array[1]}
  prefix=${array[4]}
  gt=${array[5]}
  ref=${array[6]}
  echo "$st/$gt"
  echo "mkdir -p $WD/trueVariants/${st}/"  >> $scriptsFolder/$pipelinesName.a.16.sh
  TRUEVariants="$WD/trueVariants/${st}/${pipelinesName}_${st}_${gt}_TRUE.vcf"
  ESTIMATED="$WD/proc/varcalling/methodD/${st}/${gt}/${pipelinesName}_${st}_${gt}.vcf"
  cmp="$WD/proc/cmp/methodD/${st}/${gt}/${pipelinesName}_${st}_${gt}"
  echo "$vcftools --vcf $TRUEVariants --gzdiff $ESTIMATED --diff-site --out $cmp &> ${cmp}.out" >> $scriptsFolder/$pipelinesName.a.17d.sh
done



echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.17e.o
#$ -e $outputFolder/$pipelinesName.a.17e.e
#$ -N cs2.a.17e
"> $scriptsFolder/$pipelinesName.a.17e.sh
for line in $(cat $WD/$pipelinesName.references.txt);do
  refText=$line
  array=(${line//:/ })
  st=${array[1]}
  prefix=${array[4]}
  gt=${array[5]}
  ref=${array[6]}
  echo "$st/$gt"
  echo "mkdir -p $WD/trueVariants/${st}/"  >> $scriptsFolder/$pipelinesName.a.16.sh
  TRUEVariants="$WD/trueVariants/${st}/${pipelinesName}_${st}_${gt}_TRUE.vcf"
  ESTIMATED="$WD/proc/varcalling/methodE/${st}/${gt}/${pipelinesName}_${st}_${gt}.vcf"
  cmp="$WD/proc/cmp/methodE/${st}/${gt}/${pipelinesName}_${st}_${gt}"
  echo "$vcftools --vcf $TRUEVariants --gzdiff $ESTIMATED --diff-site --out $cmp &> ${cmp}.out" >> $scriptsFolder/$pipelinesName.a.17e.sh
done


jobID=$(qsub -l num_proc=1,s_rt=0:20:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.17a.sh | awk '{ print $3}')
jobID=$(qsub -l num_proc=1,s_rt=0:20:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.17b.sh | awk '{ print $3}')
jobID=$(qsub -l num_proc=1,s_rt=0:20:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.17c.sh | awk '{ print $3}')
jobID=$(qsub -l num_proc=1,s_rt=0:20:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.17d.sh | awk '{ print $3}')
jobID=$(qsub -l num_proc=1,s_rt=0:20:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.17e.sh | awk '{ print $3}')





################################################################################
# STEP 15. GET CONSENSUS SEQUENCES FOR REFERENCES
################################################################################

# Reference INDIVIDUALS
nInds=15
SEEDFILE="$WD/report/scripts/consensus.originals.files"
for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for item in $(seq 1 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $item)
    for ind in $(seq 0 $nInds); do
      echo "$individualsFolder/${st}/${gt}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}.fasta" >> $SEEDFILE
    done
  done
done

echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.15.o
#$ -e $outputFolder/$pipelinesName.a.15.e
#$ -N $pipelinesName.a.15

module load python/2.7.8

SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE)
echo \$SEED

python $consensus -i \$SEED -o \$(echo \$SEED | sed 's/individuals/consensus/g' |  sed 's/data/consensus/g')

module unload python/2.7.8

" >  $scriptsFolder/$pipelinesName.a.15.sh

numJobs=$(wc -l  $SEEDFILE | awk '{ print $1}')
jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-$numJobs $scriptsFolder/$pipelinesName.a.15.sh | awk '{ print $3}')









###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
# https://www.biostars.org/p/94573/
nInds=15
SEEDFILE="$WD/report/scripts/consensus.estimated.files"
for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for item in $(seq 1 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $item)
    for ind in $(seq 0 $nInds); do
      echo "$individualsFolder/${st}/${gt}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}.fasta" >> $SEEDFILE
    done
  done
done

echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.16.o
#$ -e $outputFolder/$pipelinesName.a.16.e
#$ -N $pipelinesName.a.16


$bcftools consensus -f $reference -i $vcffile > $ouput


" >  $scriptsFolder/$pipelinesName.a.16.sh

numJobs=$(wc -l  $SEEDFILE | awk '{ print $1}')
jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-$numJobs $scriptsFolder/$pipelinesName.a.16.sh | awk '{ print $3}')



for i in $(seq 0 15); do
  echo "-I csSim2_1_001_data_$i.sam"
done
java -jar /home/uvi/be/mef/apps/picard/2.2.4/picard.jar MergeSamFiles I=csSim2_1_001_data_0.sam I=csSim2_1_001_data_1.sam I=csSim2_1_001_data_2.sam I=csSim2_1_001_data_3.sam I=csSim2_1_001_data_4.sam I=csSim2_1_001_data_5.sam I=csSim2_1_001_data_6.sam I=csSim2_1_001_data_7.sam I=csSim2_1_001_data_8.sam I=csSim2_1_001_data_9.sam I=csSim2_1_001_data_10.sam I=csSim2_1_001_data_11.sam I=csSim2_1_001_data_12.sam I=csSim2_1_001_data_13.sam I=csSim2_1_001_data_14.sam I=csSim2_1_001_data_15.sam O=test.sam
samtools view -Sb test.sam > test.bam
samtools sort test.bam test.sorted
samtools index test.sorted.bam

java -jar /home/uvi/be/mef/apps/gatk/3.5.0/GenomeAnalysisTK.jar -T HaplotypeCaller -R /home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/references/REF_LOC1RND_data_1_001.fasta -I test.sorted.bam -o test.vcf
/home/uvi/be/mef/apps/bcftools/1.3.1/bcftools query -l test.vcf
