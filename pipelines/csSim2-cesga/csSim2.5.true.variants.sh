###############################################################################
# STEP 16. GET TRUE VARIANTS (NO MATED)
################################################################################

echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.16.o
#$ -e $outputFolder/$pipelinesName.a.16.e
#$ -N $pipelinesName.a.16

module load jdk-oracle/1.8.0
"> $scriptsFolder/$pipelinesName.a.16.sh

for line in $(cat $WD/$pipelinesName.references.txt);do
  refText=$line
  array=(${line//:/ })
  st=${array[1]}
  prefix=${array[4]}
  gt=${array[5]}
  ref=${array[6]}
  echo "$st/$gt"
  echo "mkdir -p $WD/trueVariants/${st}/"  >> $scriptsFolder/$pipelinesName.a.16.sh
  echo "$msa2vcf -R $refText $WD/$st/${prefix}_${gt}.fasta > $WD/trueVariants/${st}/${pipelinesName}_${st}_${gt}_TRUE.vcf" >> $scriptsFolder/$pipelinesName.a.16.sh
done

# -o
echo "module unload jdk-oracle/1.8.0" >> $scriptsFolder/$pipelinesName.a.16.sh
jobID=$(qsub -l num_proc=1,s_rt=0:20:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.16.sh | awk '{ print $3}')


################################################################################
# STEP 16d. Check for demulitplexable loci.
################################################################################
SEEDFILE="$scriptsFolder/$pipelinesName.tv.files"
find $WD/trueVariants  -name "*TRUE.vcf" >$SEEDFILE

echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.16d.o
#$ -e $outputFolder/$pipelinesName.a.16d.e
#$ -N cs2.a.16d

SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE)
basic=\$(basename \$SEED .vcf)
vcfallelicprimitives \$SEED > $WD/trueVariants/1/\$basic.demux.vcf
"> $scriptsFolder/$pipelinesName.a.16d.sh

nJobs=$(wc -l $SEEDFILE | awk '{print $1}')
jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=4G,h_fsize=1G,arch=haswell -t 1-$nJobs $scriptsFolder/$pipelinesName.a.16d.sh | awk '{ print $3}')


################################################################################
# STEP 16c Get stats on TRUE VARIANTs
################################################################################
# TRUE
statsTrueVariants="$WD/$pipelinesName.tv.stats.txt"
echo -e "FILE\tSNPs\tMNPs\tINDELS\tOTHERS\tblank\tblank2" >$statsTrueVariants
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.16c.o
#$ -e $outputFolder/$pipelinesName.a.16c.e
#$ -N cs2.a.16c
"> $scriptsFolder/$pipelinesName.a.16c.sh
for item in $(seq 1 100); do
gt=$(printf "%0$numDigitsGTs""g" $item)
TASK="$WD/trueVariants/1/${pipelinesName}_1_${gt}_TRUE.vcf"
echo -e "echo \$(echo $TASK)\\t \$($bcftools stats $TASK | grep \"^SN\" | grep \"SNPs\" | awk '{print \$6}')\\t\$($bcftools stats $TASK | grep \"^SN\" | grep \"MNPs\" | awk '{print \$6}')\\t\$($bcftools stats $TASK | grep \"^SN\" | grep \"indels\" | awk '{print \$6}')\\t\$($bcftools stats $TASK | grep \"^SN\" | grep \"others\" | awk '{print \$6}')\" >> $statsTrueVariants">> $scriptsFolder/$pipelinesName.a.16c.sh
done
jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.16c.sh | awk '{ print $3}')

echo -e "FILE\tSNPs\tMNPs\tINDELS\tOTHERS" >tmp.txt
tail -n+2 $statsTrueVariants | awk '{if (NF==7) print $1,$2,$3,$4,$5; if (NF==6) print  $2,$3,$4,$5, $6}' >> tmp.txt
cat tmp.txt >  $statsTrueVariants
rm tmp.txt

################################################################################
# TRUE DEMUX
statsTrueVariantsDemux="$WD/$pipelinesName.tv.stats.demux.txt"
echo -e "FILE\tSNPs\tMNPs\tINDELS\tOTHERS\tblank\tblank2" >$statsTrueVariantsDemux
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.16d.o
#$ -e $outputFolder/$pipelinesName.a.16d.e
#$ -N cs2.a.16d
"> $scriptsFolder/$pipelinesName.a.16d.sh
for item in $(seq 1 100); do
  gt=$(printf "%0$numDigitsGTs""g" $item)
  TASK="$WD/trueVariants/1/${pipelinesName}_1_${gt}_TRUE.demux.vcf"
  echo -e "echo \$(echo $TASK)\\t \$($bcftools stats $TASK | grep \"^SN\" | grep \"SNPs\" | awk '{print \$6}')\\t\$($bcftools stats $TASK | grep \"^SN\" | grep \"MNPs\" | awk '{print \$6}')\\t\$($bcftools stats $TASK | grep \"^SN\" | grep \"indels\" | awk '{print \$6}')\\t\$($bcftools stats $TASK | grep \"^SN\" | grep \"others\" | awk '{print \$6}')\" >> $statsTrueVariantsDemux">> $scriptsFolder/$pipelinesName.a.16d.sh
done
jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.16d.sh | awk '{ print $3}')

echo -e "FILE\tSNPs\tMNPs\tINDELS\tOTHERS" >tmp.txt
tail -n+2 $statsTrueVariantsDemux | awk '{if (NF==7) print $1,$2,$3,$4,$5; if (NF==6) print  $2,$3,$4,$5, $6}' >> tmp.txt
cat tmp.txt >  $statsTrueVariantsDemux
rm tmp.txt

<<DOWNLOAD
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/csSim2.tv.stats.txt .
DOWNLOAD

################################################################################
# Getting tables for comparison
echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.a.16e.o
#$ -e $outputFolder/$pipelinesName.a.16e.e
#$ -N cs2.a.16e

module unload jdk-oracle/1.8.0
"> $scriptsFolder/$pipelinesName.a.16e.sh

mkdir -p $WD/trueVariants/1/tables
for item in $(seq 1 $nGTs); do
  gt=$(printf "%0$numDigitsGTs""g" $item)
reference="$WD/references/REF_${prefixRef}_${prefixLoci}_1_${gt}.fasta"
inputVCF="$WD/trueVariants/1/${pipelinesName}_1_${gt}_TRUE"
outputTable="$WD/trueVariants/1/tables/${pipelinesName}_1_${gt}_TRUE"
echo -e "java -Xmx2G -jar $GATK -R $reference -T VariantsToTable -V ${inputVCF}.vcf -F POS -F REF -F ALT -AMD -o ${outputTable}.nodemux.table">> $scriptsFolder/$pipelinesName.a.16e.sh
echo -e "java -Xmx2G -jar $GATK -R $reference -T VariantsToTable -V ${inputVCF}.demux.vcf -F POS -F REF -F ALT -AMD -o ${outputTable}.demux.table">> $scriptsFolder/$pipelinesName.a.16e.sh
done

echo -e "module unload jdk-oracle/1.8.0">> $scriptsFolder/$pipelinesName.a.16e.sh
jobID=$(qsub -l num_proc=1,s_rt=2:00:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.16e.sh | awk '{ print $3}')

################################################################################
# Extracting data from tables
<<NO
[uvibemef@svg01 1]$ echo "$scriptsFolder/$pipelinesName.tv.vcf.nodemux.files"
/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/report/scripts/csSim2.tv.vcf.nodemux.files
[uvibemef@svg01 1]$ echo "$WD/trueVariants/1/$pipelinesName.tv.nc.nodemux.tables.summary"
/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/trueVariants/1/csSim2.tv.nc.nodemux.tables.summary
NO
demux="$scriptsFolder/$pipelinesName.tv.vcf.demux.files"
nodemux="$scriptsFolder/$pipelinesName.tv.vcf.nodemux.files"

find $WD/trueVariants/1/tables/ -name "*TRUE.demux.table" > $demux
find $WD/trueVariants/1/tables/ -name "*TRUE.nodemux.table" > $nodemux

echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.a.16f.o
#$ -e $outputFolder/$pipelinesName.a.16f.e
#$ -N cs2.a.16f

module load R/3.2.3

Rscript --vanilla $HOME/src/me-phylolab-conussim/variants/extractVariants.simpleData.R $scriptsFolder/$pipelinesName.tv.vcf.demux.files $WD/trueVariants/1/$pipelinesName.tv.nc.demux.tables.summary A
Rscript --vanilla $HOME/src/me-phylolab-conussim/variants/extractVariants.simpleData.R $scriptsFolder/$pipelinesName.tv.vcf.nodemux.files $WD/trueVariants/1/$pipelinesName.tv.nc.nodemux.tables.summary A

module unload R/3.2.3" > $scriptsFolder/$pipelinesName.a.16f.sh

jobID=$(qsub -l num_proc=1,s_rt=0:30:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.16f.sh | awk '{ print $3}')

<<DOWNLOAD
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/trueVariants/1/*.tables.summary .
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/trueVariants/1/*.nc.demux.tables.summary  .
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/trueVariants/1/*.nc.nodemux.tables.summary  .
DOWNLOAD


################################################################################
# STEP 16g Get stats on TRUE VARIANTs after demux
################################################################################

statsTrueVariants="$WD/$pipelinesName.tv.stats.demux.txt"

echo -e "FILE\tSNPs\tMNPs\tINDELS\tOTHERS\tblank\tblank2" >$statsTrueVariants
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.16g.o
#$ -e $outputFolder/$pipelinesName.a.16g.e
#$ -N cs2.a.16g
"> $scriptsFolder/$pipelinesName.a.16g.sh
for item in $(seq 1 100); do
gt=$(printf "%0$numDigitsGTs""g" $item)
TASK="$WD/trueVariants/1/${pipelinesName}_1_${gt}_TRUE_C.demux.vcf"
echo -e "echo \$(echo $TASK)\\t \$($bcftools stats $TASK | grep \"^SN\" | grep \"SNPs\" | awk '{print \$6}')\\t\$($bcftools stats $TASK | grep \"^SN\" | grep \"MNPs\" | awk '{print \$6}')\\t\$($bcftools stats $TASK | grep \"^SN\" | grep \"indels\" | awk '{print \$6}')\\t\$($bcftools stats $TASK | grep \"^SN\" | grep \"others\" | awk '{print \$6}')\" >> $statsTrueVariants">> $scriptsFolder/$pipelinesName.a.16g.sh
done
jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.16g.sh | awk '{ print $3}')

echo -e "FILE\tSNPs\tMNPs\tINDELS\tOTHERS" >tmp.txt
tail -n+2 csSim2.tv.stats.demux.txt | awk '{if (NF==7) print $1,$2,$3,$4,$5; if (NF==6) print  $2,$3,$4,$5, $6}' >> tmp.txt
cat tmp.txt >  csSim2.tv.stats.demux.txt
rm tmp.txt

<<DOWNLOAD
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/csSim2.tv.stats.demux.txt .
DOWNLOAD
