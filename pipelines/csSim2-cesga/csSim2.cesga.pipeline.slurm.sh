################################################################################
# (c) 2015-2016 Merly Escalona <merlyescalona@uvigo.es>
# Phylogenomics Lab. University of Vigo.
#
# Description:
# ============
# Pipeline organized to be run @CESGA's cluster.
# Version 2 to check different approaches for the selection of reference loci.
################################################################################
#!/bin/bash -l
################################################################################
# Naming Variables
################################################################################
pipelinesName="csSim2"
stReplicates=1
numDigits=${#stReplicates}
nGTs=100
numGeneTrees="f:${nGTs}"
numDigitsGTs=${#nGTs}
sizeLoci="500bp"
prefix="data"
timeRange="u:200000,20000000" # 200k to 20My
nIndsTaxa="f:6" # Number of individuals per taxa (actual num of ind will be 3)
nTaxa="f:5" # Number of taxa
subsRate="e:10000000" # exp(10^7)
prefixLoci="data"
prefixRef="LOC1RND"
################################################################################
# Program paths
################################################################################
filterEven="$HOMESVG/src/me-phylolab-conussim/conusSim.filtering.even.R"
matingProgram="$HOMESVG/src/me-phylolab-mating/mating.py"
pipelineBash="$HOMESVG/src/me-phylolab-conussim/conusSim-pipeline"
LociFiltering="$HOMESVG/src/me-phylolab-mating/LociFiltering.py"
lociReferenceSelection="$HOMESVG/src/me-phylolab-conussim/LociRefSelection/locirefsel.main.py"
art="$HOMESVG/bin/art_illumina"
simphy="$HOMESVG/bin/simphy"
indelible="$HOMESVG/bin/indelible"
wrapper="$HOMESVG/bin/INDELIble_wrapper.pl"
diversity="$HOMESVG/bin/diversity"
fastqc="$HOMESVG/apps/fastqc/0.11.5/fastqc"
bwa="$HOMESVG/apps/bwa/0.7.13/bwa"
PICARD="$HOMESVG/apps/picard/2.2.4/picard.jar"
SAMTOOLS="$HOMESVG/apps/samtools/1.3.1/samtools"
GATK="$HOMESVG/apps/gatk/3.5.0/GenomeAnalysisTK.jar"
VarScan="$HOMESVG/apps/varscan/2.3.9/VarScan.v2.3.9.jar"
extractingmapq="$HOMESVG/src/me-phylolab-conussim/mapq/extracting.mapq.R"
extractingvcftables="$HOMESVG/src/me-phylolab-conussim/variants/extracting.variant.info.R"
################################################################################
# Folders
################################################################################
#PHYLOLAB="/home/uvi/be/mef/mnt/phylolab"
WD="$PHYLOLAB/$USER/$pipelinesName"
profilePath="$HOMESVG/data/csNGSProfile"
pair1Path="$HOMESVG/data/csNGSProfile/csNGSProfile_hiseq2500_1.txt"
pair2Path="$HOMESVG/data/csNGSProfile/csNGSProfile_hiseq2500_2.txt"
outputFolder="$WD/report/output"
usageFolder="$WD/report/usage"
scriptsFolder="$WD/report/scripts"
filesFolder="$WD/report/files"
controlFolder="$WD/report/controlFiles"
jobsSent="$outputFolder/$pipelinesName.jobs"
readsFolder="$WD/reads"
alnReadsFolder="$WD/reads/aln"
fqReadsFolder="$WD/reads/fq"
samReadsFolder="$WD/reads/sam"
referencesFolder="$WD/references"
procSam="$WD/proc/sam"
procBam="$WD/proc/bam"
procSorted="$WD/proc/sorted"
procDup="$WD/proc/dup"
procMAPQ="$WD/proc/mapq"
qcFolder="$WD/report/qc"
stats="$WD/report/stats"
procVarcalling="$WD/proc/varcalling"


###############################################################################
# STEP 11. Variant calling with samtools Method F
###############################################################################

nInds=15
echo -e "#! /bin/bash
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --cpus-per-task 2
#SBATCH -t 02:00:00
#SBATCH --mem 4G
#SBATCH -J $pipelinesName.a.11f
module load jdk/1.8.0
" >  $scriptsFolder/$pipelinesName.a.11f.slurm.sh

for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for item in $(seq 70 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $item)
    for ind in $(seq 0 $nInds); do

      reference="$referencesFolder/REF_${prefixRef}_${prefixLoci}_${st}_${gt}.fasta"
      outputVCF="$procVarcalling/methodF/${st}/${gt}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}.vcf"
      outputTable="$procVarcalling/methodF/${st}/${gt}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}.table"
      inputBAM="$procSorted/${st}/${gt}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}.sorted.bam"
      echo -e "

java -jar -Xmx2G $GATK -T HaplotypeCaller  -mmq 40 -R $reference -I $inputBAM -o $outputVCF
info_fields=\$(head -5000 $outputVCF | perl -n_e '/^.*INFO.*ID=([a-zA-Z0-9_]+),/ && print \"-F \$1 \"' | uniq)
genotype_fields=\$(head -5000 $outputVCF | perl -ne '/^.*FORMAT.*ID=([a-zA-Z0-9_]+),/ && print \"-GF \$1 \"' | uniq)
java -jar $GATK -R $reference -T VariantsToTable -V $outputVCF -F POS -F REF -F ALT -F QUAL \${info_fields[@]} \${genotype_fields[@]} -AMD -o $outputTable

      "  >>  $scriptsFolder/$pipelinesName.a.11f.slurm.sh
    done
  done
done

echo -e "\nmodule unload jdk/1.8.0" >>  $scriptsFolder/$pipelinesName.a.11f.slurm.sh

sbatch $scriptsFolder/$pipelinesName.a.11f.slurm.sh


###############################################################################
find $procVarcalling/methodF -name "*.table" > $scriptsFolder/$pipelinesName.mf.vcf.table.files
echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.a.12f.o
#$ -e $outputFolder/$pipelinesName.a.12f.e
#$ -N $pipelinesName.a.12f

module load R/3.2.3

Rscript --vanilla $extractingvcftables $scriptsFolder/$pipelinesName.mf.vcf.table.files $WD/proc/varcalling/methodF/$pipelinesName.mf.vcf.table.summary E

module unload R/3.2.3" > $scriptsFolder/$pipelinesName.a.12f.sh

jobID=$(qsub -l num_proc=1,s_rt=0:30:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.12f.sh | awk '{ print $3}')
