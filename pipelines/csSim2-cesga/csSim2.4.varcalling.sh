###############################################################################
# STEP 14. Variant calling with samtools Method A
###############################################################################
# At this step I know how many individuals are created, number of loci (Gts) and
# species trees.
nInds=15
mkdir -p $procVarcalling/methodA
for line in $(tail -n+2  $WD/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  mkdir -p $procVarcalling/methodA/$st/$gt
done

echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.a.14a.o
#$ -e $outputFolder/$pipelinesName.a.14a.e
#$ -N $pipelinesName.a.14a

module load samtools jdk-oracle/1.8.0
" >  $scriptsFolder/$pipelinesName.a.14a.sh

for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for item in $(seq 1 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $item)

      reference="$referencesFolder/REF_${prefixRef}_${prefixLoci}_${st}_${gt}.fasta"
      outputVCF="$procVarcalling/methodA/${st}/${gt}/${pipelinesName}_${st}_${gt}.vcf"
      outputTable="$procVarcalling/methodA/${st}/${gt}/${pipelinesName}_${st}_${gt}.table"
      files=""
      for ind in $(seq 0 15); do
        files="$files $procSorted/${st}/${gt}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}.sorted.bam"
      done
      echo -e "
samtools mpileup -D -u -I -f $reference  $files | bcftools view -vcg - > $outputVCF
      "  >>  $scriptsFolder/$pipelinesName.a.14a.sh
    done
done
echo -e "\nmodule unload samtools jdk-oracle/1.8.0" >>  $scriptsFolder/$pipelinesName.a.14a.sh

jobID=$(qsub -l num_proc=1,s_rt=1:30:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.14a.sh | awk '{ print $3}')


###############################################################################
# STEP 14. Variant calling with samtools Method B
###############################################################################

# Sample's names
for item in $(seq 0 15); do
  echo "Sample$item" >> $WD/$pipelinesName.sample.names
done
mkdir -p $procVarcalling/methodB
for line in $(tail -n+2  $WD/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  mkdir -p $procVarcalling/methodB/$st/$gt
done

echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.a.14b.o
#$ -e $outputFolder/$pipelinesName.a.14b.e
#$ -N cs2.a.14b

module load samtools jdk-oracle/1.8.0
" >  $scriptsFolder/$pipelinesName.a.14b.sh

for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for item in $(seq 1 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $item)
    reference="$referencesFolder/REF_${prefixRef}_${prefixLoci}_${st}_${gt}.fasta"
    outputVCF="$procVarcalling/methodB/${st}/${gt}/${pipelinesName}_${st}_${gt}.vcf"
    outputTable="$procVarcalling/methodB/${st}/${gt}/${pipelinesName}_${st}_${gt}.table"
    files=""
    for ind in $(seq 0 15); do
      files="$files $procSorted/${st}/${gt}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}.sorted.bam"
    done

    echo -e "

samtools mpileup -D -I -f $reference  $files | java -jar $VarScan mpileup2snp --vcf-sample-list $WD/${pipelinesName}.sample.list --output-vcf > $outputVCF

      "  >>  $scriptsFolder/$pipelinesName.a.14b.sh
  done
done

echo -e "\nmodule unload samtools jdk-oracle/1.8.0" >>  $scriptsFolder/$pipelinesName.a.14b.sh

jobID=$(qsub -l num_proc=1,s_rt=2:00:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.14b.sh | awk '{ print $3}')


###############################################################################
# STEP 14. Variant calling with samtools Method C
###############################################################################
mkdir -p $procVarcalling/methodC
for line in $(tail -n+2  $WD/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  mkdir -p $procVarcalling/methodC/$st/$gt
done

echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.a.14c.o
#$ -e $outputFolder/$pipelinesName.a.14c.e
#$ -N cs2.a.14c

module load samtools jdk-oracle/1.8.0
" >  $scriptsFolder/$pipelinesName.a.14c.sh

for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for item in $(seq 1 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $item)
    reference="$referencesFolder/REF_${prefixRef}_${prefixLoci}_${st}_${gt}.fasta"
    outputVCF="$procVarcalling/methodC/${st}/${gt}/${pipelinesName}_${st}_${gt}.vcf"
    outputTable="$procVarcalling/methodC/${st}/${gt}/${pipelinesName}_${st}_${gt}.table"

    files=""
    for ind in $(seq 0 15); do
      files="$files $procSorted/${st}/${gt}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}.sorted.bam"
    done

    echo -e "
samtools mpileup -D -I -f $reference  $files | java -Xmx2G -jar $VarScan2 mpileup2snp --vcf-sample-list $WD/${pipelinesName}.sample.list --output-vcf  > $outputVCF
      "  >>  $scriptsFolder/$pipelinesName.a.14c.sh
  done
done

echo -e "\nmodule unload samtools jdk-oracle/1.8.0" >>  $scriptsFolder/$pipelinesName.a.14c.sh

jobID=$(qsub -l num_proc=1,s_rt=2:00:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.14c.sh | awk '{ print $3}')


###############################################################################
# STEP 14. Variant calling with samtools Method D
###############################################################################
for line in $(tail -n+2  $WD/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  mkdir -p $procVarcalling/methodD/$st/$gt
done
echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.a.14d.o
#$ -e $outputFolder/$pipelinesName.a.14d.e
#$ -N cs2.a.14d

module load samtools jdk-oracle/1.8.0
" >  $scriptsFolder/$pipelinesName.a.14d.sh

for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for item in $(seq 1 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $item)
    reference="$referencesFolder/REF_${prefixRef}_${prefixLoci}_${st}_${gt}.fasta"
    outputVCF="$procVarcalling/methodD/${st}/${gt}/${pipelinesName}_${st}_${gt}.vcf"
    outputTable="$procVarcalling/methodD/${st}/${gt}/${pipelinesName}_${st}_${gt}.table"

    files=""
    for ind in $(seq 0 15); do
      files="$files -I $procSorted/${st}/${gt}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}.sorted.bam"
    done
    echo -e "
java -jar -Xmx2G $GATK -T HaplotypeCaller -R $reference $files -o $outputVCF
    "  >>  $scriptsFolder/$pipelinesName.a.14d.sh
  done
done

echo -e "\nmodule unload samtools jdk-oracle/1.8.0" >>  $scriptsFolder/$pipelinesName.a.14d.sh

jobID=$(qsub -l num_proc=1,s_rt=2:00:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.14d.sh | awk '{ print $3}')


###############################################################################
# STEP 14. Variant calling with samtools Method E
###############################################################################
mkdir -p $procVarcalling/methodE
for line in $(tail -n+2  $WD/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  mkdir -p $procVarcalling/methodE/$st/$gt
done

<<QLOGIN
qlogin -l num_proc=1,s_rt=2:00:00,s_vmem=4G,h_fsize=1G
module load jdk-oracle/1.8.0

for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for item in $(seq 1 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $item)
    echo "$st/$gt"
    reference="$referencesFolder/REF_${prefixRef}_${prefixLoci}_${st}_${gt}.fasta"
    outputVCF="$procVarcalling/methodE/${st}/${gt}/${pipelinesName}_${st}_${gt}.vcf"
    outputTable="$procVarcalling/methodE/${st}/${gt}/${pipelinesName}_${st}_${gt}.table"
    rm $WD/report/scripts/freebayes.loc.$st.$gt.filelist
    for ind in $(seq 0 15); do
      echo "$procSorted/${st}/${gt}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}.sorted.bam" >> $WD/report/scripts/freebayes.loc.$st.$gt.filelist
    done
    $freebayes -f $reference -L $WD/report/scripts/freebayes.loc.$st.$gt.filelist > $outputVCF
  done
done

QLOGIN



###############################################################################
# STEP 14. Variant calling with samtools Method F
###############################################################################
for line in $(tail -n+2  $WD/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  mkdir -p $procVarcalling/methodF/$st/$gt
done
echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.a.14f.o
#$ -e $outputFolder/$pipelinesName.a.14f.e
#$ -N cs2.a.14f

module load samtools jdk-oracle/1.8.0
" >  $scriptsFolder/$pipelinesName.a.14f.sh

for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for item in $(seq 1 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $item)
    reference="$referencesFolder/REF_${prefixRef}_${prefixLoci}_${st}_${gt}.fasta"
    outputVCF="$procVarcalling/methodF/${st}/${gt}/${pipelinesName}_${st}_${gt}"
    echo -e "
$angsd -bam $WD/report/scripts/freebayes.loc.$st.$gt.filelist -GL 1 -doMaf 1 -doPost 1 -doMajorMinor 1 -doGlf 2 -doGeno 3 -doVCF 1 -SNP_pval 1e-6 -out $outputVCF
cd \$(dirname $outputVCF.vcf.gz)
gunzip -f $outputVCF.geno.gz
gunzip -f $outputVCF.mafs.gz
gunzip -f $outputVCF.beagle.gz
gunzip -f $outputVCF.vcf.gz
mv $outputVCF.vcf $outputVCF.vcf.bak
echo \"##fileformat=VCFv4.2\" > $outputVCF.vcf
tail -n+2 $outputVCF.vcf.bak >> $outputVCF.vcf
    "  >>  $scriptsFolder/$pipelinesName.a.14f.sh
  done
done

echo -e "\nmodule unload samtools jdk-oracle/1.8.0" >>  $scriptsFolder/$pipelinesName.a.14f.sh

jobID=$(qsub -l num_proc=1,s_rt=2:00:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.14f.sh | awk '{ print $3}')

################################################################################
# STEP 15b. DEMULTIPLEXING
################################################################################

echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.a.15b.o
#$ -e $outputFolder/$pipelinesName.a.15b.e
#$ -N cs2.a.15b

module load samtools jdk-oracle/1.8.0
" >  $scriptsFolder/$pipelinesName.a.15b.sh

methods=("A" "B" "C" "D" "E" "F")
for met in ${methods[@]};do
  for line in $(cat $WD/$pipelinesName.evens); do
    st=$(printf "%0$numDigits""g" $line)
    for item in $(seq 1 $nGTs); do
      gt=$(printf "%0$numDigitsGTs""g" $item)
      echo -e "\e[1A\r M$met - $st/$gt"
      outputVCF="$procVarcalling/method${met}/${st}/${gt}/${pipelinesName}_${st}_${gt}"
        echo -e "vcfallelicprimitives $outputVCF.vcf > $outputVCF.demux.vcf" >>  $scriptsFolder/$pipelinesName.a.15b.sh
    done
  done
done
echo -e "\nmodule unload samtools jdk-oracle/1.8.0" >>  $scriptsFolder/$pipelinesName.a.15b.sh
jobID=$(qsub -l num_proc=1,s_rt=3:00:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.15b.sh | awk '{ print $3}')


###############################################################################
# STEP 12. Summary of the VCF tables
###############################################################################
# Get summary of th VCF files
echo -e "CHROM\tPOS\tREF\tALT" > $WD/${pipelinesName}.angsd.vcf
find $WD/proc/varcalling/methodF/ -name "*.vcf" | xargs cat | grep -v "^#" | awk '{print $1,$2,$4,$5}' >> $WD/${pipelinesName}.angsd.vcf

# Get all information from the MAFS files
echo -e "chromo\tposition\tmajor\tminor\tknownEM\tpK-EM\tnInd" > $WD/${pipelinesName}.angsd.mafs
find $WD/proc/varcalling/methodF/ -name "*.mafs" | xargs cat | grep -v "^chromo" >> $WD/${pipelinesName}.angsd.mafs

################################################################################
# STEP 15. SELECTING SNPS
################################################################################
echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -e $outputFolder/$pipelinesName.a.15.e
#$ -N cs2.a.15

module load samtools jdk-oracle/1.8.0
" >  $scriptsFolder/$pipelinesName.a.15.sh

methods=("A" "B" "C" "D" "E" "F")
for met in ${methods[@]};do
  for line in $(cat $WD/$pipelinesName.evens); do
    st=$(printf "%0$numDigits""g" $line)
    for item in $(seq 1 $nGTs); do
      gt=$(printf "%0$numDigitsGTs""g" $item)
      echo -e "\e[1A\r M$met - $st/$gt"
      reference="$referencesFolder/REF_${prefixRef}_${prefixLoci}_${st}_${gt}.fasta"
      outputVCF="$procVarcalling/method${met}/${st}/${gt}/${pipelinesName}_${st}_${gt}"
      outputTable="$procVarcalling/method${met}/${st}/${gt}/${pipelinesName}_${st}_${gt}"
      echo -e "
      java -jar $GATK -R $reference -T SelectVariants -V ${outputVCF}.demux.vcf  -o ${outputVCF}.demux.snp.vcf -selectType SNP
      java -jar $GATK -R $reference -T SelectVariants -V ${outputVCF}.demux.vcf  -o ${outputVCF}.demux.indels.vcf -selectType INDEL
      info_fields=\$(head -5000 ${outputVCF}.demux.vcf | perl -ne '/^.*INFO.*ID=([a-zA-Z0-9_]+),/ && print \"-F \$1 \"' | uniq)
      genotype_fields=\$(head -5000 ${outputVCF}.demux.vcf | perl -ne '/^.*FORMAT.*ID=([a-zA-Z0-9_]+),/ && print \"-GF \$1 \"' | uniq)
      java -jar $GATK -R $reference -T VariantsToTable -V ${outputVCF}.demux.vcf -F POS -F REF -F ALT -F QUAL -F TYPE -F HOM-REF -F HET -F HOM-VAR -F NO-CALL -F VAR -F NCALLED -F NSAMPLES -F MULTI-ALLELIC \${info_fields[@]} \${genotype_fields[@]} -AMD -o $outputTable.demux.table
      java -jar $GATK -R $reference -T VariantsToTable -V ${outputVCF}.demux.snp.vcf -F POS -F REF -F ALT -F QUAL -F TYPE -F HOM-REF -F HET -F HOM-VAR -F NO-CALL -F VAR -F NCALLED -F NSAMPLES -F MULTI-ALLELIC \${info_fields[@]} \${genotype_fields[@]} -AMD -o $outputTable.demux.snp.table
      java -jar $GATK -R $reference -T VariantsToTable -V ${outputVCF}.demux.indels.vcf -F POS -F REF -F ALT -F QUAL -F TYPE -F HOM-REF -F HET -F HOM-VAR -F NO-CALL -F VAR -F NCALLED -F NSAMPLES -F MULTI-ALLELIC \${info_fields[@]} \${genotype_fields[@]} -AMD -o $outputTable.demux.indels.table
      "  >>  $scriptsFolder/$pipelinesName.a.15.sh
    done
  done
done

echo -e "\nmodule unload samtools jdk-oracle/1.8.0" >>  $scriptsFolder/$pipelinesName.a.15.sh
jobID=$(qsub -l num_proc=1,s_rt=6:00:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.15.sh | awk '{ print $3}')


###############################################################################
# STEP 12. Summary of the VCF tables
###############################################################################
find $procVarcalling/methodA -name "*demux.table" > $scriptsFolder/$pipelinesName.ma.vcf.table.files
find $procVarcalling/methodB -name "*demux.table" > $scriptsFolder/$pipelinesName.mb.vcf.table.files
find $procVarcalling/methodC -name "*demux.table" > $scriptsFolder/$pipelinesName.mc.vcf.table.files
find $procVarcalling/methodD -name "*demux.table" > $scriptsFolder/$pipelinesName.md.vcf.table.files
find $procVarcalling/methodE -name "*demux.table" > $scriptsFolder/$pipelinesName.me.vcf.table.files
find $procVarcalling/methodF -name "*demux.table" > $scriptsFolder/$pipelinesName.mf.vcf.table.files

echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.a.12.o
#$ -e $outputFolder/$pipelinesName.a.12.e
#$ -N $pipelinesName.a.12

module load R/3.2.3

Rscript --vanilla $HOME/src/me-phylolab-conussim/variants/extractVariants.simpleData.GT.R $scriptsFolder/$pipelinesName.ma.vcf.table.files $WD/proc/varcalling/methodA/$pipelinesName.ma.vcf.demux.table.summary A
Rscript --vanilla $HOME/src/me-phylolab-conussim/variants/extractVariants.simpleData.GT.R $scriptsFolder/$pipelinesName.mb.vcf.table.files $WD/proc/varcalling/methodB/$pipelinesName.mb.vcf.demux.table.summary B
Rscript --vanilla $HOME/src/me-phylolab-conussim/variants/extractVariants.simpleData.GT.R $scriptsFolder/$pipelinesName.mc.vcf.table.files $WD/proc/varcalling/methodC/$pipelinesName.mc.vcf.demux.table.summary C
Rscript --vanilla $HOME/src/me-phylolab-conussim/variants/extractVariants.simpleData.GT.R $scriptsFolder/$pipelinesName.md.vcf.table.files $WD/proc/varcalling/methodD/$pipelinesName.md.vcf.demux.table.summary D
Rscript --vanilla $HOME/src/me-phylolab-conussim/variants/extractVariants.simpleData.GT.R $scriptsFolder/$pipelinesName.me.vcf.table.files $WD/proc/varcalling/methodE/$pipelinesName.me.vcf.demux.table.summary D
Rscript --vanilla $HOME/src/me-phylolab-conussim/variants/extractVariants.angsd.GT.R $scriptsFolder/$pipelinesName.mf.vcf.table.files $WD/proc/varcalling/methodF/$pipelinesName.mf.vcf.demux.table.summary D

module unload R/3.2.3" > $scriptsFolder/$pipelinesName.a.12.sh

jobID=$(qsub -l num_proc=1,s_rt=0:30:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.12.sh | awk '{ print $3}')

<<ANGSD
filename="/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/report/scripts/csSim2.mf.vcf.table.files"
output="/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/proc/varcalling/methodF/csSim2.mf.vcf.demux.table.summary"
ANGSD
# Method F has been done by hand! in a QLOGIN
<<DOWNLOAD
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/proc/varcalling/methodA/csSim2.ma.vcf.demux.table.summary .
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/proc/varcalling/methodB/csSim2.mb.vcf.demux.table.summary .
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/proc/varcalling/methodC/csSim2.mc.vcf.demux.table.summary .
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/proc/varcalling/methodD/csSim2.md.vcf.demux.table.summary .
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/proc/varcalling/methodE/csSim2.me.vcf.demux.table.summary .
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/proc/varcalling/methodF/csSim2.mf.vcf.demux.table.summary .

scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/trueVariants/1/csSim2.tv.nc.nodemux.tables.summary .
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/trueVariants/1/csSim2.tv.nc.demux.tables.summary .
DOWNLOAD


#####################################################################
# Stats from vcf types
#####################################################################
echo -e "MET\tST\tGT\tSNPs\tMNPs\tINDELS\tOTHERS\tblank\tblank2" > $WD/$pipelinesName.varcall.stats
echo -e "MET\tST\tGT\tSNPs\tMNPs\tINDELS\tOTHERS\tblank\tblank2" > $WD/$pipelinesName.varcall.demux.stats
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.16b.o
#$ -e $outputFolder/$pipelinesName.a.16b.e
#$ -N cs2.a.16b
"> $scriptsFolder/$pipelinesName.a.16b.sh

methods=("A" "B" "C" "D" "E" "F")
for met in ${methods[@]};do
  for line in $(cat $WD/$pipelinesName.evens); do
    st=$(printf "%0$numDigits""g" $line)
    for item in $(seq 1 $nGTs); do
      gt=$(printf "%0$numDigitsGTs""g" $item)
      echo -e "\e[1A\r M$met - $st/$gt"
      TASK="$WD/proc/varcalling/method${met}/$st/$gt/${pipelinesName}_${st}_${gt}"
      echo -e "echo \"\$(echo $met)\\t\$(echo $st)\\t\$(echo $gt)\"\\t \$($bcftools stats $TASK.vcf   | grep \"^SN\" | grep \"SNPs\" | awk '{print \$6}')\\t\$($bcftools stats $TASK.vcf        | grep \"^SN\" | grep \"MNPs\" | awk '{print \$6}')\\t\$($bcftools stats $TASK.vcf        | grep \"^SN\" | grep \"indels\" | awk '{print \$6}')\\t\$($bcftools stats $TASK.vcf        | grep \"^SN\" | grep \"others\" | awk '{print \$6}')\" >> $WD/$pipelinesName.varcall.stats" >> $scriptsFolder/$pipelinesName.a.16b.sh
    done
  done
done

jobID=$(qsub -l num_proc=1,s_rt=1:00:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.16b.sh | awk '{ print $3}')

echo -e "MET\tST\tGT\tSNPs\tMNPs\tINDELS\tOTHERS" >tmp.txt
tail -n+2 $WD/$pipelinesName.varcall.stats | awk 'NR!=1{if (NF==9) print $1,$2,$3,$4,$5,$6,$7; if (NF==8) print  $2,$3,$4,$5,$6,$7,$8}' >> tmp.txt
cat tmp.txt >  $WD/$pipelinesName.varcall.stats


echo -e "MET\tST\tGT\tSNPs\tMNPs\tINDELS\tOTHERS\tblank\tblank2" > $WD/$pipelinesName.varcall.demux.stats
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.16a.o
#$ -e $outputFolder/$pipelinesName.a.16a.e
#$ -N cs2.a.16a
"> $scriptsFolder/$pipelinesName.a.16a.sh

methods=("A" "B" "C" "D" "E" "F")
for met in ${methods[@]};do
  for line in $(cat $WD/$pipelinesName.evens); do
    st=$(printf "%0$numDigits""g" $line)
    for item in $(seq 1 $nGTs); do
      gt=$(printf "%0$numDigitsGTs""g" $item)
      echo -e "\e[1A\r M$met - $st/$gt"
      TASK="$WD/proc/varcalling/method${met}/$st/$gt/${pipelinesName}_${st}_${gt}"
      echo -e "echo \"\$(echo $met)\\t\$(echo $st)\\t\$(echo $gt)\"\\t \$($bcftools stats $TASK.demux.vcf | grep \"^SN\" | grep \"SNPs\" | awk '{print \$6}')\\t\$($bcftools stats $TASK.demux.vcf  | grep \"^SN\" | grep \"MNPs\" | awk '{print \$6}')\\t\$($bcftools stats $TASK.demux.vcf  | grep \"^SN\" | grep \"indels\" | awk '{print \$6}')\\t\$($bcftools stats $TASK.demux.vcf  | grep \"^SN\" | grep \"others\" | awk '{print \$6}')\" >> $WD/$pipelinesName.varcall.demux.stats" >> $scriptsFolder/$pipelinesName.a.16a.sh
    done
  done
done
jobID=$(qsub -l num_proc=1,s_rt=1:00:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.16a.sh | awk '{ print $3}')

echo -e "MET\tST\tGT\tSNPs\tMNPs\tINDELS\tOTHERS" >tmp.txt
tail -n+2 $WD/$pipelinesName.varcall.demux.stats | awk 'NR!=1{if (NF==9) print $1,$2,$3,$4,$5,$6,$7; if (NF==8) print  $2,$3,$4,$5,$6,$7,$8}' >> tmp.txt
cat tmp.txt >  $WD/$pipelinesName.varcall.demux.stats



<<DOWNLOAD
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/csSim2.varcall.stats .
scp cesga-svg:/home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/csSim2.varcall.demux.stats .
DOWNLOAD
