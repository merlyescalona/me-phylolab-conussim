#!/bin/bash -l
################################################################################
# (c) 2015-2016 Merly Escalona <merlyescalona@uvigo.es>
# Phylogenomics Lab. University of Vigo.
#
# Description:
# ============
# Pipeline organized to be run @CESGA's cluster.
# Version 2 to check different approaches for the selection of reference loci.
################################################################################
################################################################################
################################################################################
#######                               _           _                      #######
#######             /\               | |         (_)                     #######
#######            /  \   _ __   __ _| |_   _ ___ _ ___                  #######
#######           / /\ \ | '_ \ / _` | | | | / __| / __|                 #######
#######          / ____ \| | | | (_| | | |_| \__ \ \__ \                 #######
#######         /_/    \_\_| |_|\__,_|_|\__, |___/_|___/                 #######
#######                                 __/  |                           #######
#######                                 |___/                            #######
################################################################################
################################################################################
# Creating folder for processing

mkdir -p $procBam $procSam $procSorted $procDup
for line in $(tail -n+2  $WD/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  mkdir -p $procSam/$st/$gt $procBam/$st/$gt $procSorted/$st/$gt $procDup/$st/$gt
  mkdir -p $procMAPQ/$st/$gt $procVarcalling/$st/$gt  $consensusFolder/$st/$gt
done


###############################################################################
# Mapping with BWA
###############################################################################
# STEP 1 Indexing the reference sequences
###############################################################################
SEEDFILE="$WD/report/scripts/${pipelinesName}.a.1.files"
find $referencesFolder -name "*.fasta" > $SEEDFILE

echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.1.o
#$ -e $outputFolder/$pipelinesName.a.1.e
#$ -N $pipelinesName.a.1

SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE)
cd $referencesFolder
$bwa index -a is \$SEED

">   $scriptsFolder/$pipelinesName.a.1.sh


totalIndexesFiles=$(wc -l $SEEDFILE | awk '{print $1}')
jobID=$(qsub -l num_proc=1,s_rt=0:10:00,s_vmem=4G,h_fsize=2G,arch=haswell -t 1-$totalIndexesFiles $scriptsFolder/$pipelinesName.a.1.sh | awk '{ print $3}')

###############################################################################
###############################################################################
# Sample names
for line in $(seq 0 15); do
  echo "I.$(printf "%02g" ${line})" >> csSim2.sample.list
done
###############################################################################
# STEP 2 Getting alignments
# Working with Illumina PE reads longer than 70 bp -> use bwa mem
# Always preferrably by individuals
# WARNING!!! Everything is being run in the same script and for qsubbing
# it will be necessary to estimate the time the job will take to run all the
# bwa mem.
###############################################################################

echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.a.2.o
#$ -e $outputFolder/$pipelinesName.a.2.e
#$ -N $pipelinesName.a.2

"  > $scriptsFolder/$pipelinesName.a.2.sh

counter=1
totalNumR1Files=$(find $readsFolder/fq/ -name *_R1.fq | wc -l |awk '{ print $1}')
for line in $(tail -n+2  $WD/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  echo "$counter/$totalNumR1Files"
  ref="$referencesFolder/REF_${prefixRef}_${prefixLoci}_${st}_${gt}.fasta"
  infile1="$readsFolder/fq/${st}/${gt}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}_"
  outfile1="$procSam/${st}/${gt}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}.sam"
  RGID=$(printf "%0${#totalNumR1Files}""g" $counter)
  SMID="S.${st}-G.${gt}-I.$(printf "%02g" ${ind})"
  echo "bwa mem -t 1 -R \"@RG\tID:${RGID}\tSM:${SMID}\tPL:Illumina\tLB:${RGID}\tPU:HiSeq2500\" ${ref} ${infile1}R1.fq ${infile1}R2.fq > $outfile1" >> $scriptsFolder/$pipelinesName.a.2.sh
  let counter=counter+1
done

jobID=$(qsub -l num_proc=1,s_rt=3:00:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.2.sh | awk '{ print $3}')

###############################################################################
# STEP 3. Conversion SAM2BAM and sorting.
###############################################################################

SEEDFILE="$scriptsFolder/$pipelinesName.a.3.files"
find $procSam -name *.sam | grep -v "merged" > $SEEDFILE
totalSamtoolsJobs=$(wc -l $SEEDFILE | awk '{print $1}')
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.3.o
#$ -e $outputFolder/$pipelinesName.a.3.e
#$ -N $pipelinesName.a.3
module load samtools
SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE)
OUTPUTBASE=\$(echo \$SEED | sed 's/proc\/sam/proc\/sorted/g' | sed 's/\.sam/\.sorted/g')

echo \$OUTPUTBASE
samtools view -bSh \$SEED | samtools sort - \$OUTPUTBASE

samtools index \${OUTPUTBASE}.bam
module unload samtools
"  > $scriptsFolder/$pipelinesName.a.3.sh

jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-$totalSamtoolsJobs $scriptsFolder/$pipelinesName.a.3.sh | awk '{ print $3}')

###############################################################################
# STEP 4. Geetting number of mapped reads per file
# http://crazyhottommy.blogspot.com.es/2013/06/count-how-many-mapped-reads-in-bam-file.html
###############################################################################
echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.a.4.o
#$ -e $outputFolder/$pipelinesName.a.4.e
#$ -N $pipelinesName.a.4

module load samtools

for line in \$(find $WD/proc/sorted -name *.sorted.bam ); do
  mappedReads=\$(samtools view -c -F 4 \$line)
  unmappedReads=\$(samtools view -c -f 4 \$line)
  echo \"\$line\\t\$mappedReads\" >> $WD/${pipelinesName}.mapped.reads.txt
  echo \"\$line\\t\$unmappedReads\" >> $WD/${pipelinesName}.unmapped.reads.txt
done

module unload samtools

" >  $scriptsFolder/$pipelinesName.a.4.sh
jobID=$(qsub -l num_proc=1,s_rt=0:20:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.4.sh | awk '{ print $3}')

###############################################################################
# STEP 5. Mark Duplicates
# Important to load jdk module and explicitly specify amount of memory to be used
###############################################################################
SEEDFILE="$scriptsFolder/$pipelinesName.a.5.files"
OUTFILES="$scriptsFolder/$pipelinesName.a.5.out.files"
DUPFILES="$scriptsFolder/$pipelinesName.a.5.dup.files"
find $procSorted -name *.sorted.bam > $SEEDFILE
cat $SEEDFILE | sed 's/proc\/sorted/proc\/dup/g' | sed 's/\.sorted\.bam/\.dup\.bam/g' > $OUTFILES
cat $OUTFILES | sed 's/proc\/sorted/proc\/dup/g' | sed 's/\.dup\.bam/\.dup\.txt/g' > $DUPFILES

totalLines=$(wc -l $SEEDFILE | awk '{print $1}')

# THIS HAS BEEN MADE THROUGH QLOGIN

echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.a.4.1.o
#$ -e $outputFolder/$pipelinesName.a.4.1.e
#$ -N $pipelinesName.a.4.1
module load jdk-oracle/1.8.0
"> $scriptsFolder/$pipelinesName.a.4.1.sh

for item in $(seq 1 $totalLines);do
  echo -e "
  SEED=\"$(awk "NR==$item" $SEEDFILE)\"
  OUT=\"$(awk "NR==$item" $OUTFILES)\"
  SAMPLE=\"$(awk "NR==$item" $DUPFILES)\"
  java -Xmx2G -jar $PICARD MarkDuplicates I=\${SEED} O=\${OUT} CREATE_INDEX=true M=\${SAMPLE} VALIDATION_STRINGENCY=LENIENT
  ">> $scriptsFolder/$pipelinesName.a.4.1.sh
done

echo -e "module unload jdk-oracle/1.8.0" >> $scriptsFolder/$pipelinesName.a.4.1.sh
jobID=$(qsub -l num_proc=1,s_rt=2:00:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.4.1.sh | awk '{ print $3}')

<<QLOGIN
qlogin -l num_proc=1,s_rt=1:45:00,s_vmem=4G,h_fsize=1G,arch=haswell
module load jdk-oracle/1.8.0
for item in $(seq 1 $totalLines);do
  SEED=$(awk "NR==$item" $SEEDFILE)
  OUT=$(awk "NR==$item" $scriptsFolder/$pipelinesName.a.5.out.files)
  SAMPLE=$(awk "NR==$item" $scriptsFolder/$pipelinesName.a.5.dup.files)
  java -Xmx2G -jar $PICARD MarkDuplicates I=${SEED} O=${OUT} CREATE_INDEX=true M=${SAMPLE} VALIDATION_STRINGENCY=LENIENT
done
QLOGIN

# How to extract duplicates info from prev step.

find $procDup -name "*.txt" > tmp.txt
dupResultsSum="$WD/${pipelinesName}.dup.summary"
header="ID,LIBRARY\tUNPAIRED_READS_EXAMINED\tREAD_PAIRS_EXAMINED\tSECONDARY_OR_SUPPLEMENTARY_RDS\tUNMAPPED_READS\tUNPAIRED_READ_DUPLICATES\tREAD_PAIR_DUPLICATES\tREAD_PAIR_OPTICAL_DUPLICATES\tPERCENT_DUPLICATION\tESTIMATED_LIBRARY_SIZE"
echo -e $header > tmp2.txt
counter=1
for item in $(cat tmp.txt); do
  echo $counter
  line=$(cat $item | grep -A2 "METRICS CLASS" | tail -1)
  echo -e "$(basename $item .dup.txt)\t$line" >>tmp2.txt
  let counter=counter+1
done
cat tmp2.txt | sed 's/\t/,/g' > $dupResultsSum
rm tmp.txt tmp2.txt

###############################################################################
# STEP 6. Checking coverage
###############################################################################
# Get all the coverage information for all individuals pero reference loci

SEEDFILE="$scriptsFolder/$pipelinesName.a.6.coverage.files"
for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for item in $(seq 1 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $item)
    echo "$procSorted/${st}/${gt}/" >> $SEEDFILE
  done
done

for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for item in $(seq 1 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $item)
    find $procSorted/${st}/${gt} -name "*.bam" > $coverageFolder/coverage.files.${st}.${gt}.txt
  done
done

# Order of lines in this file "$scriptsFolder/$pipelinesName.a.6.coverage.files"
# is the number of jobs that will be obtained as output in the coverage folder

echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.6.o
#$ -e $outputFolder/$pipelinesName.a.6.e
#$ -N $pipelinesName.a.6

SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE)

module load samtools
samtools depth \$(find \${SEED} -name *.bam) > $WD/report/coverage/coverage.\$SGE_TASK_ID.txt
module unload samtools

" >  $scriptsFolder/$pipelinesName.a.6.sh

numJobs=$(wc -l  $SEEDFILE | awk '{ print $1}')
jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-$numJobs $scriptsFolder/$pipelinesName.a.6.sh | awk '{ print $3}')


###############################################################################
# STEP 7. Quality map distribution
#         Computation
###############################################################################
SEEDFILE="$scriptsFolder/$pipelinesName.a.7.files"
find $procSorted -name *.bam > $SEEDFILE
totalSamtoolsJobs=$(wc -l $SEEDFILE | awk '{print $1}')

echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.7.o
#$ -e $outputFolder/$pipelinesName.a.7.e
#$ -N $pipelinesName.a.7

module load samtools
SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE)
OUTPUTBASE=\$(echo \$SEED | sed 's/proc\/sorted/proc\/mapq/g' | sed 's/\.bam/\.csv/g')

echo \$OUTPUTBASE
samtools view \$SEED | awk '{print \$1\",\"\$5}' > \$OUTPUTBASE

module unload samtools" > $scriptsFolder/$pipelinesName.a.7.sh

jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-$totalSamtoolsJobs $scriptsFolder/$pipelinesName.a.7.sh | awk '{ print $3}')


###############################################################################
# STEP 8. Extracting summary of mapq distributions
# Getting all the output files of the MAPQ into a single file
###############################################################################

find $procMAPQ -name "*.csv" > $scriptsFolder/$pipelinesName.mapq.files
<<FAST_CHECK
for item in $(cat test.mapq ); do
  numErrors=$(cat $item |grep -v -n ",60" | wc -l);
  if [ $numErrors -gt 1 ]; then
    echo $item >> $scriptsFolder/$pipelinesName.mapq.files
  fi
done
FAST_CHECK

echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.a.8.o
#$ -e $outputFolder/$pipelinesName.a.8.e
#$ -N $pipelinesName.a.8

module load R/3.2.3

Rscript --vanilla $extractingmapq $scriptsFolder/$pipelinesName.mapq.files $WD/$pipelinesName.mapq.summary

module unload R/3.2.3" > $scriptsFolder/$pipelinesName.a.8.sh

jobID=$(qsub -l num_proc=1,s_rt=0:30:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.8.sh | awk '{ print $3}')

###############################################################################
# STEP 9. indexing reference files
###############################################################################

SEEDFILE="$WD/report/scripts/references.files"
find $referencesFolder -name "*.fasta" > $SEEDFILE
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.9.o
#$ -e $outputFolder/$pipelinesName.a.9.e
#$ -N $pipelinesName.a.9

SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE)

module load samtools

samtools faidx \$SEED

module unload samtools

" >  $scriptsFolder/$pipelinesName.a.9.sh

numJobs=$(wc -l  $SEEDFILE | awk '{ print $1}')
jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-$numJobs $scriptsFolder/$pipelinesName.a.9.sh | awk '{ print $3}')



###############################################################################
## Probably Everything thats under this line is not necessary

###############################################################################
# STEP 10. Merging bam files
###############################################################################
nInds=15
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.10.o
#$ -e $outputFolder/$pipelinesName.a.10.e
#$ -N $pipelinesName.a.10

module load samtools jdk-oracle/1.8.0
" >  $scriptsFolder/$pipelinesName.a.10.sh

for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for item in $(seq 1 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $item)

ref="$referencesFolder/REF_${prefixRef}_${prefixLoci}_${st}_${gt}.fasta"

inputFiles=()
for i in $(seq 0 ${nInds}); do
  inputFiles="$inputFiles I=${pipelinesName}_${st}_${gt}_${prefixLoci}_$i.sam"
done

echo -e "
cd $procSam/${st}/${gt}/

java -Xmx4G -jar $PICARD MergeSamFiles $inputFiles O=$procSam/${st}/${gt}/${pipelinesName}_${st}_${gt}.merged.sam


" >>  $scriptsFolder/$pipelinesName.a.10.sh
done
done

echo -e "module unload samtools jdk-oracle/1.8.0" >>  $scriptsFolder/$pipelinesName.a.10.sh

jobID=$(qsub -l num_proc=1,s_rt=3:00:00,s_vmem=6G,h_fsize=2G,arch=haswell  $scriptsFolder/$pipelinesName.a.10.sh | awk '{ print $3}')

###############################################################################
# STEP 11. Conversion to BAM
###############################################################################

SEEDFILE="$scriptsFolder/$pipelinesName.a.11.files"
find $procSam -name *.merged.sam > $SEEDFILE
totalSamtoolsJobs=$(wc -l $SEEDFILE | awk '{print $1}')

echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.11.o
#$ -e $outputFolder/$pipelinesName.a.11.e
#$ -N $pipelinesName.a.11

module load samtools
SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE)
OUTPUTBASE=\$(echo \$SEED | sed 's/proc\/sam/proc\/sorted/g' | sed 's/\.sam/\.bam/g')

samtools view -Sb \$SEED -o \$OUTPUTBASE

module unload samtools" > $scriptsFolder/$pipelinesName.a.11.sh

jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-$totalSamtoolsJobs $scriptsFolder/$pipelinesName.a.11.sh | awk '{ print $3}')


###############################################################################
# STEP 12. Sort
###############################################################################

SEEDFILE="$scriptsFolder/$pipelinesName.a.12.files"
find $procSorted -name *.merged.bam > $SEEDFILE
totalSamtoolsJobs=$(wc -l $SEEDFILE | awk '{print $1}')

echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.12.o
#$ -e $outputFolder/$pipelinesName.a.12.e
#$ -N $pipelinesName.a.12

module load samtools
SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE)
OUTPUTBASE=\$(echo \$SEED | sed 's/\.bam/\.sorted/g')

samtools sort \$SEED \$OUTPUTBASE


module unload samtools" > $scriptsFolder/$pipelinesName.a.12.sh

jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-$totalSamtoolsJobs $scriptsFolder/$pipelinesName.a.12.sh | awk '{ print $3}')


###############################################################################
# STEP 13. Sort
###############################################################################
SEEDFILE="$scriptsFolder/$pipelinesName.a.13.files"
find $procSorted -name *.merged.sorted.bam > $SEEDFILE
totalSamtoolsJobs=$(wc -l $SEEDFILE | awk '{print $1}')

echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.13.o
#$ -e $outputFolder/$pipelinesName.a.13.e
#$ -N $pipelinesName.a.13

module load samtools
SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE)

samtools index \$SEED

module unload samtools" > $scriptsFolder/$pipelinesName.a.13.sh

jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-$totalSamtoolsJobs $scriptsFolder/$pipelinesName.a.13.sh | awk '{ print $3}')
