################################################################################
# STEP 15. SELECTING SNPS
################################################################################
methods=("A" "B" "C" "D" "E" "F")

for met in ${methods[@]};do
echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -e $outputFolder/$pipelinesName.a.18.${met}.e
#$ -N cs2-18${met}

module load samtools jdk-oracle/1.8.0
" >  $scriptsFolder/$pipelinesName.a.18.${met}.sh

  for line in $(cat $WD/$pipelinesName.evens); do
    st=$(printf "%0$numDigits""g" $line)
    for item in $(seq 1 $nGTs); do
      gt=$(printf "%0$numDigitsGTs""g" $item)
      echo -e "\e[1A\r M$met - $st/$gt"
      reference="$referencesFolder/REF_${prefixRef}_${prefixLoci}_${st}_${gt}.fasta"
      outputVCF="$procVarcalling/method${met}/${st}/${gt}/${pipelinesName}_${st}_${gt}"
      outputTable="$procVarcalling/method${met}/${st}/${gt}/${pipelinesName}_${st}_${gt}"
      echo -e "
      gunzip ${outputVCF}.demux.vcf.gz
      java -jar $GATK -R $reference -T VariantsToTable -V ${outputVCF}.demux.vcf -F POS -F REF -F ALT -F QUAL -F TYPE -F HOM-REF -F HET -F HOM-VAR -F NO-CALL -F VAR -F NCALLED -F NSAMPLES -F MULTI-ALLELIC -GF GT -GF GQ -GF DP -GF RD -GF AD -AMD -o $outputTable.demux.v2.table
      "  >>  $scriptsFolder/$pipelinesName.a.18.${met}.sh
    done
  done
  echo -e "\nmodule unload samtools jdk-oracle/1.8.0" >>  $scriptsFolder/$pipelinesName.a.18.${met}.sh
  qsub -l num_proc=1,s_rt=6:00:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.18.${met}.sh
done

methods=("A" "B" "C" "D" "E" "F")
for met in ${methods[@]};do
  mkdir -p "checkInds/method${met}"
done

PHYLOLAB="/home/uvi/be/mef/mnt/phylolab"
WD="$PHYLOLAB/uvibemef/csSim2"
procVarcalling="$WD/proc/varcalling"
pipelinesName="csSim2"
for met in ${methods[@]};do
  for line in $(cat csSim2.evens); do
    st=$(printf "%01""g" $line)
    for item in $(seq 1 100); do
      gt=$(printf "%03""g" $item)
      outputVCF="$procVarcalling/method${met}/${st}/${gt}/${pipelinesName}_${st}_${gt}"
      echo "scp uvibemef@svg.cesga.es:${outputVCF}.demux.v2.table" "checkInds/method${met}/" >> download.sh

    done
  done
done


ffff
