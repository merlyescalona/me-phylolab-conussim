################################################################################
# STEP 16. GET TRUE VARIANTS (MATED)
################################################################################
mkdir -p $WD/consensus/$st/all/
for line in $(cat $WD/$pipelinesName.evens);do
  st=$(printf "%0$numDigits""g" $line)
  for item in $(seq 1 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $item)
    filesCN=()
    for ind in $(seq 0 $nInds);do
      cat "$WD/consensus/$st/$gt/${pipelinesName}_${st}_${gt}_consensus_${ind}.fasta" >> $WD/consensus/$st/all/${pipelinesName}_${st}_${gt}_consensus.fasta
    done
  done
done

echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.16b.o
#$ -e $outputFolder/$pipelinesName.a.16b.e
#$ -N cs2.a.16b

module load jdk-oracle/1.8.0
"> $scriptsFolder/$pipelinesName.a.16b.sh

for line in $(cat $WD/$pipelinesName.references.txt);do
  refText=$line
  array=(${line//:/ })
  st=${array[1]}
  prefix=${array[4]}
  gt=${array[5]}
  ref=${array[6]}
  echo "$st/$gt"
  echo "mkdir -p $WD/trueVariants/${st}/"  >> $scriptsFolder/$pipelinesName.a.16b.sh
  echo "$msa2vcf -R $refText $WD/consensus/$st/all/${pipelinesName}_${st}_${gt}_consensus.fasta > $WD/trueVariants/${st}/${pipelinesName}_${st}_${gt}_TRUE_C.vcf" >> $scriptsFolder/$pipelinesName.a.16b.sh
done

# -o
echo "module unload jdk-oracle/1.8.0" >> $scriptsFolder/$pipelinesName.a.16b.sh
jobID=$(qsub -l num_proc=1,s_rt=0:20:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.16b.sh | awk '{ print $3}')
