################################################################################
################################################################################
#######                               _           _                      #######
#######             /\               | |         (_)                     #######
#######            /  \   _ __   __ _| |_   _ ___ _ ___                  #######
#######           / /\ \ | '_ \ / _` | | | | / __| / __|                 #######
#######          / ____ \| | | | (_| | | |_| \__ \ \__ \                 #######
#######         /_/    \_\_| |_|\__,_|_|\__, |___/_|___/                 #######
#######                                 __/  |                           #######
#######                                 |___/                            #######
################################################################################
################################################################################
# Creating folder for processing

mkdir -p $procBam $procSam $procSorted $procDup $procIndMap
for line in $(tail -n+2  $WD/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  mkdir -p $procSam/$st/$gt $procBam/$st/$gt $procSorted/$st/$gt $procDup/$st/$gt
  mkdir -p $procMAPQ/$st/$gt $procVarcalling/$st/$gt  $consensusFolder/$st/$gt
  mkdir -p $procIndMap/$st/$gt
done


SEEDFILE="$scriptsFolder/$pipelinesName.a.3.files"
find $procSam -name *.sam | grep -v "merged" > $SEEDFILE
totalSamtoolsJobs=$(wc -l $SEEDFILE | awk '{print $1}')
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.a.3.o
#$ -e $outputFolder/$pipelinesName.a.3.e
#$ -N cs2.a.3
module load samtools
SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE)
OUTPUTBASE=\$(echo \$SEED | sed 's/proc\/sam/proc\/indsorted/g' | sed 's/\.sam/\.ind\.sorted/g')

echo \$OUTPUTBASE
samtools view -bS \$SEED | samtools sort - \$OUTPUTBASE
module unload samtools
"  > $scriptsFolder/$pipelinesName.a.3.sh
jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-$totalSamtoolsJobs $scriptsFolder/$pipelinesName.a.3.sh | awk '{ print $3}')

################################################################################
# This files are already BAM files


procIndSorted="$WD/proc/indsorted"
for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for item in $(seq 1 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $item)
    for ind in $(seq 0 15); do
      echo "$procIndSorted/$st/$gt/${pipelinesName}_${st}_${gt}_data_${ind}.ind.sorted.bam" >>  $WD/report/scripts/${pipelinesName}_${st}_${gt}.angsd.filelist
    done
  done
done
