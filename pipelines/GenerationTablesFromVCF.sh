################################################################################
# STEP 15. SELECTING SNPS
################################################################################
echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.a.15c.o
#$ -e $outputFolder/$pipelinesName.a.15c.e
#$ -N vcftabs.a.15c

module load samtools jdk-oracle/1.8.0
" >  $scriptsFolder/$pipelinesName.a.15c.sh

methods=("A" "B" "C" "D" "E")
for met in ${methods[@]};do
  for line in $(cat $WD/$pipelinesName.evens); do
    st=$(printf "%0$numDigits""g" $line)
    for item in $(seq 1 $nGTs); do
      gt=$(printf "%0$numDigitsGTs""g" $item)
      echo -e "\e[1A\r M$met - $st/$gt"
      reference="$referencesFolder/REF_${prefixRef}_${prefixLoci}_${st}_${gt}.fasta"
      outputVCF="$procVarcalling/method${met}/${st}/${gt}/${pipelinesName}_${st}_${gt}"
      outputTable="$procVarcalling/method${met}/${st}/${gt}/${pipelinesName}_${st}_${gt}"
      inputBAM="$procSorted/${st}/${gt}/${pipelinesName}_${st}_${gt}.merged.sorted.bam"
      echo -e "
info_fields=\$(head -5000 ${outputVCF}.demux.vcf | perl -ne '/^.*INFO.*ID=([a-zA-Z0-9_]+),/ && print \"-F \$1 \"' | uniq)
genotype_fields=\$(head -5000 ${outputVCF}.demux.vcf | perl -ne '/^.*FORMAT.*ID=([a-zA-Z0-9_]+),/ && print \"-GF \$1 \"' | uniq)
java -jar $GATK -R $reference -T VariantsToTable -V ${outputVCF}.demux.vcf -F POS -F REF -F ALT -F QUAL -F TYPE -F HOM-REF -F HET -F HOM-VAR -F NO-CALL -F VAR -F NCALLED -F NSAMPLES -F MULTI-ALLELIC \${info_fields[@]} \${genotype_fields[@]} -GF GP -AMD -o $outputTable.demux.table
      "  >>  $scriptsFolder/$pipelinesName.a.15c.sh
    done
  done
done


echo -e "\nmodule unload samtools jdk-oracle/1.8.0" >>  $scriptsFolder/$pipelinesName.a.15c.sh
jobID=$(qsub -l num_proc=1,s_rt=3:00:00,s_vmem=4G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.a.15c.sh | awk '{ print $3}')
