#!/bin/bash
# $1 - Path of the SimPhy project folder
# $2 - the path for the file with the ids of the species trees that are going to
#      be filtered
PROG="csHAE.filtering.trees.even.inds.sh"
PROG_VERSION="1.0"
AUTHOR="Merly Escalona <merlyescalona@uvigo.es>"

# bash csHAE.filtering.trees.even.inds.sh /home/merly/conusSim/csTst/  /home/merly/conusSim/csTst/csTst.even.inds 10
usage(){
  echo -e "$PROG
Version: v$PROG_VERSION
===============================================================================

Usage: $PROG \$1 \$2 \$3

Description: Program generated to separated species tree replicates with even
and odd number of individuals per species in order to be able to run the mating
program which need the number of individuals per species to be even.

This scripts creates the even/odd folders. Also generates 2 files that describe
the relation between the indexes of the original replicates and the indexes for
the replicates that are considered Even and Odd respectively.

\$1 - Absolute path of the SimPhy project folder
\$2 - Absolute path for the file with the ids of the species trees that are going to
      be filtered
\$3 - Num Replicates
"
}

unsettingVariables(){
  # Unsetting variables
  unset error simphyFolder filterFile PROG PROG_VERSION AUTHOR currentPath numReplicates
}
# -----------------------------------------------------------------------------
# Snippet retrived from:
# https://raymii.org/s/snippets/Bash_Bits_Check_If_Item_Is_In_Array.html
in_array() {
    local haystack=${1}[@]
    local needle=${2}
    for i in ${!haystack}; do
        if [[ ${i} == ${needle} ]]; then
            return 0
        fi
    done
    return 1
}

###############################################################################
# Checking parameters exist
###############################################################################

if [ "$#" -ne 3 ]; then
  echo -e "ERROR: Illegal number of parameters\n
-------------------------------------------------------------------------------
  "
  usage
  exit
fi

###############################################################################
# Checking type of parameterrs
###############################################################################
# Till here have correct number of parameters
simphyFolder=$1
filterFile=$2
numReplicates=$3
error=0
if [ ! -d $simphyFolder ]; then
  echo "ERROR: $simphyFolder does not exist. Please check the path."
  let error=error+1
fi

#if [ -e $filterFile ]; then
if [ ! -e $filterFile ]; then
  echo "ERROR: $filterFile does not exist. Please check the path."
  let error=error+1
fi

# If any of the path have an error, show usage and exit.
if [ $error -ne 0 ]; then
  usage
  unsettingVariables
  exit
fi

###############################################################################
# Main block
###############################################################################
currentPath=$(pwd)
echo "Moving to SimPhy folder"
cd $simphyFolder


declare -a sts
count=0
while read line; do
  sts[count]=$(printf "%02d" $line)
  #echo ${sts[count]}
  let count=count+1
done < $filterFile

mkdir odd even
referencesEven="$simphyFolder/relation.inds.even.txt"
referencesOdd="$simphyFolder/relation..inds.odd.txt"
printf "Original\tNew\n" > $referencesEven
printf "Original\tNew\n" > $referencesOdd
#numST=${#sts[@]}.
#NumReplicates
indexEven=1
indexOdd=1
for index in $(seq 1 $numReplicates); do
  currentIndex=$(printf "%04d" $index)
  printf "\rReplicate $currentIndex"
  wholepath=$(readlink -f $currentIndex)
  #result=$(in_sts_array $currentIndex)
  #printf "\t $result"
  #if [[ $result -eq 0 ]]; then
  if in_array sts $currentIndex; then
    printf "$currentIndex\t$(printf "%04d" $indexEven)\n" >> $referencesEven
    ln -s $wholepath even/$(printf "%04d" $indexEven)
    let indexEven=indexEven+1
  else
    printf "$currentIndex\t$(printf "%04d" $indexOdd)\n" >> $referencesOdd
    ln -s $wholepath odd/$(printf "%04d" $indexOdd)
    let indexOdd=indexOdd+1
  fi
done

unset sts count currentIndex wholepath indexEven indexOdd referencesEven referencesOdd
