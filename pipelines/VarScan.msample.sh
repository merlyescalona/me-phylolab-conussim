#!/bin/bash

# Testing multiple sample SAM/BAM to be introduced in VarScan
module load samtools
module load jdk-oracle/1.8.0


st=1
gt=001
ind=1
RGID="000${ind}"
SMID="S.${st}-G.${gt}-I.${ind}"

ref="$referencesFolder/REF_${prefixRef}_${prefixLoci}_${st}_${gt}.fasta"
infile1="$readsFolder/fq/${st}/${gt}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}_"

# Mapping 1 sample
bwa mem -t 1 -R "@RG\tID:${RGID}\tSM:${SMID}\tPL:Illumina\tLB:${RGID}\tPU:HiSeq2500" ${ref} ${infile1}R1.fq ${infile1}R2.fq >test.sam

ind=2
SMID="S.${st}-G.${gt}-I.${ind}"
RGID="000${ind}"
infile1="$readsFolder/fq/${st}/${gt}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}_"
# Mapping 2 sample
bwa mem -t 1 -R "@RG\tID:${RGID}\tSM:${SMID}\tPL:Illumina\tLB:${RGID}\tPU:HiSeq2500" ${ref} ${infile1}R1.fq ${infile1}R2.fq > test2.sam

# merging maps
java -Xmx4G -jar $PICARD MergeSamFiles I=test.sam I=test2.sam O=merged.sam

# Sorting
samtools view -bSh merged.sam | samtools sort -h - merged.sorted

# index
samtools index merged.sorted.bam


echo -e "0001\n0002" >test1.names # RG names
echo -e "S.1-G.001-I.1\nS.1-G.001-I.2" >test2.names # Sample Names (SM)

# Varcalling
samtools mpileup -D -I -f $reference merged.sorted.bam | java -jar $VarScan2 mpileup2snp --output-vcf --vcf-sample-list test1.names > test1.vcf
samtools mpileup -D -I -f $reference merged.sorted.bam | java -jar $VarScan2 mpileup2snp --output-vcf --vcf-sample-list test2.names > test2.vcf
samtools mpileup -D -I -f $reference merged.sorted.bam | java -jar $VarScan2 mpileup2snp  > test.txt


 diff test1.vcf test2.vcf
<<RES
 24c24
 < #CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	0001	0002
 ---
 > #CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	S.1-G.001-I.1	S.1-G.001-I.2
RES


cat test.txt
<<RES
Chrom	                                  Position	    Ref	    Var	    Cons:Cov:Reads1:Reads2:Freq:P-value	     StrandFilter:R1+:R1-:R2+:R2-:pval	  SamplesRef    SamplesHet 	  SamplesHom	SamplesNC	Cons:Cov:Reads1:Reads2:Freq:P-value
csSim2:1:REF:LOC1RND:data:001:2_0_0	    112	          A	      G	      G:278:0:278:100%:1.2535E-166	           Pass:0:0:250:28:1E0	                0             0	            1	           0	G:278:0:278:100%:1.2535E-166
csSim2:1:REF:LOC1RND:data:001:2_0_0	    290	          A	      G	      G:488:0:488:100%:6.1323E-293	           Pass:0:0:196:292:1E0	                0             0	            1	           0	G:488:0:488:100%:6.1323E-293
csSim2:1:REF:LOC1RND:data:001:2_0_0	    334	          A	      G	      G:408:0:408:100%:8.1952E-245	           Pass:0:0:106:302:1E0	                0             0	            1	           0	G:408:0:408:100%:8.1952E-245
csSim2:1:REF:LOC1RND:data:001:2_0_0	    360	          A	      G	      G:388:0:388:100%:8.7873E-233	           Pass:0:0:64:324:1E0	                0             0	            1	           0	G:388:0:388:100%:8.7873E-233
RES








samtools view -bSh merged.sam | samtools sort -h - merge.sorted
