#!/bin/bash
# This file has been generated to extract usage information of a job in a
# SGE environment.
# Parameters:
# $1 -> name of the output file
# $2 -> name of the job
# Example:
# bash usage_monitorization.sh output.file 13324
EXIT=0
trap exiting SIGINT
exiting() { echo "Ctrl-C trapped" ; EXIT=1;}
usage(){
  echo -e '''Usage Monitorization
(c) Merly Escalona <merlyescalona@uvigo.es>

This file has been generated to extract usage information of a job in a
SGE environment.

Parameters:
===========
$1 -> name of the output file
$2 -> name of the job

Output:
=======
Output will be a CSV file (comma separated values file), with the following
columns:

  round,date,cpu, mem, io, vmem, maxvmem

Round is related to the number of times information has been obtained, as default
this information is taken every second.

Example:
===========
bash usage_monitorization.sh output.file 13324

  '''

}

<<EXAMPLE
jobID=$(qsub $WD/scripts/conusSim.$pipelinesName.1.sh | awk '{ print $3}')
# monitor <outputFile> <jobID/name> 0
bash $monitor $WD/usage/conusSim.$pipelinesName.1.usage $jobID &
EXAMPLE
jobID=$(qstat -u merly | grep $2 | awk '{print $1}')
START=$(date +%s)
echo "Checking job $2 wiht ID: $jobID"

echo "round,date,cpu, mem, io, vmem, maxvmem" > $statsFile.running
jobExists="TRUE"
# Checking that the job is running
if [ -z $jobID ]; then
  jobExists="FALSE"
  usage()
  exit
else
  jobExists="TRUE"
fi
statsFile=$1

counter=1
while [ $jobExists == "TRUE" ]
do
  echo -n "$counter, $(date), " >> $statsFile.running
  stats=$(qstat -j $jobID | grep usage | awk '{print $3,$4,",",$6,$7,$8}')
  echo $stats >> $statsFile.running
  sleep 1
  let counter=counter+1
  END=$(date +%s)
  DIFF=$(( $END - $START ))
  echo -e "\r $jobID:$counter\tRounds - $DIFF\tseconds elapsed."

   jobID=$(qstat -u merly | grep $2 | awk '{print $1}')
   if [ -z $jobID ]; then
     echo "$jobID"
     jobExists="FALSE"
   fi
   if [ $EXIT -eq 1 ]; then
     jobExists="FALSE"
     echo "Finishing job"
   fi

done


echo "round,date,cpu, mem, io, vmem, maxvmem" >  $statsFile
cat $statsFile.running  | sed 's/cpu=/ /g' | sed 's/io=/ /g' | sed 's/maxvmem=/ /g'|sed 's/vmem=/ /g' | sed 's/mem=/ /g' | sed 's/M//g' >> | tr '[:upper:]' '[:lower:]' | sed 's/n\/a/ /g' | awk -F "," '{ if (NF < 7)   print $1,",",$2,",",$3,",  ,  ,  ,  "; else   print $1,",",$2,",",$3,",",$4,",",$5,",",$6; }' >> $statsFile
rm $statsFile.running
