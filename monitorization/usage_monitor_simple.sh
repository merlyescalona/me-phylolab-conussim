#!/bin/bash
usage(){
echo -e '''Usage Monitor Simple
(c) Joao M. Alves,  Merly Escalona<jalves,merlyescalona@uvigo.es>

Extracts usage information of a process. To be used for single runs in the command line.

Usage:

./usage_monitor_simple.sh <pid> <monitortime> <outputfile>

Example:

myCommand & ID=$! && ./usage_monitor_simple.sh $ID 5 outputfile.txt
	** This example will not work for interactive scripts. If the script you want to
	monitor is interactive, please get the PID in a different way and run the
	<usage_monitor_simple.sh> alone.

'''
}
# How to call: myCommand & ID=$! && ./usage_monitor_simple.sh $ID 5 outputfile.txt
if [ "$#" -lt 3 ]; then
	usage;
	exit;
else
	PID=$1
	sleepTime=$2  # (Units: Seconds) leaving it as it was
	outputfile=$3
	# echo "$PID,$sleepTime,$outputfile" # Test purposes
	# Writing header
	ps eo pid,ppid,nlwp,pcpu,vsz,psr,time --ppid $PID > $outputfile
	# checking if this file exits, because it has all the information of the memory.
	# It is created when the process starts and deleted when it finishes.
	while [ -e /proc/$PID ]
	do
	sleep $sleepTime
	ps eo pid,ppid,nlwp,pcpu,vsz,psr,time --ppid $PID | tail -n+2 >> $outputfile
	done
fi
