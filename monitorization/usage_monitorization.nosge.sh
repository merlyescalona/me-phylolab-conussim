<<INFORMATION
http://locklessinc.com/articles/memory_usage/

How do you work out the total memory used by a process at
run-time? One way is to use the malloc_stats() function. If implemented in your allocator, it will print out the memory used, together with perhaps other information about the internal state of itself in user-understandable format. However, unfortunately, the format of this information is not standardized. It also is output to stderr so is not directly accessible.

Another possibility is to use the mallinfo routine. If implemented, this returns a struct filled with data on the internal state of the allocator. Unfortunately, this routine is also not that useful. The reason is that the fields in struct mallinfo were originally defined with respect to the internals of a rather old allocator. A new allocator will use different algorithms, and may have vastly different internal accounting of memory. There may be very little correspondence between the meaning of the individual fields between allocators.

We are left with using operating system specific APIs to determine memory usage. This is however, an advantage. We will be able to determine the complete overhead of an application, including memory not allocated through the C library. For example, when the process loads a shared library, the linker will use the mmap() system call directly. Similarly, the thread stacks in the pthread implementation will be directly mapped rather than malloced.

To access the total memory information about a process on Linux, we use the /proc virtual file system. Within it there is a directory full of information for each active process id (pid). By reading /proc/(pid)/status we can obtain information about memory. Amoungst other things, in Linux version 2.6.39, this file includes:

VmPeak:	Peak virtual memory usage
VmSize:	Current virtual memory usage
VmLck:	Current mlocked memory
VmHWM:	Peak resident set size
VmRSS:	Resident set size
VmData:	Size of "data" segment
VmStk:	Size of stack
VmExe:	Size of "text" segment
VmLib:	Shared library usage
VmPTE:	Pagetable entries size
VmSwap:	Swap space used

# usage_monitorization_nosge.sh
# Information about mem usage

RSS is the Resident Set Size and is used to show how much
memory is allocated to that process and is in RAM. It does
not include memory that is swapped out. It does include
memory from shared libraries as long as the pages from
those libraries are actually in memory. It does include
all stack and heap memory.

VSZ is the Virtual Memory Size. It includes all memory that the
 process can access, including memory that is swapped out and
 mory that is from shared libraries.

So if process A has a 500K binary and is linked to 2500K of
shared libraries, has 200K of stack/heap allocations of
which 100K is actually in memory (rest is swapped),
and has only actually loaded 1000K of the shared libraries
and 400K of its own binary then:

RSS: 400K + 1000K + 100K = 1500K
VSZ: 500K + 2500K + 200K = 3200K
Since part of the memory is shared, many processes may use it, so if you add up all of the RSS values you can easily end up with more space than your system has.

There is also PSS (proportional set size).
This is a newer measure which tracks the shared memory as a
 proportion used by the current process. So if there were two
  processes using the same shared library from before:

PSS: 400K + (1000K/2) + 100K = 400K + 500K + 100K = 1000K
Threads all share the same address space, so the RSS, VSZ and PSS for each thread is identical to all of the other threads in the process. Use ps or top to view this information in linux/unix.

There is way more to it than this, to learn more check the following references:

http://unixhelp.ed.ac.uk/CGI/man-cgi?ps
http://emilics.com/blog/article/mconsumption.html
INFORMATION

run test.sh & PID=$!
touch run.txt
while kill -0 $PID >/dev/null 2>&1
do
sleep 5
if [ -s run.txt ]
then
    ps eo pid,ppid,nlwp,pcpu,pmem,psr,time --ppid $PID | tail -n 1 >> run.txt
else
    ps eo pid,ppid,nlwp,pcpu,pmem,psr,time --ppid $PID >> run.txt
fi
done


myCommand & PID=$!
units="kB"
sleepTime=5  # (Units: Seconds) leaving it as it was
# Writing header
echo -e "PID\tVmSize\tVmPeak\tNumThreads\t%CPU\tProc.Assigned\tTime" >result.txt
# checking if this file exits, because it has all the information of the memory.
# It is created when the process starts and deleted when it finishes.
while [ -e /proc/$PID ]
do
	sleep $sleepTime
	procStatus="/proc/$PID/status"
	# Memory Information
	vmpeak=$(cat $procStatus | grep VmPeak | awk '{print $2}') # Total loaded
	vmsize=$(cat $procStatus | grep VmSize | awk '{print $2}') # current loaded
	# Processor and Time information
	psRes=($(ps eo nlwp,pcpu,psr,time --ppid $PID| tail -n 1))
	# Order of the information in the array
	# numThreads (0),pcpu (1),assignedProcessor (2),time (3)
	# Writing output
	echo -e "$PID\t$vmsize\t$vmpeak\t${psRes[0]}\t${psRes[1]}\t${psRes[2]}\t${psRes[3]}" >> result.txt
done
