# Introduction

This has been done to get information on how ART [^art] works with the different
built-in sequencing system profiles , their quality, the posterior effect when doing mapping.

The preliminary results, with built-in HS25 profile (ART version ChocolateCherryCake), and with
a customized profile from an empirical capture experiment, show that the first (built-in profiles) look better than the customized ones.
Questions arise on how to measure the "close-to-reality" profile.

What I think I should be doing to verify this:
- re-do sequencing simulation with more profiles.
- map to their corresponding reference loci
- compare mapping quality
- compare coverage
- mappability (check out GEM/ GMA)
- information about the sequences being used
    - diversity
    - nucleotidic frequency
    - GC content


Side note: Would this give me some information on how profiles influence the analysis?




To check mappability:
[http://blog.kokocinski.net/index.php/sequence-mappability-alignability?blog=2](http://blog.kokocinski.net/index.php/sequence-mappability-alignability?blog=2)


https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3261895/


Also, the genome mappability score analyzere

https://sourceforge.net/p/gma-bio/wiki/Manual/
