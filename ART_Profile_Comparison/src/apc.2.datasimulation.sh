################################################################################
# (c) 2015-2016 Merly Escalona <merlyescalona@uvigo.es>
# Phylogenomics Lab. University of Vigo.
################################################################################
#!/bin/bash -l
################################################################################
# STEP 0. Setting environment
cp -r /home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/1/ $WD/
cp -r /home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/individuals/ $WD/
cp /home/uvi/be/mef/mnt/phylolab/uvibemef/csSim2/csSim2.mating $WD/
mkdir -p $WD/readsPersonalized
mkdir -p $WD/readsDefault
mkdir $WD/scripts $WD/files $WD/output

################################################################################
# STEP 7. NGS Simulation
################################################################################
for line in $(tail -n+2  $WD/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  echo "$st $gt $ind"
  inputFile="${pipelinesName}_${st}_${gt}_${prefix}_${ind}.fasta"
  echo "$WD/individuals/$st/$gt/$inputFile" >>  $WD/files/art.files
done

SEEDFILE="$WD/files/art.files"
################################################################################
# Generating job for art run with personalized profile
echo -e "#! /bin/bash
#$ -o $WD/output/APC.2a.o
#$ -e $WD/output/APC.2a.e
#$ -N APC2.2a

SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE )
INPUTBASE=\$(basename \$SEED .fasta)

cd $WD/readsPersonalized/
art_illumina -sam  -1 $profilePath/csNGSProfile_hiseq2500_1.txt -2 $profilePath/csNGSProfile_hiseq2500_2.txt -f 100 -l 150 -p  -m 250 -s 50 -rs $RANDOM -ss HS25 -i \$SEED -o \${INPUTBASE}_R

">   $WD/scripts/APC.2a.sh
################################################################################
# Generating job for art run with default profile
echo -e "#! /bin/bash
#$ -o $WD/output/APC.2b.o
#$ -e $WD/output/APC.2b.e
#$ -N APC2.2b

SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE )
INPUTBASE=\$(basename \$SEED .fasta)

cd $WD/readsDefault/
art_illumina -sam  -f 100 -l 150 -p  -m 250 -s 50 -rs $RANDOM -ss HS25 -i \$SEED -o \${INPUTBASE}_R

">   $WD/scripts/APC.2b.sh
################################################################################
# Launching job array for both methods
totalArtJobs=$(wc -l $SEEDFILE | awk '{print $1}')
jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-$totalArtJobs $WD/scripts/APC.2a.sh | awk '{ print $3}')
jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-$totalArtJobs $WD/scripts/APC.2b.sh | awk '{ print $3}')

################################################################################
# Reorganization of reads reads according to output file format and ST and GT.
for line in $(tail -n+2  $WD/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  echo -e "\r$st/$gt"
  mkdir -p $WD/readsPersonalized/sam/$st/$gt/ $WD/readsPersonalized/fq/$st/$gt/ $WD/readsPersonalized/aln/$st/$gt/
  mkdir -p $WD/readsDefault/sam/$st/$gt/ $WD/readsDefault/fq/$st/$gt/ $WD/readsDefault/aln/$st/$gt/
done


st=1
for item in $(seq 1 $nGTs); do
  gt=$(printf "%0$numDigitsGTs""g" $item)
  echo -e "\r$st/$gt"
  filePrefix="${pipelinesName}_${st}_${gt}_data_"
  mv "$WD/readsPersonalized/$filePrefix"*.fq $WD/readsPersonalized/fq/$st/$gt/
  mv "$WD/readsPersonalized/$filePrefix"*.sam $WD/readsPersonalized/sam/$st/$gt/
  mv "$WD/readsPersonalized/$filePrefix"*.aln $WD/readsPersonalized/aln/$st/$gt/
  mv "$WD/readsDefault/$filePrefix"*.fq $WD/readsDefault/fq/$st/$gt/
  mv "$WD/readsDefault/$filePrefix"*.sam $WD/readsDefault/sam/$st/$gt/
  mv "$WD/readsDefault/$filePrefix"*.aln $WD/readsDefault/aln/$st/$gt/
done
################################################################################
# FASTQC
################################################################################
fq1Files="$WD/readsPersonalized.allfiles.fastq.files"
fq2Files="$WD/readsDefault.allfiles.fastq.files"

find $WD/readsPersonalized -name *.fq | xargs cat > $fq1Files
find $WD/readsDefault -name *.fq | xargs cat > $fq2Files

# los ficheros existen solo tengo que mandar los jobs

################################################################################
echo -e "#! /bin/bash
#$ -o $WD/output/APC.3a.o
#$ -e $WD/output/APC.3a.e
#$ -N APC.3a

INPUTBASE=$(basename $fq1Files .fq)

cd $WD/qc1/\$INPUTBASE
$fastqc $fq1Files -o $WD/qc1/$INPUTBASE

">   $WD/scripts/APC.3a.sh
################################################################################
echo -e "#! /bin/bash
#$ -o $WD/output/APC.3b.o
#$ -e $WD/output/APC.3b.e
#$ -N APC.3b

INPUTBASE=$(basename $fq1Files .fq)

cd $WD/qc2/\$INPUTBASE
$fastqc $fq2Files -o $WD/qc2/$INPUTBASE

">   $WD/scripts/APC.3b.sh
################################################################################

qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell $WD/scripts/APC.3a.sh
qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell $WD/scripts/APC.3b.sh
