################################################################################
# Variables
################################################################################
pipelinesName="APC"
stReplicates=1
numDigits=${#stReplicates}
nGTs=100
numGeneTrees="f:${nGTs}"
numDigitsGTs=${#nGTs}
sizeLoci="500bp"
prefix="data"
timeRange="u:200000,20000000" # 200k to 20My
nIndsTaxa="f:6" # Number of individuals per taxa (actual num of ind will be 3)
nTaxa="f:5" # Number of taxa
subsRate="e:10000000" # exp(10^7)
prefixLoci="data"
prefixRef="LOC1RND"
################################################################################
# Paths
################################################################################
wrapper="$HOME/src/me-phylolab-conussim/scripts/INDELIble_wrapper.pl"
WD="$HOME/research/$pipelinesName"
controlFile="$HOME/src/me-phylolab-conussim/ART_Profile_Comparison/src/indelible.control.txt"
matingProgram="$HOME/src/me-phylolab-mating/mating.py"
pair1Path="$HOME/research/csNGSProfile_hiseq2500_1.txt"
pair2Path="$HOME/research/csNGSProfile_hiseq2500_2.txt"
lociReferenceSelection="$HOME/src/me-phylolab-conussim/LociRefSelection/locirefsel.main.py"
################################################################################
# MODULES
################################################################################
module load bio/art/050616 bio/simphy/1.0.2 bio/indelible/1.03 perl/5.22.1_1 python/2.7.8 bio/fastqc/0.11.2 bio/diversity/2.0.0 bio/samtools/1.2.0
################################################################################
# SIMPHY
################################################################################
# All running in triploid
simphy -rs $stReplicates -rl $numGeneTrees -sb ln:-15,1 -st $timeRange\
  -sl $nTaxa -so f:1 -sp f:100000 -su $subsRate -si $nIndsTaxa -sg f:1\
  -gp f:200 -hg ln:1.2,1 -v 1 -o $pipelinesName\
  -cs $RANDOM -om 1 -od 1 -op 1 -oc 1 -on 1 > $pipelinesName.out
################################################################################
# wrapper
################################################################################
perl $wrapper $WD $controlFile $RANDOM 1 &> $WD/$pipelinesName.2.txt
mkdir $WD/output
mv $WD/$pipelinesName.2.txt $pipelinesName.out $WD/output
################################################################################
# Indelible into ST=1
################################################################################
cd $WD/1
indelible > $pipelinesName/output/$pipelinesName.indelible.out
################################################################################
# Mating
################################################################################
python $matingProgram -p $prefixLoci -sf $WD/ -l DEBUG
################################################################################
# NGS read generation
# GA1 - GenomeAnalyzer I (36bp,44bp), GA2 - GenomeAnalyzer II (50bp, 75bp)
# HS10 - HiSeq 1000 (100bp),          HS20 - HiSeq 2000 (100bp),
# NS50 - NextSeq500 v2 (75bp)
# MinS - MiniSeq TruSeq (50bp)
################################################################################
# These allow reads of 150bps
# HS25 - HiSeq 2500 (125bp, 150bp)
# HSXn - HiSeqX PCR free (150bp)
# HSXt - HiSeqX TruSeq (150bp)
# MSv1 - MiSeq v1 (250bp),
# MSv3 - MiSeq v3 (250bp),
################################################################################
mkdir $WD/readsCustom/ $WD/HS25 $WD/HSXn $WD/HSXt $WD/MSv1 $WD/MSv3
for line in $(tail -n+2  $WD/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  echo "$st $gt $ind"
  inputFile="$WD/individuals/$st/$gt/${pipelinesName}_${st}_${gt}_${prefix}_${ind}.fasta"
  INPUTBASE=$(basename $inputFile .fasta)
  art_illumina -1 $pair1Path -2 $pair2Path -f 100 -l 150 -p  -m 250 -s 50 -rs $RANDOM  -i $inputFile -o $WD/readsCustom/${INPUTBASE}_R >> $WD/output/profile.out
  art_illumina -f 100 -l 150 -p  -m 250 -s 50 -rs $RANDOM -ss HS25 -i $inputFile -o $WD/HS25/${INPUTBASE}_R >> $WD/output/HS25.out
  art_illumina -f 100 -l 150 -p  -m 250 -s 50 -rs $RANDOM -ss HSXn -i $inputFile -o $WD/HSXn/${INPUTBASE}_R >> $WD/output/HSXn.out
  art_illumina -f 100 -l 150 -p  -m 250 -s 50 -rs $RANDOM -ss HSXt -i $inputFile -o $WD/HSXt/${INPUTBASE}_R >> $WD/output/HSXt.out
  art_illumina -f 100 -l 150 -p  -m 250 -s 50 -rs $RANDOM -ss MSv1 -i $inputFile -o $WD/MSv1/${INPUTBASE}_R >> $WD/output/MSv1.out
  art_illumina -f 100 -l 150 -p  -m 250 -s 50 -rs $RANDOM -ss MSv3 -i $inputFile -o $WD/MSv3/${INPUTBASE}_R >> $WD/output/MSv3.out
done

rm $WD/readsCustom/*.aln $WD/HS25/*.aln $WD/HSXn/*.aln $WD/HSXt/*.aln $WD/MSv1/*.aln $WD/MSv3/*.aln
################################################################################
# FASTQC
################################################################################
fq1Files="$WD/readsCustom.allfiles.fastq.files"
fq2Files="$WD/HS25.allfiles.fastq.files"
fq3Files="$WD/HSXn.allfiles.fastq.files"
fq4Files="$WD/HSXt.allfiles.fastq.files"
fq5Files="$WD/MSv1.allfiles.fastq.files"
fq6Files="$WD/MSv3.allfiles.fastq.files"

find $WD/readsCustom -name *.fq | xargs cat > $fq1Files
find $WD/HS25 -name *.fq | xargs cat > $fq2Files
find $WD/HSXn -name *.fq | xargs cat > $fq3Files
find $WD/HSXt -name *.fq | xargs cat > $fq4Files
find $WD/MSv1 -name *.fq | xargs cat > $fq5Files
find $WD/MSv3 -name *.fq | xargs cat > $fq6Files

mkdir $WD/qcCustom/ $WD/qcHS25/ $WD/qcHSXn/ $WD/qcHSXt/ $WD/qcMSv1/ $WD/qcMSv3/

fastqc $fq1Files -o $WD/qcCustom/
fastqc $fq2Files -o $WD/qcHS25/
fastqc $fq3Files -o $WD/qcHSXn/
fastqc $fq4Files -o $WD/qcHSXt/
fastqc $fq5Files -o $WD/qcMSv1/
fastqc $fq6Files -o $WD/qcMSv3/

mkdir $WD/qc
mv $WD/qc* $WD/qc
rm *.fastq.files
################################################################################
# Reference sequence selection
################################################################################
python $lociReferenceSelection -ip $prefixLoci -op $prefixRef -sf $WD -o $WD/references/ -m 1
for item in $(find $WD/references -name *.fasta); do
  head -1 $item >> $WD/${pipelinesName}.references.txt;
done
################################################################################
# diversity
################################################################################
mkdir $WD/stats
st=1
for indexGT in $(seq 1 $nGTs); do
  gt=$(printf "%0$numDigitsGTs""g" $indexGT)
  filename="$WD/$st/${prefix}_${gt}_TRUE.phy"
  INPUTBASE=$(basename $filename .phy)
  diversity $filename &> $WD/stats/${INPUTBASE}.0.stats
  diversity $filename -g &> $WD/stats/${INPUTBASE}.1.stats
  diversity $filename -g -m -p 2> $WD/stats/${INPUTBASE}.2.stats >> div.long.out
done

tmp1="$WD/diversity.parsing.1.tmp"
tmp2="$WD/diversity.parsing.2.tmp"
for indexGT in $(seq 1 $nGTs); do
  gt=$(printf "%0$numDigitsGTs""g" $indexGT)
  elem=$(cat $WD/stats/${prefixLoci}_${gt}_TRUE.0.stats | grep "mean pairwise distance =" | awk -F"=" '{print $2}' )
  echo $elem
  echo $elem >>$tmp1
done
cat div.long.out | grep ^data| paste -d "," - $tmp1 > $tmp2
diversitySummary="$WD/${pipelinesName}.diversity.summary"
echo "file,numTaxa,numSites,NumVarSites,NumInfSites,numMissSites,numGapSites,numAmbiguousSites,freqA,freqC,freqG,freqT,meanPairwiseDistancePerSite,meanPairwiseDistance" > $diversitySummary
cat $tmp2 >> $diversitySummary
rm $tmp1 $tmp2

outFolder="$WD/stats/csv"
inputFolder="$WD/stats"
mkdir $outFolder
for item in $(seq 1 $nGTs); do
  gt=$(printf "%0$numDigitsGTs""g" $item)
  inputbase="${prefixLoci}_${gt}"
  echo $inputbase
  Rscript --vanilla $HOME/src/me-phylolab-conussim/diversityMatrices/parseDiversityMatrices.R $inputFolder $inputbase $outFolder
done
################################################################################
# Mapping
################################################################################
# Reference indexing
module load bio/bwa/0.7.10
for filename in $(find $WD/references -name "*.fasta"); do
  bwa index -a is $filename
done

for line in $(seq 0 15); do
  echo "I.$(printf "%02g" ${line})" >> csSim2.sample.list
done

dataFolders=(readsCustom HS25 HSXn HSXt MSv1 MSv3)
for readsFolder in ${dataFolders[@]}; do
  counter=1
  totalNumR1Files=$(find $WD/$readsFolder/ -name *_R1.fq | wc -l |awk '{ print $1}')
  mkdir $WD/$readsFolder/sam
  for line in $(tail -n+2  $WD/$pipelinesName.mating); do
    array=(${line//,/ })
    st=${array[0]}
    gt=${array[1]}
    ind=${array[2]}
    echo "$readsFolder/$counter/$totalNumR1Files"
    ref="$WD/references/REF_${prefixRef}_${prefixLoci}_${st}_${gt}.fasta"
    infile1="$WD/$readsFolder/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}_"
    outfile1="$WD/$readsFolder/sam/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}.sam"
    RGID=$(printf "%0${#totalNumR1Files}""g" $counter)
    SMID="S.${st}-G.${gt}-I.$(printf "%02g" ${ind})"
    bwa mem -t 1 -R "@RG\tID:${RGID}\tSM:${SMID}\tPL:Illumina\tLB:${RGID}\tPU:$readsFolder" ${ref} ${infile1}R1.fq ${infile1}R2.fq > $outfile1
    let counter=counter+1
  done
done
################################################################################
# Sorting and conversion to bam
################################################################################
for readsFolder in ${dataFolders[@]}; do
  counter=1
  totalNumR1Files=$(find $WD/$readsFolder/sam -name "*.sam" | wc -l |awk '{ print $1}')
  for line in $(tail -n+2  $WD/$pipelinesName.mating); do
    array=(${line//,/ })
    st=${array[0]}
    gt=${array[1]}
    ind=${array[2]}
    echo "$readsFolder/$counter/$totalNumR1Files"
    SEED="$WD/$readsFolder/sam/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}.sam"
    samtools view -bSh $SEED | samtools sort - $WD/$readsFolder/sam/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}
    samtools index $WD/$readsFolder/sam/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}.bam
    let counter=counter+1
  done
done

################################################################################
# Checking mapping
# https://broadinstitute.github.io/picard/explain-flags.html
################################################################################
for readsFolder in ${dataFolders[@]}; do
  counter=1
  numFiles=$(find $WD/$readsFolder/sam -name "*.bam" | wc -l)
  echo "FILEPATH,NUMREADS" > $WD/pairing/${pipelinesName}.$readsFolder.mapped.reads.txt
  echo "FILEPATH,NUMREADS" > $WD/pairing/${pipelinesName}.$readsFolder.unmapped.reads.txt
  echo "FILEPATH,NUMREADS" > $WD/pairing/${pipelinesName}.$readsFolder.properpair.reads.txt
  echo "FILEPATH,NUMREADS" > $WD/pairing/${pipelinesName}.$readsFolder.unproperpair.reads.txt
  for line in $(find $WD/$readsFolder/sam -name "*.bam"); do
    mappedReads=$(samtools view -c -F 4 $line)
    unmappedReads=$(samtools view -c -f 4 $line)
    properlyPairedReads=$(samtools view -c -F 2 $line)
    unproperlyPairedReads=$(samtools view -c -f 2 $line)
    echo "$readsFolder/$counter/$numFiles"
    echo "$line,$mappedReads" >> $WD/pairing/${pipelinesName}.$readsFolder.mapped.reads.txt
    echo "$line,$unmappedReads" >> $WD/pairing/${pipelinesName}.$readsFolder.unmapped.reads.txt
    echo "$line,$properlyPairedReads" >> $WD/pairing/${pipelinesName}.$readsFolder.properpair.reads.txt
    echo "$line,$unproperlyPairedReads" >> $WD/pairing/${pipelinesName}.$readsFolder.unproperpair.reads.txt
    let counter=counter+1
  done
done

################################################################################
# Coverage
################################################################################

samtools depth \$(find \${SEED} -name *.bam) > $WD/report/coverage/coverage.\$SGE_TASK_ID.txt
