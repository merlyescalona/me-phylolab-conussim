import argparse,datetime,logging,os,sys
import ConsensusMultiFasta as cmf
import numpy as np
import random as rnd
################################################################################
# CONSTANTS
VERSION=0
MIN_VERSION=0
FIX_VERSION=1
PROGRAM_NAME="ConsensusFasta"
AUTHOR="Merly Escalona <merlyescalona@uvigo.es>"
################################################################################
# python LociReferenceSelection.py -p <prefix> -SF <simphy_path> -o outout -m method
################################################################################

def handlingParameters():
	"""
		Handling parameters.
		This functiuon takes care of the cmd-line input.
	"""

	parser = argparse.ArgumentParser(\
	prog="consensus.fasta.py (v.{0}.{1}.{2})".format(VERSION,MIN_VERSION,FIX_VERSION),\
		formatter_class=argparse.RawDescriptionHelpFormatter,\
		description=\
'''
Description:
============
This programs takes a multiple fasta file, corresponding to a diploid genome and
generates a consensus sequence with ambiguities.

Assumptions:
============
For the original purpose of this script, I required a diploid genome (a multiple
fasta file with only 2 sequences). Therefore, I assume that the input file will
have 2 sequences.

Also, I am assuming that both sequences have the same length.

Output:
=======

- The output will be a directory of FASTA files.

		''',\
			epilog="Version {0}.{1}.{2} (Still under development)".format(VERSION,MIN_VERSION,FIX_VERSION),\
			add_help=False
		)
	requiredGroup= parser.add_argument_group('Required arguments')
	requiredGroup.add_argument('-i','--input',metavar='<input_file>', type=str,\
		help='Path of the input multi fasta file.', required=True)
	requiredGroup.add_argument('-o','--output', metavar="<output_filer>",type=str,\
		help="Path of the output fasta file.",\
		required=True)


	informationGroup= parser.add_argument_group('Information arguments')
	informationGroup.add_argument('-v', '--version',\
		action='version',\
		version='{0} - {1}.{2}.{3}'.format(PROGRAM_NAME,VERSION,MIN_VERSION,FIX_VERSION),\
		help="Show program's version number and exit")
	informationGroup.add_argument('-h', '--help',\
		action='store_true',\
		help="Show this help message and exit")
	try:
		tmpArgs = parser.parse_args()
	except:
		sys.stdout.write("\n----------------------------------------------------------------------\n")
		parser.print_help()
		sys.exit()
	return tmpArgs

"""
MAIN
"""
if __name__=="__main__":
	try:
		cmdArgs = handlingParameters()
		print(cmdArgs)
		prog = cmf.ConsensusMultiFasta(cmdArgs.input,cmdArgs.output)
		prog.run()
	except KeyboardInterrupt:
		sys.stdout.write("{0}{1}\nInterrupted!{2}\nPlease run again for the expected outcome.\n".format(mof.BOLD,mof.DARKCYAN, mof.END))
		sys.exit()
	except IOError:
		sys.stderr.write("An error ocurred. Exiting.")
		sys.exit()
