import argparse,datetime,logging,os,sys
import numpy as np
import random as rnd

IUPAC=dict({\
    'A':'A','a':'a',\
    'C':'C','c':'c',\
    'G':'G','g':'g',\
    'T':'T','t':'t',\
    'AC':'M','ac':'m',\
    'AG':'R','ag':'r',\
    'AT':'W','at':'w',\
    'CG':'S','cg':'s',\
    'CT':'Y','ct':'y',\
    'GT':'K','gt':'k',\
    'ACG':'V','acg':'v',\
    'ACT':'H','act':'h',\
    'AGT':'D','agt':'d',\
    'CGT':'B','cgt':'b',\
    'ACGT':'N','acgt':'n'\
})
class ConsensusMultiFasta:
    def __init__(self,cmdInput,cmdOutput):
        self.input=cmdInput
        self.output=cmdOutput
        if(os.path.exists(self.input)):
            sys.stdout.write("Input file exists")
        else:
            raise IOError("Input error","Input file does not exist. Please verify.")

    def consensus(self):
        f=open(self.input,"r")
        lines=f.readlines()
        des1=lines[0][1:-1]
        seq1=lines[1][0:-1]
        des2=lines[2][1:-1]
        seq2=lines[3][0:-1]
        f.close()
        finalSeq=""
        for index in range(0,len(seq1)):
            pos=set([seq1[index].upper(),seq2[index].upper()])
            pos=list(pos)
            pos="".join(sorted(pos))
            finalSeq+=IUPAC[pos]


        tmp1=des1.split(":")
        tmp2=des2.split(":")

        fout=open(self.output,"w")
        #pipelinesName:ST:GT:CONSENSUS:WA:IND:S1:S2
        fout.write(">{0}:{1}:{2}:CONSENSUS:WA:{3}:{4}:{5}\n{6}\n".format(\
            tmp1[0],\
            tmp1[1],\
            tmp1[2],\
            tmp1[4],\
            tmp1[5],\
            tmp2[5],\
            finalSeq
        ))
        fout.close()

    def run(self):
        self.consensus()
