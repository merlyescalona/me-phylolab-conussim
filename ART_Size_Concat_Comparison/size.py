import argparse,datetime,logging,os,subprocess,sys
import numpy as np
import random as rnd
from MELoggingFormatter import MELoggingFormatter as mlf
from select import select


################################################################################
# ART PARAMETERIZATION
################################################################################
def callArt(inFile,outFile,seqNum, seqLen):
    params=["-f","100","--len","150","--mflen","250","--quiet","--paired","--sdev","50","--samout","--seqSys","HS25"]
    callParams=["art_illumina"]+params+["--in", inFile,"--out",outFile]
    appLogger.debug(" ".join(callParams))
    proc="";ngsMessageWrong=""
    try:
        proc = subprocess.check_output(callParams)
    except subprocess.CalledProcessError as error:
        ngsMessageWrong+="\n------------------------------------------------------------------------\n\n"+\
        "{}".format(error.output)
    simType = [line for line in proc.split('\n') if "simulation" in line][0].lstrip()
    cpuTime = [line for line in proc.split('\n') if "CPU" in line][0].split(":")[1]
    seed = [line for line in proc.split('\n') if "seed" in line][0].split(":")[1]
    #  output of this is in BYTES
    inputSize=str(os.path.getsize(inFile))
    outputSize=str(os.path.getsize("{0}1.fq".format(outFile))*2)+\
                str(os.path.getsize("{0}1.aln".format(outFile))*2)+\
                str(os.path.getsize("{0}.sam".format(outFile)))
    return [seqLen,seqNum,cpuTime,seed,inputSize,outputSize]


if __name__ == '__main__':

    ################################################################################
    LOG_LEVEL_CHOICES=["DEBUG","INFO","WARNING","ERROR"]
    ################################################################################
    # Logging infor
    path="."
    startTime=datetime.datetime.now()
    appLogger=logging.getLogger('art-size-comparison')
    logging.basicConfig(format="%(asctime)s - %(levelname)s:\t%(message)s",\
        datefmt="%d/%m/%Y %I:%M:%S %p",\
        filename="{0}/sizecomparison.{1:%Y}{1:%m}{1:%d}-{1:%H}:{1:%M}:{1:%S}.log".format(\
            path,startTime),\
        filemode='a',\
        level=logging.DEBUG)
    ch = logging.StreamHandler()
    loggerFormatter=mlf(fmt="%(asctime)s - %(levelname)s:\t%(message)s",datefmt="%d/%m/%Y %I:%M:%S %p")
    ch.setFormatter(loggerFormatter)
    ch.setLevel(logging.INFO)
    appLogger.addHandler(ch)
    ################################################################################
    # Read generator
    ################################################################################
    # Read size generator
    # I'm doing this scenario from 500bp to  1.000.000 bp (1Mbp) from 100bp steps.
    sizeSample=range(500,1000000, 100)
    summData=[[]]
    nucs=["A","C","G","T"]
    input1File="input1.fasta"
    input2File="input2.fasta"
    output1File="output1_R"
    output2File="output2_R"

    # for size in range(0,len(sizeSample)):

    stats=open("stats.txt","w")
    stats.write("seqLen,seqNum,cpuTime,seed,inputSize,outputSize\n")
    stats.close()
    stats=open("stats.txt","a")
    for size in range(0,len(sizeSample)):
        appLogger.info("Seq length: {0}".format(sizeSample[size]))
        seq=[ "".join(rnd.sample(nucs,1)) for pos in range(0,sizeSample[size])]
        seq="".join(seq)
        des1=">desc1";des2=">desc2"
        f=open(input1File,"w")
        f.write("{0}\n{1}".format(des1,seq))
        f.close()
        half=sizeSample[size]/2
        f=open(input2File,"w")
        f.write("{0}\n{1}\n{2}\n{3}".format(des1,seq,des2,seq))
        f.close()
        call1=callArt(input1File,output1File,1,sizeSample[size])
        call2=callArt(input2File,output2File,2,sizeSample[size])
        stats.write(",".join([str(item) for item in call1]))
        stats.write("\n")
        stats.write(",".join([str(item) for item in call2]))
        stats.write("\n")

    stats.close()
    endTime=datetime.datetime.now()
    appLogger.info("Elapsed time:\t{0}".format(endTime-startTime))
    appLogger.info("Ending at:\t{0}".format(endTime.strftime("%a, %b %d %Y. %I:%M:%S %p")))
