#########################################################
## This script obtained from Sonal Singhal 9 August 2012
## Modified with directory, file names by jgb 
#########################################################

use warnings;
use strict;

my $dir = '/home/jgb/skinkarray/nimblegen/';
my $results_dir = '/home/jgb/skinkarray/nimblegen/exons/';
my $seq1 = '/home/jgb/skinkarray/nimblegen/transcriptomes/Carlia_N_trinity.fa.final.annotated';
my $seq2 = '/home/jgb/skinkarray/nimblegen/transcriptomes/Carlia_S_trinity.fa.final.annotated';
my $seq3 = '/home/jgb/skinkarray/nimblegen/transcriptomes/Lampro_N_trinity.fa.final.annotated';
my $seq4 = '/home/jgb/skinkarray/nimblegen/transcriptomes/Lampro_C_trinity.fa.final.annotated';
my $seq5 = '/home/jgb/skinkarray/nimblegen/transcriptomes/Lampro_S_trinity.fa.final.annotated';
my $seq6 = '/home/jgb/skinkarray/nimblegen/transcriptomes/Sapro_C_trinity.fa.final.annotated';
my $seq7 = '/home/jgb/skinkarray/nimblegen/transcriptomes/Sapro_S_trinity.fa.final.annotated';

#consider all exons which are matched at least this much or greater
my $exonMatch = 0.25; 
my $anolisExons = $dir . 'anolisExons.fa';
my $np = 4;
my $eval = 1e-10; #evalue required for matches

my %compare1 = ('Sapro' => 'Lampro',
				'Lampro' => 'Carlia',
				'Carlia' => 'Sapro');
my %compare2 = ('Sapro_C' => 'Sapro_S',
				'Carlia_N' => 'Carlia_S',
				'Lampro_N' => 'Lampro_C',
				'Lampro_C' => 'Lampro_S');
##########################
# call the alignments    #
##########################

my $anolisOut = $anolisExons . ".unique";

alignment($seq1, 'Carlia_N');
alignment($seq2, 'Carlia_S');
alignment($seq3, 'Lampro_N');
alignment($seq4, 'Lampro_C');
alignment($seq5, 'Lampro_S');
alignment($seq6, 'Sapro_C');
alignment($seq7, 'Sapro_S');


##########################
# alignment  subroutine  #
##########################

	
sub alignment {
	my ($seq,$id) = @_;
		
	my %seq;
	open(SEQ, "<$seq");
	my $seqout = $seq . ".blast";
	open(SEQOUT, ">$seqout");
	while(<SEQ>) {
		chomp(my $line = $_);
		if ($line =~ m/>(\S+)/) {
			my $id = $1;
			chomp(my $seq = <SEQ>);
			$seq{$id} = $seq;
			print SEQOUT ">", $id, "\n$seq\n";
			}
		}
	close(SEQ); close(SEQOUT);
	
	my %exons;
	my $anolisBlast = $anolisOut . ".blast";
	open(ANOLEIN, "<$anolisOut");
	open(ANOLEOUT, ">$anolisBlast");
	while(<ANOLEIN>) {
		chomp(my $line = $_);
		if ($line =~ m/>(\S+)/) {
			my $id = $1;
			chomp(my $seq = <ANOLEIN>);
			$exons{$id} = $seq;
			print ANOLEOUT ">", $id, "\n$seq\n";
			}
		}
	close(ANOLEOUT); close(ANOLEIN);
	
	my $call1 = system("formatdb -i $seqout -p F") unless(-f $seqout . ".nin");
	my $call2 = system("formatdb -i $anolisBlast -p F") unless(-f $anolisBlast . ".nin");
	my $out1 = $seqout . ".blast.out";
	my $out2 = $seqout . ".recipblast.out";
	my $call3 = system("blastall -p blastn -d $seqout -i $anolisBlast -m 8 -a $np -b 1 -o $out1 -e $eval") unless(-f $out1);
	my $call4 = system("blastall -p blastn -d $anolisBlast -i $seqout -m 8 -b 1 -o $out2 -a $np -e $eval") unless (-f $out2);
		
	my %matches;
	open(IN, "<$out1");
	while(<IN>) {
		chomp(my $line = $_);
		my @d = split(/\t/,$line);
		$matches{$d[0]}{$d[1]} = 'no';
		}
	close(IN);
		
	open(IN, "<$out2");
	while(<IN>) {
		chomp(my $line = $_);
		my @d = split(/\t/,$line);
		if ($matches{$d[1]}{$d[0]}) {
			$matches{$d[1]}{$d[0]} = 'yes';
			}
		}
	close(IN);
	
	foreach my $exon (keys %exons) {
		if ($matches{$exon}) {
			foreach my $match (keys %{$matches{$exon}}) {
				if ($matches{$exon}{$match} eq 'yes') {
					my $out1 = 'exonHomolog.fa';
					open(OUT1, ">$out1");
					print OUT1 ">", $exon, "\n" , $exons{$exon}, "\n";
					my $elength = length($exons{$exon});
					close(OUT1);

					my $out2 = 'exonHomolog2.fa';
					open(OUT2, ">$out2");
					print OUT2 ">", $match, "\n" , $seq{$match}, "\n";
					close(OUT2);
		
					my @call = `exonerate -m coding2coding $out1 $out2 --showvulgar yes --showalignment no`;
		
					if ($call[3]) {
						if ($call[3] =~ m/vulgar/) {
							my @d = split(/\s/,$call[3]);
							my $length = abs($d[6] - $d[7]);					
							my $sub; 
							my $start;
			
							if ($d[4] =~ m/\+/ && $d[8] =~ m/\+/) {
								$start = $d[6];
								$sub = substr $seq{$match}, $start, $length;
								}
							elsif ($d[4] =~ m/\+/ && $d[8] =~ m/\-/) {
								$start = $d[7];
								$sub = substr $seq{$match}, $start, $length;
								$sub = reverse($sub);
								$sub =~ tr/ATGCatgc/TACGtacg/;
								}
							elsif ($d[4] =~ m/\-/ && $d[8] =~ m/\-/) {
								$start = $d[7];
								$sub = substr $seq{$match}, $start, $length;
								}
							elsif ($d[4] =~ m/\-/ && $d[8] =~ m/\+/) {
								$start = $d[6];
								$sub = substr $seq{$match}, $start, $length;
								$sub = reverse($sub);
								$sub =~ tr/ATGCatgc/TACGtacg/;
								}
				
							if ($length/$elength >= $exonMatch) {
								my $out = $results_dir . $exon . ".fa";
								open(FINAL, ">>$out");
								my $end = $start + $length - 1;
								my $loc = $match . '_' . $start . "_" . $end;
								print FINAL ">$id\t$loc\n", $sub, "\n";
								close(FINAL);
								}
							}				
						}	
					}
				}
			}
		}		
	my $cleanup = system("rm exonHomolog.fa exonHomolog2.fa");
	}



