#########################################################
## This script obtained from Sonal Singhal 9 August 2012
## Modified with directory, file names by jgb 
#########################################################

use warnings;
use strict;

my $dir = '/home/jgb/skinkarray/nimblegen/';
my $results_dir = '/home/jgb/skinkarray/nimblegen/exons/';
my $seq1 = '/home/jgb/skinkarray/nimblegen/transcriptomes/Carlia_N_trinity.fa.final.annotated';
my $seq2 = '/home/jgb/skinkarray/nimblegen/transcriptomes/Carlia_S_trinity.fa.final.annotated';
my $seq3 = '/home/jgb/skinkarray/nimblegen/transcriptomes/Lampro_N_trinity.fa.final.annotated';
my $seq4 = '/home/jgb/skinkarray/nimblegen/transcriptomes/Lampro_C_trinity.fa.final.annotated';
my $seq5 = '/home/jgb/skinkarray/nimblegen/transcriptomes/Lampro_S_trinity.fa.final.annotated';
my $seq6 = '/home/jgb/skinkarray/nimblegen/transcriptomes/Sapro_C_trinity.fa.final.annotated';
my $seq7 = '/home/jgb/skinkarray/nimblegen/transcriptomes/Sapro_S_trinity.fa.final.annotated';

my $exonLength = 200;
#consider all exons which are matched at least this much or greater
my $exonMatch = 0.25; 
my $anolisExons = $dir . 'anolisExons.fa';
#my $anolisExons = "/home/jgb/genomes/anolis_carolinensis/cdna/Anolis_carolinensis.AnoCar2.0.67.cdna.all.fa";
my $exonFile = $dir . 'uniqueExons.fa';
my $np = 4;
my $eval = 1e-10; #evalue required for matches
my $divout = $dir . 'divergence.out';
my %compare1 = ('Sapro' => 'Lampro',
				'Lampro' => 'Carlia',
				'Carlia' => 'Sapro');
my %compare2 = ('Sapro_C' => 'Sapro_S',
				'Carlia_N' => 'Carlia_S',
				'Lampro_N' => 'Lampro_C',
				'Lampro_C' => 'Lampro_S');
my $minGC = 0.3;
my $maxGC = 0.7;

###############################
# align, calculate divergence #
###############################

alignHomologs();
calculateDiv();

##########################
# behold the subroutines #
##########################

sub calculateDiv {
	my @aln = <$results_dir*aln>;
	open(OUT, ">$divout");
	
	foreach my $aln (@aln) {
		open(IN, "<$aln");
		my %species; my %genus; my $id; my $genus; 
				
		while(<IN>) {
			chomp(my $line = $_);
			if ($line =~ m/>(\S+)/) {
				$id = $1;
				$genus = $1 if $id =~ m/(\S+)_/;
				}
			else {
				$species{$id} .= $line;
				$genus{$genus} .= $line;
				}
			}
			
		my @div;	
		my $zero = 0;
		foreach my $c1 (keys %compare1) {
			my $div = div($genus{$c1},$genus{$compare1{$c1}});
			$div = sprintf("%.4f",$div) unless ($div eq 'NA');
			unless ($div eq 'NA') {
				$zero++ if $div == 0;
				}
			push(@div,$div);
			}
			
			
		foreach my $c1 (keys %compare2) {
			my $div = 'NA';
			if ($species{$c1} && $species{$compare2{$c1}}) {
				$div = div($species{$c1},$species{$compare2{$c1}});		
				unless ($div eq 'NA') {
					$div = sprintf("%.4f",$div);					
					}
				}
			push(@div,$div);	
			}	
			
		my @length; my @gc; my $length = 0;
		foreach my $c (keys %species) {
			my $seq = $species{$c};
			my $l = ($seq =~ s/[atgc]//ig);
			$seq = $species{$c};
			my $gc = ($seq =~ s/[gc]//ig);
			$length++ if $l < $exonLength;
			push(@length,$l);
			push(@gc,$gc);
			}
		my $avgLength = sprintf("%d",average(\@length));	
		my $avgDiv = sprintf("%.4f",average(\@div));
		my $avgGC = sprintf("%.4f",average(\@gc)/$avgLength);
		my $var = sprintf("%.4f",variance($avgDiv,\@div));
		my $name = $1 if $aln =~ m/(ENSA.*exon\d+)/;
		
		unless ($length) {
			unless ($zero) {
				unless ($var > 0.002) {
					if ($avgLength > $exonLength) {
						if ($avgGC > $minGC && $avgGC < $maxGC) {
							print OUT $name, "\t", $avgLength, "\t", $avgDiv, "\t", $var, "\t", $avgGC, "\t";
							print OUT join("\t",@div);
							print OUT "\n";
							}
						}
					}
				}
			}	
		}
	close(OUT);	
	}
	
sub variance {
	my ($avg, $a) = @_;
	my @a = @{$a};
	my $var;
	if (scalar(@a) > 0) {
		my $sum = 0; my $num;
		foreach my $var (@a) {
			unless ($var eq 'NA') {
				$sum += ($avg - $var) ** 2;
				$num++;
				}
			}	
		$var = $sum/$num;
		}
	else {
		$var = 0;
		}
	return($var);
	}		
	
sub average {
	my ($a) = @_;
	my @a = @{$a};
	my $avg;
	if (scalar(@a) > 0) {
		my $sum = 0; my $num;
		foreach my $var (@a) {
			unless ($var eq 'NA') {
				$sum += $var;
				$num++;
				}
			}	
		$avg = $sum/$num;
		}
	else {
		$avg = 0;
		}
	return($avg);
	}	
	
sub div {	
	my ($seq1,$seq2) = @_;
	my @b1 = split(//,$seq1);
	my @b2 = split(//,$seq2);
	
	my %ti = ('A' => 'G', 'G' => 'A', 'C' => 'T', 'T' => 'C');
	
	my $gc; my $ti = 0; my $tv = 0; my $l = 0;

	for (my $j = 0; $j < scalar(@b1); $j++) {
		if (uc($b1[$j]) =~ m/[A|T|G|C]/i) {
			if (uc($b1[$j]) =~ m/[G|C]/i) {
				$gc++;
				}	
			if (uc($b2[$j]) =~ m/[A|T|G|C]/) {	
				$l++;
				unless (uc($b1[$j]) eq uc($b2[$j])) {
					#this is a mutation!
					if ( $ti{uc($b1[$j])} eq uc($b2[$j]) ) {
						$ti++;
						}
					else {	
						$tv++;
						}
					}	
				}	
			}
		}
				
	my $div;	
	if ($l > 0) {	
		my $p = $ti/$l; my $q = $tv/$l; my $w = $gc/$l;
				
		if ($w == 1) {
			$div = 'NA';
			}
		else {	
			my $a = 1 - ( $p/ (2 * $w * (1 - $w) ) ) - $q;
			my $b = 1 - 2 * $q ;
			if ($a <= 0 || $b <= 0 ) {
				$div = 'NA';
				}
			else {	
				$div = (-2*$w) * ( 1 - $w) *  log( $a ) - 0.5 * ( 1 - 2 * $w * ( 1 - $w ) ) * log($b);
				}
			}	
		}
	else {
		$div = 'NA';
		}
	return($div);	
	}	

sub alignHomologs {
	my @seq = <$results_dir*ENS*fa>;
	
	foreach my $file (@seq) {
		open(IN, "<$file");
		my %g; my %s;
		while(<IN>) {
			chomp(my $line = $_);
			if ($line =~ m/>(\S+)/) {
				my $genus = $1 if $line =~ m/>(\S+)_/;
				my $species = $1 if $line =~ m/>(\S+)/;
			
				$g{$genus}++; $s{$species}++;
				}
			}
		close(IN);
		if (scalar(keys %g) > 2) {
			my $out = $file . ".aln";
			my $call1 = system("muscle -in $file -out $out");
			my $call2 = system("rm $file") if (-f $out);
			}
		}
	}
	

		
		
