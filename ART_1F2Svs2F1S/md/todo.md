- This report has to be added to the SimPhy wrapper report, as an explanation on why
the program generates a multiple sequence file.
- Also, each dataset belonging to a strand needs to be mapped against the reference
sequence in order to notice the coverage of each strand as compared to the one
expected and compared to the reference sequence.
