From the first image, related to a comparison of the different approaches,
we can see the similitude of coverage in both cases. Also, the pattern
in which the reads were mapped are similar.

When we check out the second scenario, checking the coverage of both strands separately,
related to the one we see above, we note that coverage is divided `~50%` of coverage
per strand, which is expected.
