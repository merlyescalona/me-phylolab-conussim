# Synopsis

The general purpose of the pipeline we are working on is to generate
next-generation sequencing (NGS) reads out of phylogenies. Pipeline includes
the generation of phylogenies with `SimPhy` (**Mallo et al. 2015**), from them
later obtain sequences using `INDELible` (**Fletcher 2009**), mate them and finally,
generate the reads with `ART` (**Huang et al. 2011**).

Specific process between mating and ngs-simulation includes the generation of
files with the sequences of the diploid individuals. For this process it is
possible to generate one (`1`) or (`2`) files. When generating one (`1`) file, I'm
storing both sequences of the individual, and when generating two (`2`), I'm putting
one sequence in each file. This situation, jointly with the fact that ART is able to
receive as input several sequences in a file, generated doubts on how ART would deal
with the generation of sequences and how it would affect the mapping. Questions
that I'm trying to solve with this analysis.

# Data processing

In order to make this analysis I took a reference sequence, and a individual from previous simulations.

- Individual:
    - `APC_1_001_data_10_5_0_0_S1.fasta` (`data/fa/seqS1.fasta`)
    - `APC_1_001_data_10_5_0_4_S2.fasta` (`data/fa/seqS2.fasta`)
    - `APC_1_001_data_10.fasta` (`data/fa/sedDip.fasta`)
- Reference:
    - `REF_LOC1RND_data_1_001.fasta`  (`data/fa/reference.fasta`)

Process that's going to be follow to accomplish the goals of this analysis includes:

1. Generation of reads: Using two situations:
    a. A single file with the two strand sequences corresponding a single individual.
    b. Two files, each with a strand sequence, both corresponding the same single individual.
2. Mapping of the reads to the reference sequencing.
3. Conversion, sorting and merging files.
4. Extraction of mapping and coverage information.

## NGS Simulation

The generation of reads include a set of parameters that are going to be fixed
for both situations, and coincide with the parameterization used for the greater
simulation scenarios.

I'm going to simulate paired-end sequencing. Size of the reads is going to be 150 bp.
The fragment size from which the reads are going to be obtained is 250, with a standard
deviation of 50. Desired coverage will be of 100x. Finally, sequencing machine is going
to be HiSeq2500, which has a built-in profile in ART (HS25).

```
module load bio/art/050616
# Running NGS for a single file, 2 sequence
art_illumina -f 100 -l 150 -p  -m 250 -s 50 -rs $RANDOM -ss HS25 -i data/fa/seqDip.fasta -o data/reads/seqDip_R > output/seqDip.out
# Running NGS for a 2 files with 1 sequence
art_illumina -f 100 -l 150 -p  -m 250 -s 50 -rs $RANDOM -ss HS25 -i data/fa/seqS1.fasta -o data/reads/seqS1_R > output/seqS1.out
art_illumina -f 100 -l 150 -p  -m 250 -s 50 -rs $RANDOM -ss HS25 -i data/fa/seqS2.fasta -o data/reads/seqS2_R > output/seqS2.out
```

## Mapping

Using `bwa`, I generated the mapping of the reads to the reference sequence, again
for the 2 situations.

```
ref="data/fa/reference.fasta"
module load bio/bwa/0.7.10
bwa index -a is $ref
# Mapping 1 file 2 seqs
infile1="data/reads/seqDip_"
outfile1="data/sam/seqDip.sam"
RGID="1"
SMID="1F2S"
bwa mem -t 1 -R "@RG\tID:${RGID}\tSM:${SMID}\tPL:Illumina\tLB:${RGID}\tPU:HS25" ${ref} ${infile1}R1.fq ${infile1}R2.fq > $outfile1
# Mapping 1 file 2seqs
infile1="data/reads/seqS1_"
outfile1="data/sam/seqS1.sam"
RGID="2"
SMID="2F1Sa"
bwa mem -t 1 -R "@RG\tID:${RGID}\tSM:${SMID}\tPL:Illumina\tLB:${RGID}\tPU:HS25" ${ref} ${infile1}R1.fq ${infile1}R2.fq > $outfile1
infile1="data/reads/seqS2_"
outfile1="data/sam/seqS2.sam"
RGID="2"
SMID="2F1Sb"
bwa mem -t 1 -R "@RG\tID:${RGID}\tSM:${SMID}\tPL:Illumina\tLB:${RGID}\tPU:HS25" ${ref} ${infile1}R1.fq ${infile1}R2.fq > $outfile1
```


## Sorting, Conversion and merging.

Completed the sorting, conversion and merging of the dataset. Sorting and conversion for
the 2 situations, and the merging for the second one. All of this using `samtools`

```
module load bio/samtools/1.2.0
# Sorting and converting to BAM (1F2S)
samtools view -bSh data/sam/seqDip.sam | samtools sort - data/bam/seqDip.sorted
samtools index data/bam/seqDip.sorted.bam

# Marging, Sorting and converting to BAM (2F1S)
samtools view -bSh data/sam/seqS1.sam | samtools sort - data/bam/seqS1.sorted
samtools view -bSh data/sam/seqS2.sam | samtools sort - data/bam/seqS2.sorted
samtools merge data/bam/seqS1S2.sorted.merge.bam data/bam/seqS1.sorted.bam data/bam/seqS2.sorted.bam
samtools index data/bam/seqS1S2.sorted.merge.bam
```

## Extraction of quality statistics from the alignments

Also, with the use of `samtools` I extracted information about the mapped reads. Specifically:

- quality control
- number of mapped and unmapped reads
- number of properly mapped/unmapped reads

As for the coverage, I got the coverage per position.


### Quality control

```
samtools index data/fa/reference.fasta
samtools stats -r data/fa/reference.fasta seqDip.sorted.bam > seqDip.sorted.bam.stats
samtools stats -r data/fa/reference.fasta seqS1S2.sorted.merge.bam > seqS1S2.sorted.bam.stats
plot-bamstats -s data/fa/reference.fasta > data/fa/reference.fasta.gc
plot-bamstats -r data/fa/reference.fasta.gc -p ../bam/seqDip.stats/ ../bam/seqDip.sorted.bam.stats
plot-bamstats -r data/fa/reference.fasta.gc -p ../bam/seqS1S2.stats/ ../bam/seqS1S2.sorted

```
