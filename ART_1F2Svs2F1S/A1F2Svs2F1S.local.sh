<<COMMENT
Data used for this test:
Sequence:
  APC_1_001_data_10_5_0_0_S1.fasta (data/fa/seqS1.fasta)
  APC_1_001_data_10_5_0_4_S2.fasta (data/fa/seqS2.fasta)
  APC_1_001_data_10.fasta (data/fa/sedDip.fasta)
Reference:
  REF_LOC1RND_data_1_001.fasta  (data/reference.fasta)

I'm going to generate reads for both "datasets", mapped them against the Reference
and see statistics related to the mapped reads and coverage.
COMMENT

mkdir -p data/fa data/reads data/sam output data/bam

################################################################################
# NGS Generation
################################################################################
module load bio/art/050616
# Running NGS for a single file, 2 sequence
art_illumina -f 100 -l 150 -p  -m 250 -s 50 -rs $RANDOM -ss HS25 -i data/fa/seqDip.fasta -o data/reads/seqDip_R > output/seqDip.out

# Running NGS for a 2 files with 1 sequence
art_illumina -f 100 -l 150 -p  -m 250 -s 50 -rs $RANDOM -ss HS25 -i data/fa/seqS1.fasta -o data/reads/seqS1_R > output/seqS1.out
art_illumina -f 100 -l 150 -p  -m 250 -s 50 -rs $RANDOM -ss HS25 -i data/fa/seqS2.fasta -o data/reads/seqS2_R > output/seqS2.out


################################################################################
# BWA
################################################################################
ref="data/fa/reference.fasta"

module load bio/bwa/0.7.10
bwa index -a is $ref

# Mapping 1 file 2 seqs
infile1="data/reads/seqDip_"
outfile1="data/sam/seqDip.sam"
RGID="1"
SMID="1F2S"
bwa mem -t 1 -R "@RG\tID:${RGID}\tSM:${SMID}\tPL:Illumina\tLB:${RGID}\tPU:HS25" ${ref} ${infile1}R1.fq ${infile1}R2.fq > $outfile1

# Mapping 1 file 2seqs
infile1="data/reads/seqS1_"
outfile1="data/sam/seqS1.ssm"
RGID="2"
SMID="2F1Sa"
bwa mem -t 1 -R "@RG\tID:${RGID}\tSM:${SMID}\tPL:Illumina\tLB:${RGID}\tPU:HS25" ${ref} ${infile1}R1.fq ${infile1}R2.fq > $outfile1

infile1="data/reads/seqS2_"
outfile1="data/sam/seqS2.sam"
RGID="2"
SMID="2F1Sb"
bwa mem -t 1 -R "@RG\tID:${RGID}\tSM:${SMID}\tPL:Illumina\tLB:${RGID}\tPU:HS25" ${ref} ${infile1}R1.fq ${infile1}R2.fq > $outfile1

################################################################################
# Samtools
################################################################################
module load bio/samtools/1.2.0
# Sorting and converting to BAM (1F2S)
samtools view -bSh data/sam/seqDip.sam | samtools sort - data/bam/seqDip.sorted
samtools index data/bam/seqDip.sorted.bam

# Marging, Sorting and converting to BAM (2F1S)
samtools view -bSh data/sam/seqS1.sam | samtools sort - data/bam/seqS1.sorted
samtools view -bSh data/sam/seqS2.sam | samtools sort - data/bam/seqS2.sorted
samtools merge data/bam/seqS1S2.sorted.merge.bam data/bam/seqS1.sorted.bam data/bam/seqS2.sorted.bam
samtools index data/bam/seqS1S2.sorted.merge.bam
samtools index data/bam/seqS1.sorted.bam
samtools index data/bam/seqS2.sorted.bam


################################################################################
# Coverage and mapping statistics
################################################################################
echo "FILEPATH,NUM_MAPPED,NUM_UNMAPPED,NUM_PROPERLY_MAPPED,NUM_NOT_PROPERLY_MAPPED" > mapping.info.txt
dip="data/bam/seqDip.sorted.bam"
s1s2="data/bam/seqS1S2.sorted.merge.bam"
echo "$dip,$(samtools view -c -F 4 $dip),$(samtools view -c -f 4 $dip),$(samtools view -c -f 2 $dip),$(samtools view -c -F 2 $dip)" >> mapping.info.txt
echo "$s1s2,$(samtools view -c -F 4 $s1s2),$(samtools view -c -f 4 $s1s2),$(samtools view -c -f 2 $s1s2),$(samtools view -c -F 2 $s1s2)" >> mapping.info.txt
################################################################################
# Coverage
################################################################################
samtools depth $dip > seqDip.coverage.txt
samtools depth $s1s2 > seqS1S2.coverage.txt


################################################################################
# Quality of the alignments BAM
################################################################################
samtools index data/bam/seqDip.sorted.bam
samtools stats -r data/fa/reference.fasta seqDip.sorted.bam > data/bam/seqDip.sorted.bam.stats
samtools index data/bam/seqS1S2.sorted.merge.bam
samtools stats -r data/fa/reference.fasta seqS1S2.sorted.merge.bam > data/bam/seqS1S2.sorted.bam.stats
plot-bamstats -s data/fa/reference.fasta > data/fa/reference.fasta.gc
plot-bamstats -r data/fa/reference.fasta.gc -p data/bam/seqDip.stats/ data/bam/seqDip.sorted.bam.stats
plot-bamstats -r data/fa/reference.fasta.gc -p data/bam/seqS1S2.stats/ data/bam/seqS1S2.sorted
