
        set terminal png size 600,400 truecolor
        set output "../bam/seqS1S2.stats/indel-cycles.png"
        set grid xtics ytics y2tics back lc rgb "#cccccc"
        set style line 1 linetype 1  linecolor rgb "red"
        set style line 2 linetype 2  linecolor rgb "black"
        set style line 3 linetype 3  linecolor rgb "green"
        set style line 4 linetype 4  linecolor rgb "blue"
        set style increment user
        set ylabel "Indel count"
        set xlabel "Read Cycle"
        set title "seqS1S2.sorted.bam.stats"
    plot '-' w l ti 'Insertions (fwd)', '' w l ti 'Insertions (rev)', '' w l ti 'Deletions (fwd)', '' w l ti 'Deletions (rev)'
4	0
67	0
146	0
end
4	0
67	1
146	0
end
4	0
67	0
146	1
end
4	1
67	0
146	0
end
