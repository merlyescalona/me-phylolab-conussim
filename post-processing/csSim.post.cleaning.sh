#!/bin/bash
###############################################################################
# Constants
###############################################################################
pipelinesName="csSim1"
stReplicates=1
numDigits=${#stReplicates}
nGTs=500
numGeneTrees="f:${nGTs}}"
numDigitsGTs=${#nGTs}
sizeLoci="1000bp"
prefix="data"
timeRange="u:20000,20000000" # 200k to 20My
nIndsTaxa="f:10" # Number of individuals per taxa (actual num of ind will be 5)
nTaxa="f:10" # Number of taxa
subsRate="f:10000000" # 10^8
################################################################################
# Program paths
################################################################################
fastqc="/home/merly/src/fastqc/fastqc"
################################################################################
# Folders
################################################################################
PHYLOLAB="/home/uvi/be/mef/mnt/phylolab"
WD="$PHYLOLAB/$USER/$pipelinesName"
profilePath="$HOME/data/csNGSProfile"
pair1Path="$HOME/data/csNGSProfile/csNGSProfile_hiseq2500_1.txt"
pair2Path="$HOME/data/csNGSProfile/csNGSProfile_hiseq2500_2.txt"
outputFolder="$WD/report/output"
usageFolder="$WD/report/usage"
scriptsFolder="$WD/report/scripts"
filesFolder="$WD/report/files"
controlFolder="$WD/report/controlFiles"
jobsSent="$outputFolder/$pipelinesName.jobs"
readsFolder="$WD/reads"
alnReadsFolder="$WD/reads/aln"
fqReadsFolder="$WD/reads/fq"
###############################################################################
# STEP 0. Quality control
###############################################################################
# Done with FastQC
$fastqc $WD/reads/fq/* --outdir=$WD/report/qc/
<<CleaningProcess
1. Trim [Trimmomatic, cutadapt, seq_crumbs]
  a. (Only for empirical) Remove adapters [SCYTHE]
  b. Read ends with low quality bases [SICKLE, PRINSEQ]
2. Filter out:
  a. Identical forward and reverse reads, to avoid inflating coverage estimates
  b. Exact PCR read duplicates [Picard MarkDuplicates]
  c. Low complexity reads (NCBI dust, seq_crumbs)
  d. Low quality reads  QS <20 (Need to check this threshold) [Illumiprocessor, PRINSEQ, seq_crumbs]
  e. Reads with ambiguous (Ns) bases [Illumiprocessor]
3. Merge overlapping forward-reverse paired-end reads [FLASH,COPEread, Alan Merge]
4. (Optional) If contamination can be an issue, remove reads matching contaminant sources
  a. Can be done aligning reads to contaminant reference genome [bowtie2]
CleaningProcess
###############################################################################
# STEP 1. Trimming read ends with low quality
###############################################################################
#Test Case 1: SICKLE
# Dataset in /media/merly/PL-MerlyEscalona-BAK/conusSim/dataset

testDataset="/media/merly/PL-MerlyEscalona-BAK/conusSim/dataset"
sickleTest="/media/merly/PL-MerlyEscalona-BAK/conusSim/sickleTest_quality"
mkdir $sickleTest
for item in $(ls $testDataset/*R1.fq); do
  base=$(basename $item R1.fq)
  echo $base
  sickle pe -f "${base}R1.fq" -r "${base}R2.fq" -o "${sickleTest}/${base}R1.filtered.fq" -p "${sickleTest}/${base}R2.filtered.fq" -t sanger -s "${sickleTest}/${base}.trimmed.fq"
done

###############################################################################
# STEP 1.1,2. Quality control again
###############################################################################
cat $sickleTest/*.fq >sickleTest.all.fq
mkdir $sickleTest/qc/
$fastqc $sickleTest/* --outdir=$sickleTest/qc/



###############################################################################
# STEP 1.2 Trimming read ends with low quality
###############################################################################
#Test Case 1: PRINSEQ
# Dataset in /media/merly/PL-MerlyEscalona-BAK/conusSim/dataset

testDataset="/media/merly/PL-MerlyEscalona-BAK/conusSim/dataset"
prinseqTest="/media/merly/PL-MerlyEscalona-BAK/conusSim/prinseq_output"
prinseqGood="/media/merly/PL-MerlyEscalona-BAK/conusSim/prinseq_output/good"
prinseqBad="/media/merly/PL-MerlyEscalona-BAK/conusSim/prinseq_output/bad"
prinseqExe="/home/merly/src/prinseq-lite-0.20.4/prinseq-lite.pl"
mkdir $prinseqTest $prinseqGood $prinseqBad
QSDef=20
base="allSeqs"
perl $prinseqExe -fastq "${base}.R1.fq" -fastq2 "${base}.R2.fq" -out_format 3 -out_good "${prinseqTest}/${base}.good.fq" -out_bad "${prinseqTest}/${base}.bad.fq" -qual_noscale -trim_qual_left 20 -trim_qual_right 20
