#!/bin/bash
###############################################################################
# Constants
###############################################################################
pipelinesName="csSim0"
stReplicates=1
numDigits=${#stReplicates}
nGTs=100
numGeneTrees="f:${nGTs}"
numDigitsGTs=${#nGTs}
sizeLoci="500bp"
prefix="data"
timeRange="u:20000,20000000" # 200k to 20My
nIndsTaxa="f:6" # Number of individuals per taxa (actual num of ind will be 5)
nTaxa="f:5" # Number of taxa
subsRate="f:10000000" # 10^8
prefixLoci="data"
prefixRef="LOC1RND"
################################################################################
# Program paths
################################################################################
filterEven="$HOME/src/me-phylolab-conussim/conusSim.filtering.even.R"
matingProgram="$HOME/src/me-phylolab-mating/mating.py"
LociFiltering="$HOME/src/me-phylolab-mating/LociFiltering.py"
lociReferenceSelection="$HOME/src/me-phylolab-conussim/LociRefSelection/locirefsel.main.py"
art="$HOME/bin/art_illumina"
simphy="$HOME/bin/simphy"
indelible="$HOME/bin/indelible"
wrapper="$HOME/bin/INDELIble_wrapper.pl"
diversity="$HOME/bin/diversity"
fastqc="$HOME/apps/0.11.5/fastqc"
bwa="$HOME/apps/bwa/0.7.13/bwa"
PICARD="$HOME/apps/picard/2.2.4/picard.jar"
################################################################################
# Folders
################################################################################
PHYLOLAB="/home/uvi/be/mef/mnt/phylolab"
WD="$PHYLOLAB/$USER/$pipelinesName"
profilePath="$HOME/data/csNGSProfile"
pair1Path="$HOME/data/csNGSProfile/csNGSProfile_hiseq2500_1.txt"
pair2Path="$HOME/data/csNGSProfile/csNGSProfile_hiseq2500_2.txt"
outputFolder="$WD/report/output"
usageFolder="$WD/report/usage"
scriptsFolder="$WD/report/scripts"
filesFolder="$WD/report/files"
controlFolder="$WD/report/controlFiles"
jobsSent="$outputFolder/$pipelinesName.jobs"
readsFolder="$WD/reads"
alnReadsFolder="$WD/reads/aln"
fqReadsFolder="$WD/reads/fq"
samReadsFolder="$WD/reads/sam"
referencesFolder="$WD/references"
procSam="$WD/proc/sam"
procBam="$WD/proc/bam"
procSorted="$WD/proc/sorted"
procDup="$WD/proc/dup"
###############################################################################
# Creating folder for processing
mkdir -p $procBam $procSam $procSorted $procDup
for line in $(tail -n+2  $WD/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  mkdir -p $procSam/$st/$gt/$ind $procBam/$st/$gt/$ind $procSorted/$st/$gt/$ind $procDup/$st/$gt/$ind
done

###############################################################################
# Mapping with BWA
###############################################################################
# STEP 1 Indexing the reference sequences
###############################################################################
echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.p.1.o
#$ -e $outputFolder/$pipelinesName.p.1.e
#$ -N $pipelinesName.p.1

cd $referencesFolder"  > $scriptsFolder/$pipelinesName.p.1.sh

for line in $(cat $WD/$pipelinesName.evens); do
  st=$(printf "%0$numDigits""g" $line)
  for indexGT in $(seq 1 $nGTs); do
    gt=$(printf "%0$numDigitsGTs""g" $indexGT)
    echo "bwa index -a is REF_${prefixRef}_${prefixLoci}_${st}_${gt}.fasta"  >> $scriptsFolder/$pipelinesName.p.1.sh
  done
done

jobID=$(qsub -l num_proc=1,s_rt=0:10:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.p.1.sh | awk '{ print $3}')
echo "$pipelinesName"".p.1    $jobID" >> $jobsSent
ls -Rl $WD > $filesFolder/$pipelinesName.p.1.files
echo "$pipelinesName"".p.1    $jobID" >> $usageFolder/$pipelinesName.p.1.usage
cat $outputFolder/$pipelinesName.p.1.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.p.1.usage
cat $outputFolder/$pipelinesName.p.1.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.p.1.usage

###############################################################################
# STEP 2 Getting alignments
# Working with Illumina PE reads longer than 70 bp -> use bwa mem
# Always preferrably by individuals
# WARNING!!! Everything is being run in the same script and for qsubbing
# it will be necessary to estimate the time the job will take to run all the
# bwa mem.
###############################################################################


echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.p.2.o
#$ -e $outputFolder/$pipelinesName.p.2.e
#$ -N $pipelinesName.p.2

"  > $scriptsFolder/$pipelinesName.p.2.sh

counter=1
totalNumR1Files=$(find $readsFolder/fq/ -name *_R1.fq | wc -l |awk '{ print $1}')
for line in $(tail -n+2  $WD/$pipelinesName.mating); do
  array=(${line//,/ })
  # Have 5 elems
  st=${array[0]}
  gt=${array[1]}
  ind=${array[2]}
  sp=${array[3]}
  m1=${array[4]}
  m2=${array[5]}

  ref="$referencesFolder/REF_${prefixRef}_${prefixLoci}_${st}_${gt}.fasta"
  infile1="$readsFolder/fq/${st}/${gt}/${ind}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}_${sp}_0_${m1}_S1_"
  infile2="$readsFolder/fq/${st}/${gt}/${ind}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}_${sp}_0_${m2}_S2_"
  outfile1="$procSam/${st}/${gt}/${ind}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}_${sp}_0_${m1}_S1.sam"
  outfile2="$procSam/${st}/${gt}/${ind}/${pipelinesName}_${st}_${gt}_${prefixLoci}_${ind}_${sp}_0_${m2}_S2.sam"

  RGID=$(printf "%0${#totalNumR1Files}""g" $counter)
  echo "bwa mem -t 4 -R \"@RG\tID:${RGID}\tSM:${RGID}\tPL:Illumina\tLB:${RGID}\tPU:HiSeq2500\" ${ref} ${infile1}R1.fq ${infile1}R2.fq > $outfile1" >> $scriptsFolder/$pipelinesName.p.2.sh
  let counter=counter+1
  RGID=$(printf "%0${#totalNumR1Files}""g" $counter)
  echo "bwa mem -t 4 -R \"@RG\tID:${RGID}\tSM:${RGID}\tPL:Illumina\tLB:${RGID}\tPU:HiSeq2500\" ${ref} ${infile2}R1.fq ${infile2}R2.fq > $outfile2" >> $scriptsFolder/$pipelinesName.p.2.sh
  let counter=counter+1
done

jobID=$(qsub -l num_proc=4,s_rt=1:00:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.p.2.sh | awk '{ print $3}')


echo "$pipelinesName"".p.2    $jobID" >> $jobsSent
ls -Rl $WD > $filesFolder/$pipelinesName.p.2.files
echo "$pipelinesName"".p.2    $jobID" >> $usageFolder/$pipelinesName.p.2.usage
cat $outputFolder/$pipelinesName.p.2.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.p.2.usage
cat $outputFolder/$pipelinesName.p.2.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.p.2.usage




###############################################################################
# STEP 3. Conversion SAM2BAM and sorting.
###############################################################################

SEEDFILE="$scriptsFolder/$pipelinesName.p.3.files"
find $procSam -name *.sam > $SEEDFILE
totalSamtoolsJobs=$(wc -l $SEEDFILE | awk '{print $1}')
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.p.3.o
#$ -e $outputFolder/$pipelinesName.p.3.e
#$ -N $pipelinesName.p.3
module load samtools
SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE)
OUTPUTBASE=\$(echo \$SEED | sed 's/proc\/sam/proc\/sorted/g' | sed 's/\.sam/\.sorted/g')

echo \$OUTPUTBASE
samtools view -bS \$SEED | samtools sort - \$OUTPUTBASE
module unload samtools
"  > $scriptsFolder/$pipelinesName.p.3.sh


jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-$totalSamtoolsJobs $scriptsFolder/$pipelinesName.p.3.sh | awk '{ print $3}')


echo "$pipelinesName"".p.3    $jobID" >> $jobsSent
ls -Rl $WD > $filesFolder/$pipelinesName.p.3.files
echo "$pipelinesName"".p.3    $jobID" >> $usageFolder/$pipelinesName.p.3.usage
cat $outputFolder/$pipelinesName.p.3.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.p.3.usage
cat $outputFolder/$pipelinesName.p.3.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.p.3.usage

###############################################################################
# STEP 3.1. Geetting number of mapped reads per file
# http://crazyhottommy.blogspot.com.es/2013/06/count-how-many-mapped-reads-in-bam-file.html
###############################################################################
echo -e "#! /bin/bash
#$ -m bea
#$ -M escalona10@gmail.com
#$ -o $outputFolder/$pipelinesName.p.3.1.o
#$ -e $outputFolder/$pipelinesName.p.3.1.e
#$ -N $pipelinesName.p.3.1

module load samtools

for line in \$(find $WD/proc/sorted -name *.sorted.bam ); do
  mappedReads=\$(samtools view -c -F 4 \$line)
  unmappedReads=\$(samtools view -c -f 4 \$line)
  echo \"\$line\\t\$mappedReads\" >> $WD/${pipelinesName}.mapped.reads.txt
  echo \"\$line\\t\$unmappedReads\" >> $WD/${pipelinesName}.unmapped.reads.txt
done

module unload samtools

" >  $scriptsFolder/$pipelinesName.p.3.1.sh
jobID=$(qsub -l num_proc=1,s_rt=0:05:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.p.3.1.sh | awk '{ print $3}')
echo "$pipelinesName"".p.3    $jobID" >> $jobsSent
echo "$pipelinesName"".p.3    $jobID" >> $usageFolder/$pipelinesName.p.3.usage
cat $outputFolder/$pipelinesName.p.3.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.p.3.usage
cat $outputFolder/$pipelinesName.p.3.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.p.3.usage


<<SAMTOOLS
http://davetang.org/wiki/tiki-index.php?page=SAMTools
# Converstion SAM to BAM
samtools view -bS test.sam > test.bam
# SORTING BAM
samtools sort test.bam test_sorted
# Conversion Sam to Sorted BAM
samtools view -bS file.sam | samtools sort - file_sorted
SAMTOOLS



###############################################################################
# STEP 4. Mark Duplicates
###############################################################################
SEEDFILE="$scriptsFolder/$pipelinesName.p.4.files"
OUTFILES="$scriptsFolder/$pipelinesName.p.4.out.files"
DUPFILES="$scriptsFolder/$pipelinesName.p.4.dup.files"
find $procSorted -name *.sorted.bam > $SEEDFILE
cat $SEEDFILE | sed 's/proc\/sorted/proc\/dup/g' | sed 's/\.sorted\.bam/\.dup\.bam/g' > $OUTFILES
cat $OUTFILES | sed 's/proc\/sorted/proc\/dup/g' | sed 's/\.dup\.bam/\.dup\.txt/g' > $DUPFILES

echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.p.4.o
#$ -e $outputFolder/$pipelinesName.p.4.e
#$ -N $pipelinesName.p.4

module load jdk-sun/1.8.0 picard/2.2.1

SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE)
OUT=\$(awk \"NR==\$SGE_TASK_ID\" $scriptsFolder/$pipelinesName.p.4.out.files)
SAMPLE=\$(awk \"NR==\$SGE_TASK_ID\" $scriptsFolder/$pipelinesName.p.4.dup.files)

echo -e \"
java -jar $PICARD MarkDuplicates \
   I=\${SEED} \
   O=\${OUT} \
   CREATE_INDEX=true \
   M=\${SAMPLE} \
   VALIDATION_STRINGENCY=LENIENT
\"

java -jar $PICARD MarkDuplicates \
   I=\${SEED} \
   O=\${OUT} \
   CREATE_INDEX=true \
   M=\${SAMPLE} \
   VALIDATION_STRINGENCY=LENIENT

module unload jdk-sun/1.8.0 picard/2.2.1
 "  > $scriptsFolder/$pipelinesName.p.4.sh

totalPicardJobs=$(wc -l $SEEDFILE | awk '{print $1}')
jobID=$(qsub -l num_proc=1,s_rt=0:10:00,s_vmem=6G,h_fsize=2G,arch=haswell -t 1-$totalPicardJobs $scriptsFolder/$pipelinesName.p.4.sh | awk '{ print $3}')


for line in $(cat $scriptsFolder/$pipelinesName.p.4.dup.files); do
  tail -3 $line | head -1 | awk '{print $5}'
done
echo "$pipelinesName"".p.4    $jobID" >> $jobsSent
ls -Rl $WD > $filesFolder/$pipelinesName.p.4.files
echo "$pipelinesName"".p.4    $jobID" >> $usageFolder/$pipelinesName.p.4.usage
cat $outputFolder/$pipelinesName.p.4.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.p.4.usage
cat $outputFolder/$pipelinesName.p.4.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.p.4.usage

###############################################################################
# STEP 5. Base quality score recalibration
###############################################################################
