import argparse,datetime,logging,os,sys
from MEOutputFormatter import MEOutputFormatter as mof
from MELoggingFormatter import MELoggingFormatter as mlf
from MELogLevels import MELogLevels as mll
import numpy as np
import random as rnd
from select import select

class SeqCutter:
    def __init__(self,input,output,rlen, mean,std, log=logging.DEBUG):
        self.input=os.path.abspath(input)
        self.output=output
        self.rlen=rlen
        self.mean=mean
        self.std=std
        self.inputBasename=""
        self.log=log
        if (output[-1]=="/"):
			self.output=os.path.abspath(output[0:-1])
        else:
			self.output=os.path.abspath(output)

        self.output="{0}/fragmented".format(self.output)
        print(self.output)
        try:
            os.mkdir(self.output)
        except:
            print("Output folder ({0}) exists. ".format(self.output))

        # Logging
        self.appLogger=logging.getLogger('')
        self.setLogger()


    def checkArgs(self):

        if (os.path.exists(self.input)):
            self.appLogger.log(mll.CONFIG_LEVEL,"Input file exists:\t{0}".format(self.input))
        else:
            self.ending(False, "Input file does not exist.")

        if (os.path.isfile(self.input)):
            self.appLogger.log(mll.CONFIG_LEVEL,"Input is a file.".format(self.input))
        else:
            self.ending(False, "The input path does not belong to a file.\nPlease verify the input.")

        self.inputBasename, extension = os.path.splitext(self.input)
        if (extension in ['.fa','.FA','.fasta','.FASTA'] ):
            self.appLogger.log(mll.CONFIG_LEVEL,"FASTA File".format(self.input))
        else:
            self.ending(False, "The extension of the format is unknown.\nPlease verify the input file.")

        f=open(self.input,"r")
        first=f.readline()
        if (first[0]==">"):
            self.appLogger.info("File with correct format.")
        else:
            self.ending(False, "Input file has wrong format.\nPlease verify the input file.")
        f.close()

        if (self.mean < self.rlen):
            self.ending(False, "Selected fragment length must be greater than the read length.\nPlease verify your parameters.")
        else:
            self.appLogger.info("Generating fragments of {0} +- {1}".format(self.mean, self.std))



    def iterateOverSequences(self):
        self.inputBasename=os.path.basename(self.inputBasename)
        outfile="{0}/{1}.frag.fasta".format(self.output, self.inputBasename)
        f=open(self.input,'r')
        fo=open(outfile, 'a')
        counter=0
        des="";seq=""
        fragmentCounter=0
        for line in f:
            if (counter % 2 ==0): # This is a description
                fragmentCounter=0
                des=line[0:-1]
            else: # Im in a seqline
                seq=line[0:-1]
                new,rest=self.fragment(seq)
                fo.write("{0}_F{1}\n{2}\n".format(des,fragmentCounter,new))
                fragmentCounter=fragmentCounter+1
                while (len(rest)>self.rlen):
                    fo.write("{0}_F{1}\n{2}\n".format(des, fragmentCounter,new))
                    new,rest=self.fragment(rest)
                    fragmentCounter=fragmentCounter+1

                if (len(rest)<=self.rlen):
                    completeRest="N"*(self.rlen-len(rest)+1)
                    fo.write("{0}_F{1}\n{2}{3}\n".format(des,fragmentCounter,rest,completeRest))
            counter=counter+1
        f.close()
        fo.close()

    def fragment(self,seq):
        size=int(np.random.normal(self.mean, self.std,1)[0])
        #  print(size)
        newseq=seq[0:size]
        rest=seq[size+1: len(seq)]
        return newseq,rest

    def setLogger(self):
        loggerFormatter=mlf(fmt="%(asctime)s - %(levelname)s:\t%(message)s",datefmt="%d/%m/%Y %I:%M:%S %p")
        fileHandler=logging.FileHandler(filename="{0}/seqcutter.log".format(self.output))
        fileHandler.setFormatter(loggerFormatter)
        fileHandler.setLevel(level=logging.DEBUG)
        cmdHandler = logging.StreamHandler(sys.stdout)
        cmdHandler.setFormatter(loggerFormatter)
        cmdHandler.setLevel(self.getLogLevel(self.log))
        self.appLogger.addHandler(fileHandler)
        self.appLogger.addHandler(cmdHandler)
        logging.addLevelName(mll.CONFIG_LEVEL, "CONFIG")
        self.appLogger.log(logging.INFO,"Starting")
        self.startTime=datetime.datetime.now()
        self.endTime=None


    def ending(self, good, message):
        # good: Whether there's a good ending or not (error)
        if not good:
            self.appLogger.error(message)
        self.endTime=datetime.datetime.now()
        self.appLogger.info("--------------------------------------------------------------------------------")
        self.appLogger.info("Elapsed time:\t{0}".format(self.endTime-self.startTime))
        self.appLogger.info("Ending at:\t{0}".format(self.endTime.strftime("%a, %b %d %Y. %I:%M:%S %p")))
        sys.exit()

    def log(self, level, message):
        if level==logging.DEBUG:
            self.appLogger.debug(message)
        if level==logging.INFO:
            self.appLogger.info(message)
        if level==logging.WARNING:
            self.appLogger.warning(message)
        if level==logging.ERROR:
            self.appLogger.error(message)
        return None

    def getLogLevel(self,level):
        if level==mll.LOG_LEVEL_CHOICES[0]:
            loggingLevel=logging.DEBUG
        elif level==mll.LOG_LEVEL_CHOICES[1]:
            # show info
            loggingLevel=logging.INFO
        elif level==mll.LOG_LEVEL_CHOICES[2]:
            # show info, warnings
            loggingLevel=logging.WARNING
        else:
            loggingLevel=logging.ERROR
            # every line goes into a file
        return loggingLevel

    def run(self):
        self.checkArgs()
        self.iterateOverSequences()
        self.ending(True,"SeqCutter has finished the fragmentation.")
