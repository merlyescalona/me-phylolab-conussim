import argparse,datetime,logging,os,sys
import SeqCutter as sqc
from MEOutputFormatter import MEOutputFormatter as mof
from MELoggingFormatter import MELoggingFormatter as mlf
from MELogLevels import MELogLevels as mll
import numpy as np
import random as rnd
from select import select
################################################################################
# CONSTANTS
PROGRAM_NAME="SeqCutter"
VERSION=1
MIN_VERSION=0
FIX_VERSION=0
AUTHOR="Merly Escalona <merlyescalona@uvigo.es>"
################################################################################
# python seqcutter.py -p <prefix> -SF <simphy_path> -o outout -m method
################################################################################

def handlingParameters():
	"""
		Handling parameters.
		This functiuon takes care of the cmd-line input.
	"""

	parser = argparse.ArgumentParser(\
	prog="seqcutter.main.py (v.{0}.{1}.{2})".format(VERSION,MIN_VERSION,FIX_VERSION),\
		formatter_class=argparse.RawDescriptionHelpFormatter,\
		description=\
'''
Description:
============
This programs takes a FASTA file and cuts the sequences following a distribution.

Output:
=======

- The output will be a Multiple FASTA file, pero input file with all the fragments
corresponding to the inputted sequence.
		''',\
			epilog="Version {0}.{1}.{2} (Still under development)".format(VERSION,MIN_VERSION,FIX_VERSION),\
			add_help=False
		)
	requiredGroup= parser.add_argument_group('Required arguments')
	requiredGroup.add_argument('-i','--input', metavar="<input_fasta>",type=str,\
		help="Path of the input FASTA file",\
		required=True)
	requiredGroup.add_argument('-m','--mean', metavar="<mean_size>",type=int,\
		help="Mean size of the fragment length.",\
		required=True)
	requiredGroup.add_argument('-s','--std', metavar="<std_dev>",type=int,\
		help="Standard deviation of the distribution.",\
		required=True)
	requiredGroup.add_argument('-r','--rlen', metavar="<read_length>",type=int,\
		help="Desired read length.",\
		required=True)
	requiredGroup.add_argument('-o','--output', metavar="<output_folder>",type=str,\
		help="Name of the folder in which the reference loci will be written. This folder will be a child of the current SimPhy project folder.",\
		required=True)

	optionalGroup= parser.add_argument_group('Optional arguments')
	optionalGroup.add_argument('-l','--log',metavar='<log_level>', type=str,\
		choices=mll.LOG_LEVEL_CHOICES, default="INFO",\
		help='Specified level of log that will be shown through the standard output. Entire log will be stored in a separate file. Values:{0}. Default: {1}. '.format(mll.LOG_LEVEL_CHOICES,mll.LOG_LEVEL_CHOICES[1]))

	informationGroup= parser.add_argument_group('Information arguments')
	informationGroup.add_argument('-v', '--version',\
		action='version',\
		version='{0} {1}.{2}.{3}'.format(PROGRAM_NAME,VERSION,MIN_VERSION,FIX_VERSION),\
		help="Show program's version number and exit")
	informationGroup.add_argument('-h', '--help',\
		action='store_true',\
		help="Show this help message and exit")
	try:
		tmpArgs = parser.parse_args()
	except:
		sys.stdout.write("\n----------------------------------------------------------------------\n")
		parser.print_help()
		sys.exit()
	return tmpArgs

"""
MAIN
"""
if __name__=="__main__":
	try:
		cmdArgs = handlingParameters()
		print(cmdArgs)
		prog = sqc.SeqCutter(cmdArgs.input, cmdArgs.output, cmdArgs.rlen, cmdArgs.mean, cmdArgs.std)
		prog.run()
	except KeyboardInterrupt:
		sys.stdout.write("{0}{1}\nInterrupted!{2}\nPlease run again for the expected outcome.\n".format(mof.BOLD,mof.DARKCYAN, mof.END))
		sys.exit()
