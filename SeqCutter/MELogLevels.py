class MELogLevels:
    """
        Specific class to represent the different log level used.
    """
    LOG_LEVEL_CHOICES=["DEBUG","INFO","WARNING","ERROR"]
    CONFIG_LEVEL=45
