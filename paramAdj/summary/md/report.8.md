# DNA Sequences

`SimPhy` includes a perl script to simulate sequence alignments using `INDELible`. The `INDELible_wrapper.pl` uses a configuration file in which the user can specify different models for different loci (i.e., partitions).The Wrapper has been run with a slight modification of the GTR parameters. For the GTR model in `INDELible`, they are structured as follows:


```
|Order  | CT  | AT | GT | AC  | GC  | AG  |
|Values | 20  |  2 |  4 | 6   | 8   | 16  |
```

- `f1, f2, f3, f4` from Dirichlet `(1,1,1,1) + 0.1` each and rescaled.
- These means “identical” frequencies (around `0.25`). `0.1` so zero is not allowed.

- Rate frequencies from **[@ Darriba et al 2012]** (suppmat; simulations section). Also in **[@Arbiza et al 2011]**

- Template used to generate the INDELible's control file.

```
[TYPE] NUCLEOTIDE 1
[SETTINGS]
    [fastaextension] fasta  

[SIMPHY-UNLINKED-MODEL] csSim_unlinked
    [submodel] GTR $(rd:20,2,4,6,8,16)
    [statefreq] $(d:1,1,1,1)
[rates] 0 $(e:2) 0  
[SIMPHY-PARTITIONS] csSimUnlinked [1 csSim_unlinked 500]

[SIMPHY-EVOLVE] 1 data
```
