# Gene tree height
## Expected values

These should be within the species tree height values, which are from `40-4000` in the `Ne=10.000` case, and `8-800` in the `Ne=50.000`.
