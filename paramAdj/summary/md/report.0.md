# Introduction

This is a follow up report, from the one found in [here](http://merly.escalona.com.es:3838/reports/param_adj_sim/), where we used
`SimPhy` [@Mallo2015] to simulates species trees and their underlying locus and gene trees and gene family evolution, with simulate incomplete lineage sorting (ILS). Later on, using `INDELible` [@Fletcher2009] to evolve nucleotide sequences along the gene trees.

The input for `SimPhy` are the simulation parameter values, which can be fixed or sampled from user-defined statistical distributions. The output consists of sequence alignments and a relational database that facilitate posterior analyses.

Here we present the process followed to understand the configuration of our simulation as well as the results that came up from running it.

# Simulation scenario

- We are simulating 100 replicates (species trees), with 10 gene trees (a single gene tree within each locus tree).
- We are setting species tree heights of the replicates from 200Ky to 20My.
- We vary parameters of speciation rate, substitution rate and heterogeneity following different distributions.
- **Specific variation from the previous report, includes decreasing the effective population size (`Ne`) with `10.000` and `50.000`, as well as the substitution rates, which are going to be used as a `Uniform distribution` between `10e-8` and `10e-9` to decrease the nucleotidic diversity obtained.**


<div class="panel panel-primary">
<div class="panel-heading">
<h3 class="panel-title">
Notes
</h3>
</div>
<div class="panel-body">
As a rule of thumb, you want to use `lognormals` or `exponentials` (less flexible) when you want to explore space across different orders of magnitude (if not, you will get most of the samples in the biggest order of magnitude). Otherwise, `uniforms` or `normals`. Then you have to ask yourself which values you expect in real life and try to tweak the distribution to sample in that range.
</div>
</div>

A general overview of the <strong class="text-danger">modified</strong> parameterization is explained below making a relationship with each of its previous values. Finally, the configuration for the simulation run is shown.
