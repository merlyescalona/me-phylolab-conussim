## Sequence generation

SimPhy includes a perl script to simulate sequence alignments using INDELible. The `INDELible_wrapper.pl` uses a configuration file in which the user can specify different models for different loci (i.e., partitions).The Wrapper has been run with a slight modification of the GTR parameters. For the GTR model in INDELible, they are structured as follows:


```
|Order  | CT  | AT | GT | AC  | GC  | AG  |
|Values | 20  |  2 |  4 | 6   | 8   | 16  |
```

Template used to generate the INDELible's control file.

```
[TYPE] NUCLEOTIDE 1
[SETTINGS]
    [fastaextension] fasta  

[SIMPHY-UNLINKED-MODEL] csSim_unlinked
    [submodel] GTR $(rd:20,2,4,6,8,16)
    [statefreq] $(d:1,1,1,1)
[rates] 0 $(e:2) 0
# This allows for low variability
[SIMPHY-PARTITIONS] csSimUnlinked [1 csSim_unlinked 500]

[SIMPHY-EVOLVE] 1 data
```
## INDELible

INDELible [@Fletcher2009] is a new, portable, and flexible application for biological sequence simulation that combines many features in the same place for the first time. Using a length-dependent model of indel formation it can simulate evolution of multi-partitioned nucleotide, amino-acid, or codon data sets through the processes of insertion, deletion, and substitution in continuous time.

Nucleotide simulations may use the general unrestricted model or the general time reversible model and its derivatives, and amino-acid simulations can be conducted using fifteen different empirical rate matrices. Substitution rate heterogeneity can be modelled via the continuous and discrete gamma distributions, with or without a proportion of invariant sites. INDELible can also simulate under non-homogenous and non-stationary conditions where evolutionary models are permitted to change across a phylogeny.

Unique among indel simulation programs, INDELible offers the ability to simulate using codon models that exhibit nonsynonymous/synonymous rate ratio heterogeneity among sites and/or lineages.

## Diversity statistics

Finally, diversity statistics were run with `diversity` [@Posada2000], a program aimed to describe sequence alignments and generate distance matrices (checking nucleotide differences).
