# Fixed tree height in generation (`F_len_gen`)

This is called fixed, but it is the tree height of each replicate if it has been set or it is sampled from a distribution. If that's not the case this value will be 0. Is a simulation parameter, not a measurement of the generated tree.

## Expected values

Because we set generation time as 1, the results should be between 200Ky (`2e5`) and 20My (`2e7`), which are the observed values.
