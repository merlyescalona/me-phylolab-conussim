################################################################################
# Generic Directories
# Naming Variables
################################################################################
# Needed to run INDELIble_wrapper
################################################################################
# Control file
echo -e "
[TYPE] NUCLEOTIDE 1
[SETTINGS]
    [fastaextension] fasta

[SIMPHY-UNLINKED-MODEL] csSim_unlinked
    [submodel] GTR \$(rd:20,2,4,6,8,16)
    [statefreq] \$(d:1,1,1,1)
[rates] 0 \$(e:2) 0

[SIMPHY-PARTITIONS] csSimUnlinked [1 csSim_unlinked 500]

[SIMPHY-EVOLVE] 1 loc

" > $WD/report/controlFiles/indelible.full.range.txt

<<MUCALCULATION
R
getLNlimPlot<-function(mean,sd){
  probs=seq(0,1,length.out = 1000)
  vals=qlnorm(probs,mean,sd)
  plot(probs,vals)
  c(qlnorm(0.1,mean,sd),qlnorm(0.9,mean,sd))
}
getLNlimPlotQ<-function(mean,sd){
  probs=seq(0,1,length.out = 1000)
  vals=qlnorm(probs,mean,sd)
  q01<-qlnorm(0.1,mean,sd)
  q09<-qlnorm(0.9,mean,sd)
  filter<-intersect(which(vals <=q09),which(vals>=q01))
  plot(probs[filter],vals[filter])
  c(q01,q09)
}
getLNlimNums<-function(mean,sd){
  probs=seq(0,1,length.out = 1000)
  vals=qlnorm(probs,mean,sd)
  c(qlnorm(0.1,mean,sd),qlnorm(0.9,mean,sd))
}
getLNlimPlot(-17,2.49)
[1] 1.702678e-09 1.006596e-06
MUCALCULATION
module load bio/diversity/2.0.0  R/3.2.2_1 bio/indelible/1.03 perl/5.22.1_1
################################################################################
# Fixed parameters
# Specific case to be ran with indelible Ne=10.000
################################################################################
folders=("FRNE2K" "FRNE5K" "FRNE10K" "FRNE50K" "FRNE100K")
multipleSP=("F:2000" "F:5000" "F:10000" "F:50000" "F:100000")
numRuns=5

for run in $(seq 0 $numRuns);do

  pipelinesName=${folders[$run]}
  prefixLoci="data"
  SP=${multipleSP[$run]}
  ######################################################################### SIMPHY
  ST="U:200000,20000000"
  SU="U:0.00000001,0.0000000001" # Mutation rate
  SB="LN:-13.58,1.85" # Speciation rate - depends on SU and SI (species tree height and number of inds. per taxa/tips)
  SL="U:4,20" #Num. taxa
  SI="U:2,20" #numIndTaxa
  # SP="F:10000" # effective population size
  SO="F:1" # outgroupBranch length - This can be modified (LN:0,1)
  SG="F:1" # tree wide generation time
  GP="LN:1.4,1"  # Gene-by-lineage-specific rate heterogeneity modifier (HYPER PARAM)
  HH="LN:1.2,1"
  HG="F:GP" # Gene-by-lineage-specific rate heterogeneity modifier
  $simphy -rs 100 -rl F:10 -su $SU -sb $SB -sl $SL -si $SI -sp $SP -st $ST -so $SO -sg $SG -gp $GP -hh $HH -hg $HG  -v 1 -o $pipelinesName -cs $RANDOM -om 1 -od 1 -op 1 -oc 1 -on 1
  ############################################################## INDELIble_wrapper
  perl $wrapper $WD/$pipelinesName $WD/report/controlFiles/indelible.full.range.txt $RANDOM 1 &> $outputFolder/$pipelinesName.2.txt
  ###################################################################### indelible
  for item in $(find /home/merly/research/parameterization/$pipelinesName/ -maxdepth 1 -type d); do
    echo $item >> temp.txt
  done
  tail -n+2 temp.txt > $filesFolder/$pipelinesName.3.files
  rm temp.txt
  SEEDFILE=$filesFolder/$pipelinesName.3.files

  for item in $(cat $SEEDFILE); do
    echo "cd $item"
    cd $item
    indelible
  done
  ###################################################################### Diversity
  sgeCounter=1
  mkdir -p $WD/report/stats/${pipelinesName}
  for line in $(seq 1 100); do
    st=$(printf "%03g" $line)
    echo "" > $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}.o.stats
    for indexGT in $(seq 1 10); do
      gt=$(printf "%02g" $indexGT)
      echo -e "\r$st/$gt"
      filename="$WD/$pipelinesName/$st/${prefixLoci}_${gt}_TRUE.phy"
      INPUTBASE=$(basename $filename .phy)
      base=$(basename $filename)
      cd $WD/$pipelinesName/$st/
      diversity $base &> $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}_${INPUTBASE}.0.stats
      diversity $base -g &> $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}_${INPUTBASE}.1.stats
      diversity $base -g -m -p 2> $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}_${INPUTBASE}.2.stats 1>> $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}.o.stats
      cat $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}_${INPUTBASE}.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}_${INPUTBASE}.3.stats
      let counter=counter+1
    done
  done
  ############################################################## Parsing diversity
  tmp1="$WD/report/diversity/diversity.parsing.1.tmp"
  tmp2="$WD/report/diversity/diversity.parsing.2.tmp"
  for line in $(seq 1 100); do
    st=$(printf "%03g" $line)
    rm $tmp1
    for indexGT in $(seq 1 10); do
      gt=$(printf "%02g" $indexGT)
      elem=$(cat $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}_${prefixLoci}_${gt}_TRUE.0.stats | grep "mean pairwise distance =" | awk -F"=" '{print $2}' )
      echo ${elem}
      echo "${st},${elem}" >>$tmp1
    done
    cat $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}.o.stats | tail -n+2 | paste -d "," - $tmp1 >> $tmp2
  done
  diversitySummary="$WD/report/diversity/${pipelinesName}.diversity.summary"
  echo "file,numTaxa,numSites,NumVarSites,NumInfSites,numMissSites,numGapSites,numAmbiguousSites,freqA,freqC,freqG,freqT,meanPairwiseDistancePerSite,st,meanPairwiseDistance" > $diversitySummary
  cat $tmp2 >> $diversitySummary
  rm $tmp1 $tmp2
  ########################################################### Structuring the data
  mkdir -p $WD/report/csv/${pipelinesName}
  for line in $(seq 1 100); do
    st=$(printf "%03g" $line)
    for indexGT in $(seq 1 10); do
      gt=$(printf "%02g" $indexGT)
      Rscript --vanilla /home/merly/parseDiversityMatrices.R $WD/report/stats/${pipelinesName} ${pipelinesName}_ST_${st}_${prefixLoci}_${gt} $WD/report/csv/${pipelinesName}
    done
  done
  ####################################################### Structuring ALL the data
  for stIndex in $(seq 1 100); do
    st=$(printf "%03g" ${stIndex})
    for gtIndex in $(seq 1 10); do
      gt=$(printf "%02g" ${gtIndex})
      echo "${st}/${gt}"
      cat "$WD/${pipelinesName}/${st}/g_trees${gt}.trees" >> "$WD/${pipelinesName}/${st}/ALL_${st}.trees"
    done
  done
done
