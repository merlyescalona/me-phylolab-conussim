################################################################################
# Generic Directories
# Naming Variables
################################################################################
# Needed to run INDELIble_wrapper
################################################################################
# Control file
echo -e "
[TYPE] NUCLEOTIDE 1
[SETTINGS]
    [fastaextension] fasta

[SIMPHY-UNLINKED-MODEL] csSim_unlinked
    [submodel] GTR \$(rd:20,2,4,6,8,16)
    [statefreq] \$(d:1,1,1,1)
[rates] 0 \$(e:2) 0

[SIMPHY-PARTITIONS] csSimUnlinked [1 csSim_unlinked 500]

[SIMPHY-EVOLVE] 1 loc

" > $WD/report/controlFiles/indelible.full.range.txt

<<MUCALCULATION
R
getLNlimPlot<-function(mean,sd){
  probs=seq(0,1,length.out = 1000)
  vals=qlnorm(probs,mean,sd)
  plot(probs,vals)
  c(qlnorm(0.1,mean,sd),qlnorm(0.9,mean,sd))
}
getLNlimPlotQ<-function(mean,sd){
  probs=seq(0,1,length.out = 1000)
  vals=qlnorm(probs,mean,sd)
  q01<-qlnorm(0.1,mean,sd)
  q09<-qlnorm(0.9,mean,sd)
  filter<-intersect(which(vals <=q09),which(vals>=q01))
  plot(probs[filter],vals[filter])
  c(q01,q09)
}
getLNlimNums<-function(mean,sd){
  probs=seq(0,1,length.out = 1000)
  vals=qlnorm(probs,mean,sd)
  c(qlnorm(0.1,mean,sd),qlnorm(0.9,mean,sd))
}
getLNlimPlot(-17,2.49)
[1] 1.702678e-09 1.006596e-06
MUCALCULATION
################################################################################
# Fixed parameters
# Specific case to be ran with indelible Ne=10.000
################################################################################
pipelinesName="FRNE10K"
######################################################################### SIMPHY
ST="U:200000,20000000"
SU="U:0.00000001,0.0000000001" # Mutation rate
SB="LN:-13.58,1.85" # Speciation rate - depends on SU and SI (species tree height and number of inds. per taxa/tips)
SL="U:4,20" #Num. taxa
SI="U:2,20" #numIndTaxa
SP="F:10000" # effective population size
SO="F:1" # outgroupBranch length - This can be modified (LN:0,1)
SG="F:1" # tree wide generation time
GP="LN:1.4,1"  # Gene-by-lineage-specific rate heterogeneity modifier (HYPER PARAM)
HH="LN:1.2,1"
HG="F:GP" # Gene-by-lineage-specific rate heterogeneity modifier
command="$simphy -rs 100 -rl F:10 -su $SU -sb $SB -sl $SL -si $SI -sp $SP -st $ST -so $SO -sg $SG -gp $GP -hh $HH -hg $HG  -v 1 -o $pipelinesName -cs $RANDOM -om 1 -od 1 -op 1 -oc 1 -on 1"
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.o
#$ -e $outputFolder/$pipelinesName.e
#$ -N $pipelinesName

cd $PHYLOLAB/$USER/parameterization
$command
" > $scriptsFolder/$pipelinesName.sh
qsubLine="qsub  $scriptsFolder/$pipelinesName.sh"
jobID=$($qsubLine | awk '{ print $3}')
echo -e "$pipelinesName\t$jobID" >> $jobsSent
############################################################## INDELIble_wrapper
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.2.o
#$ -e $outputFolder/$pipelinesName.2.e
#$ -N $pipelinesName.2

cd $PHYLOLAB/$USER/parameterization
perl $wrapper $WD/$pipelinesName $WD/report/controlFiles/indelible.full.range.txt $RANDOM 1 &> $outputFolder/$pipelinesName.2.txt

" >> $scriptsFolder/$pipelinesName.2.sh
qsubLine="qsub -l num_proc=1,s_rt=1:00:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.2.sh"
jobID=$($qsubLine | awk '{ print $3}')
echo -e "$pipelinesName\t$jobID" >> $jobsSent
###################################################################### indelible
for item in $(find /home/merly/research/parameterization/$pipelinesName/ -maxdepth 1 -type d); do
echo $item >> temp.txt
done
tail -n+2 temp.txt > $filesFolder/$pipelinesName.3.files
rm temp.txt
SEEDFILE=$filesFolder/$pipelinesName.3.files
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.3.o
#$ -e $outputFolder/$pipelinesName.3.e
#$ -N $pipelinesName.3

SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE )
echo \$SEED
cd \$SEED
module load bio/indelible/1.03
indelible

" >> $scriptsFolder/$pipelinesName.3.sh
totalJobs=$(wc -l $SEEDFILE | awk '{print $1}')
jobID=$(qsub -l num_proc=1,s_rt=1:00:00,s_vmem=6G,h_fsize=4G,arch=haswell -t 1-$totalJobs $scriptsFolder/$pipelinesName.3.sh | awk '{ print $3}')
jobID=$(qsub -t 1-$totalJobs $scriptsFolder/$pipelinesName.3.sh | awk '{ print $3}')
echo -e "$pipelinesName\t$jobID" >> $jobsSent
###################################################################### diversity
###################################################################### QLOGIN
sgeCounter=1
prefixLoci="data"
module load bio/diversity/2.0.0
for line in $(seq 1 100); do
  st=$(printf "%03g" $line)
  echo "" > $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}.o.stats
  for indexGT in $(seq 1 10); do
    gt=$(printf "%02g" $indexGT)
    echo -e "\r$st/$gt"
    filename="$WD/$pipelinesName/$st/${prefixLoci}_${gt}_TRUE.phy"
    INPUTBASE=$(basename $filename .phy)
    base=$(basename $filename)
    cd $WD/$pipelinesName/$st/
    diversity $base &> $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}_${INPUTBASE}.0.stats
    diversity $base -g &> $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}_${INPUTBASE}.1.stats
    diversity $base -g -m -p 2> $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}_${INPUTBASE}.2.stats 1>> $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}.o.stats
    cat $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}_${INPUTBASE}.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}_${INPUTBASE}.3.stats
    let counter=counter+1
done
done






################################################################################
# Fixed parameters
# Specific case to be ran with indelible Ne=50.000
################################################################################
pipelinesName="FRNE50K"
######################################################################### SIMPHY
ST="U:200000,20000000"
SU="U:0.00000001,0.0000000001" # Mutation rate
SB="LN:-13.58,1.85" # Speciation rate - depends on SU and SI (species tree height and number of inds. per taxa/tips)
SL="U:4,20" #Num. taxa
SI="U:2,20" #numIndTaxa
SP="F:50000" # effective population size
SO="F:1" # outgroupBranch length - This can be modified (LN:0,1)
SG="F:1" # tree wide generation time
GP="LN:1.4,1"  # Gene-by-lineage-specific rate heterogeneity modifier (HYPER PARAM)
HH="LN:1.2,1"
HG="F:GP" # Gene-by-lineage-specific rate heterogeneity modifier
command="$simphy -rs 100 -rl F:10 -su $SU -sb $SB -sl $SL -si $SI -sp $SP -st $ST -so $SO -sg $SG -gp $GP -hh $HH -hg $HG  -v 1 -o $pipelinesName -cs $RANDOM -om 1 -od 1 -op 1 -oc 1 -on 1"
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.o
#$ -e $outputFolder/$pipelinesName.e
#$ -N $pipelinesName

cd $PHYLOLAB/$USER/parameterization
$command
" > $scriptsFolder/$pipelinesName.sh
qsubLine="qsub -l num_proc=1,s_rt=1:00:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.sh"
jobID=$($qsubLine | awk '{ print $3}')
echo -e "$pipelinesName\t$jobID" >> $jobsSent
############################################################## INDELIble_wrapper
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.2.o
#$ -e $outputFolder/$pipelinesName.2.e
#$ -N $pipelinesName.2

cd $PHYLOLAB/$USER/parameterization
perl $wrapper $WD/$pipelinesName $WD/report/controlFiles/indelible.full.range.txt $RANDOM 1 &> $outputFolder/$pipelinesName.2.txt

" >> $scriptsFolder/$pipelinesName.2.sh
qsubLine="qsub -l num_proc=1,s_rt=1:00:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.2.sh"
jobID=$($qsubLine | awk '{ print $3}')
echo -e "$pipelinesName\t$jobID" >> $jobsSent

############################################################## indelible
for item in $(find /home/merly/research/parameterization/$pipelinesName/ -maxdepth 1 -type d); do
echo $item >> temp.txt
done
tail -n+2 temp.txt > $filesFolder/$pipelinesName.3.files
rm temp.txt
SEEDFILE=$filesFolder/$pipelinesName.3.files
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.3.o
#$ -e $outputFolder/$pipelinesName.3.e
#$ -N $pipelinesName.3

SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE )
echo \$SEED
cd \$SEED
module load bio/indelible/1.03
indelible

" >> $scriptsFolder/$pipelinesName.3.sh
totalJobs=$(wc -l $SEEDFILE | awk '{print $1}')
jobID=$(qsub -l num_proc=1,s_rt=1:00:00,s_vmem=6G,h_fsize=4G,arch=haswell -t 1-$totalJobs $scriptsFolder/$pipelinesName.3.sh | awk '{ print $3}')
jobID=$(qsub -t 1-$totalJobs $scriptsFolder/$pipelinesName.3.sh | awk '{ print $3}')
###################################################################### diversity
###################################################################### QLOGIN
sgeCounter=1
prefixLoci="data"
module load bio/diversity/2.0.0
for line in $(seq 1 100); do
  st=$(printf "%03g" $line)
  echo "" > $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}.o.stats
  for indexGT in $(seq 1 10); do
    gt=$(printf "%02g" $indexGT)
    echo -e "\r$st/$gt"
    filename="$WD/$pipelinesName/$st/${prefixLoci}_${gt}_TRUE.phy"
    INPUTBASE=$(basename $filename .phy)
    base=$(basename $filename)
    cd $WD/$pipelinesName/$st/
    diversity $base &> $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}_${INPUTBASE}.0.stats
    diversity $base -g &> $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}_${INPUTBASE}.1.stats
    diversity $base -g -m -p 2> $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}_${INPUTBASE}.2.stats 1>> $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}.o.stats
    cat $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}_${INPUTBASE}.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}_${INPUTBASE}.3.stats
    let counter=counter+1
done
done









################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
#
################################################################################
# Parsing diversity
################################################################################
# Parsing both distance matrices file and output for summary
# Output file
tmp1="$WD/report/diversity/diversity.parsing.1.tmp"
tmp2="$WD/report/diversity/diversity.parsing.2.tmp"
prefixLoci="data"

for line in $(seq 1 100); do
  st=$(printf "%03g" $line)
  rm $tmp1
  for indexGT in $(seq 1 10); do
    gt=$(printf "%02g" $indexGT)
    elem=$(cat $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}_${prefixLoci}_${gt}_TRUE.0.stats | grep "mean pairwise distance =" | awk -F"=" '{print $2}' )
    echo ${elem}
    echo "${st},${elem}" >>$tmp1
  done
  cat $WD/report/stats/${pipelinesName}/${pipelinesName}_ST_${st}.o.stats | tail -n+2 | paste -d "," - $tmp1 >> $tmp2
done
diversitySummary="$WD/report/diversity/${pipelinesName}.diversity.summary"
echo "file,numTaxa,numSites,NumVarSites,NumInfSites,numMissSites,numGapSites,numAmbiguousSites,freqA,freqC,freqG,freqT,meanPairwiseDistancePerSite,st,meanPairwiseDistance" > $diversitySummary
cat $tmp2 >> $diversitySummary
rm $tmp1 $tmp2

# Distance matrices
# QLOGIN

WD="~/git/me-phylolab-conusSim/paramAdj/v2/data"
prefixLoci="data"
mkdir $outFolder
for line in $(seq 1 100); do
  st=$(printf "%03g" $line)
  for indexGT in $(seq 1 10); do
    gt=$(printf "%02g" $indexGT)
    Rscript --vanilla /home/merly/parseDiversityMatrices.R /home/merly/research/parameterization/report/stats/FRNE50K FRNE50K_ST_${st}_${prefixLoci}_${gt} /home/merly/research/parameterization/report/csv/FRNE50K
done
done
