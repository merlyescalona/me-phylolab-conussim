# Nucleotide diversity check
#

labelsDiversity=c("Ne","ST","File","# Taxa","# Sites","VarSites","InfSites","MissSites","GapSites","AmbSites","%A","%C","%G","%T","MPWDPS","MPWD")
diversitySummaryN2K<-read.csv("data/report/diversity/FRNE2K.diversity.summary")
diversitySummaryN5K<-read.csv("data/report/diversity/FRNE5K.diversity.summary")
diversitySummaryN10K<-read.csv("data/report/diversity/FRNE10K.diversity.summary")
diversitySummaryN50K<-read.csv("data/report/diversity/FRNE50K.diversity.summary")
diversitySummaryN100K<-read.csv("data/report/diversity/FRNE100K.diversity.summary")

diversitySummaryN2K$Ne=rep(2000,nrow(diversitySummaryN2K))
diversitySummaryN5K$Ne=rep(5000,nrow(diversitySummaryN5K))
diversitySummaryN10K$Ne=rep(10000,nrow(diversitySummaryN10K))
diversitySummaryN50K$Ne=rep(50000,nrow(diversitySummaryN50K))
diversitySummaryN100K$Ne=rep(100000,nrow(diversitySummaryN100K))
diversitySummaryN2K<-diversitySummaryN2K[c(ncol(diversitySummaryN2K),14,1:13,15:(ncol(diversitySummaryN2K)-1))]
diversitySummaryN5K<-diversitySummaryN5K[c(ncol(diversitySummaryN5K),14,1:13,15:(ncol(diversitySummaryN5K)-1))]
diversitySummaryN10K<-diversitySummaryN10K[c(ncol(diversitySummaryN10K),14,1:13,15:(ncol(diversitySummaryN10K)-1))]
diversitySummaryN50K<-diversitySummaryN50K[c(ncol(diversitySummaryN50K),14,1:13,15:(ncol(diversitySummaryN50K)-1))]
diversitySummaryN100K<-diversitySummaryN100K[c(ncol(diversitySummaryN100K),14,1:13,15:(ncol(diversitySummaryN100K)-1))]
colnames(diversitySummaryN2K)<-labelsDiversity
colnames(diversitySummaryN5K)<-labelsDiversity
colnames(diversitySummaryN10K)<-labelsDiversity
colnames(diversitySummaryN50K)<-labelsDiversity
colnames(diversitySummaryN100K)<-labelsDiversity

diversity=data.frame(rbind(
  diversitySummaryN2K,
  diversitySummaryN5K,centro
  diversitySummaryN10K,
  diversitySummaryN50K,
  diversitySummaryN100K
  ))

qnN2K=quantile(subset(diversity,diversity$Ne==2000)$MPWDPS, probs=c(.25,.75))
qnN5K=quantile(subset(diversity,diversity$Ne==5000)$MPWDPS, probs=c(.25,.75))
qnN10K=quantile(subset(diversity,diversity$Ne==10000)$MPWDPS, probs=c(.25,.75))
qnN50K=quantile(subset(diversity,diversity$Ne==50000)$MPWDPS, probs=c(.25,.75))
qnN100K=quantile(subset(diversity,diversity$Ne==100000)$MPWDPS, probs=c(.25,.75))

meansDataset=data.frame(
  meansV=c(
    mean(subset(diversity,diversity$Ne==2000)$MPWDPS),
    mean(subset(diversity,diversity$Ne==5000)$MPWDPS),
    mean(subset(diversity,diversity$Ne==10000)$MPWDPS),
    mean(subset(diversity,diversity$Ne==50000)$MPWDPS),
    mean(subset(diversity,diversity$Ne==100000)$MPWDPS)
  ),
  qnLow=c(qnN2K[1],qnN5K[1],qnN10K[1],qnN50K[1],qnN100K[1]),
  qnHigh=c(qnN2K[2],qnN5K[2],qnN10K[2],qnN50K[2],qnN100K[2]),
  Ne=c(2000,5000,10000,50000,100000)
)


ggplot(meansDataset,aes(x=Ne))+theme_bw()+
  theme(panel.grid.minor = element_line(colour = "grey80",linetype="dotted"))+
  geom_point(aes(x=Ne,y=meansV))+
  geom_line(aes(x=Ne,y=meansV))+
  geom_line(aes(x=Ne,y=qnLow),color="red", linetype="dotted", size=.4)+
  geom_line(aes(x=Ne,y=qnHigh),color="red", linetype="dotted", size=.4)+
  xlab("Effective population size (Ne)")+ylab("Mean pi")+
  ggtitle("Mean pairwise distance per effective population size run")


N2K=subset(wholedataset,Ne==2000)
N5K=subset(wholedataset,Ne==5000)
N10K=subset(wholedataset,Ne==10000)
N50K=subset(wholedataset,Ne==50000)
N100K=subset(wholedataset,Ne==100000)

ggplot(wholedataset,aes(x=ST,y=MPWDPS, color=VarSites))+
  geom_point()+theme_bw()+
  theme(panel.grid.minor = element_line(colour = "grey80",linetype="dotted"))+
  ylim(0,0.2)+facet_grid(~Ne)+
  ylab("pi")+xlab("Species tree index")+
  ggtitle("Distribution of the pairwise distance per species tree replicate grouped by effective population size")
