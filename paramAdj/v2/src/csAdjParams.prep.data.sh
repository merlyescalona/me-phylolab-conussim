folders=("FRNE10K" "FRNE50K")

for folder in ${folders[@]};do
  for stIndex in $(seq 1 100); do
    st=$(printf "%03g" ${stIndex})
    for gtIndex in $(seq 1 10); do
      gt=$(printf "%02g" ${gtIndex})
      echo "${st}/${gt}"
      cat "data/${folder}/${st}/g_trees${gt}.trees" >> "data/${folder}/${st}/ALL_${st}.trees"
    done
  done
done
