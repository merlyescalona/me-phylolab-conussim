
**Below, there is an extensive explanation on how parameters where chosen and how do the distribution from where values are picked look like.**

### Speciation Rate [`SB`]

The Species tree topology follows a Yule process (there is no extinction, we assume this in case of shallow divergences). We then only  have to sample a birth rate -one that generates the intended number of tips.

Birth rate chosen is a `~ LogNormal (-13.58,1.85)`.

Mean and standard deviation of the `LogNormal` distribution is set by eye, looking for values within the distribution that contain the estimated values for the specific tree height ranges.


#### Log Normal parameterization

We can calculate the expected height of a tree modeled by a Yule process with the formula:

```
E(height)= ln(n_leaves)/lambda
E(h) = expected height of a tree moduled by a Yule process
Lambda = speciation rate
```

We want to make sure that we generate trees from 200Ky to 20My, but we are also sampling the number of leaves. We can use the mean as a reference. Then, we can calculate the rate needed to get the extremes of the desired range.

```
# We’ll take the mean n_leaves (12; mean of 4-20 species) and get the lambdas for 200k and 20M generations.
ln(12)/200000=rate, rate=1.24E-5
ln(12)/20000000=rate, rate=1.24E-7
```

Afterwards, we need to look for a prior distribution that comprehends the desired region. The `lognormal` is in principle a good one since allow us to easily sample across different orders of magnitude. And now we want to search for a mean and standard deviation (LN) that encompass these values.
