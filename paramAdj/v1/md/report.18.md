# Extra lineages

Number of extra lineages per gene trees across species tree replicate. This information is a  direct measure of the incomplete lineage sorting (ILS).

The value of the extra lineages is strongly correlated with the Robinson-Foulds (RF) distance, which is used more often.

Reference values can be checked out on:

Siavash Mirarab and Tandy Warnow, in their mammals and birds datasets. Is possible that they have obtain some values from the empiricals, they for sure have information of gene tree lengths,

> For comparison, mean gene tree length on avian is 0.25 for exons and 0.75 introns; on mammals is .69, and  for 1kp (plants going back 700M years) is 10.7


## Expected values

- We are interested in having extra lineages, but not a high number, and lower than what we got on the previous simulation scenario.

- We observed that the distribution of values is skewed to the lower values.

- There's a high frequency of trees with `0` extra lineages:

    - `Ne=10.000`: `~50%`.
    - `Ne=50.000`: `~20%`.

- Case `Ne=50.000` looks better for the values we were expecting (low number of extra lineages).
