
### Outgroup [`SO`]

If using `SO=F:1`, means we are simulating an outgroup and the distance of the outgroup to the root is twice the ingroup height.
