# Species tree height/length

The coalescent units calculation should be this for diploids:

```
Ne=100000
cu=1/(2*Ne)
```

But, `SimPhy` simulates haploid genomes, therefore, the calculations are:

```
1cu=1/Ne=1e-05
```

Which, make us expect:


```
cu=1/Ne=1/100000=1e-05
cu200K=cu*200000=2
cu20M=cu*200000=200
```

Then, the outgroup is added a posteriori, which in our case the value is set as `1`.
Taken into account that, the length of the root ingroup to the root, is the same as the
ingroup tree, the quantities multiply by 2, hence the values between `4`-`400`cu.


Also, species tree lengths should be larger than the species tree heights, according to the values obtained, this seems consistent.
