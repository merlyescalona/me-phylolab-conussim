# Tree-wide substitution rate

## Expected values

Sampled from a `LogNormal` distribution (`LN:-17,2.49`),

- Maximum: `10e-7`
- Minimum: `10e-9`


These values are approximate, we are actually looking for values within the quantiles 0.1 and 0.9 of the distribution.

## Observation

Values of the distribution are biased towards the lower limit of the expected values. If values were classified within `10e-7`,`10e-8` and `10e-9` distribution should be more uniform.



**Note:** It seems that this is not reflected in the variability of the sequences (check Replicates tab).
