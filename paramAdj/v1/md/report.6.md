### Relationship between parameters and what it involves

| Parameter | Modification | SimPhy Table | SimPhy Table Field |
|-----------|--------------|--------------|--------------------|
| GP        | Genome-wide modifications | `Species_tree` | |
| HS        | Species trere branches    | `Species_tree` | |
| HL        | Locus tree (among LT)     |                | |
| HH        | Locus tree branches       |                | |
| HG        | Gene-tree branches        |                | |


In our case, we are using 1 gene tree per Locus trere, so we will only modify one of the parameters HH or HG.


### Our simulations

The values we are finally using:

```
GP=LN:1.4,1
HG=F:GP
```

Which means that we are introducing heterogeneity in the gene trees (across branches; `HG`), but doing  “at genome level” (`GP`), which means, different replicates (ST) will have different amounts of heterogeneity, but they will be the same for all loci of that replicate (`HG:F, GP`). This has the advantage that we can later look at the effect of different levels of heterogeneity if we want (sorting by replicate).
