# References
- [Posada 2000] David Posada. ***Diversity: a program aimed to describe sequence alignments***
- [Mallo et al 2015]: Mallo D, de Oliveira Martins L, Posada D (2015) ***SimPhy: Phylogenomic Simulation of Gene, Locus and Species Trees.*** Syst. Biol. doi: http://dx.doi.org/10.1093/sysbio/syv082
- [Fletcher 2009]: Fletcher W and Yang Z (2009) ***INDELible: a flexible simulator of biological sequence evolution.*** Mol. Biol. and Evol. 26(8): 1879-88
- [Huang 2011]: Weichun Huang, Leping Li, Jason R Myers, and Gabor T Marth. (2012) ***ART: a next-generation sequencing read simulator.*** Bioinformatics  28 (4): 593-594
- [Arbiza et al 2011]: Leonardo Arbiza. Mateus Patricio, Hernán Dopazo and David Posada (2011) ***Genome-Wide Heterogeneity of Nucleotide Substitution Model Fit.*** Genome Biol Evol  3 896-908. doi: 10.1093/gbe/evr080
- [Darriba et al 2012]: Diego Darriba, Guillermo L Taboada, Ramón Doallo and David Posada (2012) ***jModelTest 2: more models, new heuristics and parallel computing.*** Nature Methods 9, 772  doi:10.1038/nmeth.2109
