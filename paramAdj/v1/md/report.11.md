# Species tree leaves

## Expected values

As introduced in the configuration, we were expecting a `Uniform` distribution, between `4`, `20`.

The results, since we are asking for an outgroup, will get the minimum and maximum values plus one. Therefore, the distribution of the data is considered correct.
