# Introduction
We are using `SimPhy` [@Mallo2015] for our simulations.  `SimPhy` simulates species trees and their underlying locus and gene trees and gene family evolution, ebing able to simulate incomplete lineage sorting (ILS), gene duplication and loss (GDL), horizontal gene transfer (HGT) and gene conversion (GC). It uses `INDELible` [@Fletcher2009] to evolve nucleotide/codon/aminoacid sequences along the gene trees.

The input for `SimPhy` are the simulation parameter values, which can be fixed or sampled from user-defined statistical distributions. The output consists of sequence alignments and a relational database that facilitate posterior analyses.

Here we present the process followed to understand the configuration of our simulation as well as the results that came up from running it.

# Simulation scenario

We are simulating 100 replicates (species trees), with 10 locus tree and a single gene tree within each locus tree.
In this case, locus tree are topologically equivalent to the species tree. We are setting the heights of the replicates from 200Ky to 20My.
Furthermore, we vary parameters of speciation rate, substitution rate and heterogeneity following
different distributions.

A general overview of the parameterization is explained below making a relation ship with each of the parameters and the final values selected for the simulation.


## Notes
As a rule of thumb, you want to use `lognormals` or `exponentials` (less flexible) when you want to explore space across different orders of magnitude (if not, you will get most of the samples in the biggest order of magnitude). Otherwise, `uniforms` or `normals`. Then you have to ask yourself which values you expect in real life and try to tweak the distribution to sample in that range.
