
### Tree-wide substitution rate [`SU`]

Substitution rate for some distributions will be:
```
subs.rate=1/SU
```

While for others will be the specific value. For instance, for a specific `subs.rate=10e-7`, we would have to use `SU=F:0.0000001`.  If we want a bit of variation around 10e-7, rate is sampled from an exponential distribution that has to be set around the value (mean) we want: 10e-7, being the rate:  

`Mean subs.rate = 1/EXP(10e7)`

In our case we looked for a distribution (LN) that comprised from `10e-7` to `10e-9` subs.rate values as it seems to approach better the biological range. One reason not to do this and use a single value (sample or even fix it) would be that for more number of variables, more replicates are needed to study them and higher is the probability of generating non-useful replicates (where values approach the limits on the distributions, with low likelihood values). Yet, we think the one we defined is a reasonable range.
