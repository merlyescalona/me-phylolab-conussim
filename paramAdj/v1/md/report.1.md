### General information 
- When there is no duploss and the goal is 1GT per locus is better to set `-RL`.
- It is better to simulate several locus trees (ex: `-rl 200`) and then simulate a single gene tree along each of them (`-rg 1`). In this case (like ours) the locus tree is topologically equivalent to the species tree, but branch lengths have different units and may be scaled up or down to generate heterogeneity.
- `Species tree == locus tree != gene tree`: Duplications and losses directly modify the locus tree, while incomplete lineage sorting (ILS) modifies the gene tree.
