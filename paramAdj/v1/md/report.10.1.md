# Number of individuals per taxa

# Expected values

As introduced in the configuration, we were expecting a `Uniform` distribution, between `2`, `20`.

Not exactly what's observed, we can see a high rate of value `2`, which can be explained with the low number of replicates made. Rest of the values seem to be uniformly distributed.
