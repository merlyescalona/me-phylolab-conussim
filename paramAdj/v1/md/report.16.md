# Length of the branch from the ingroup linking to the species tree root

## Expected values

In our case it can vary between `~200K` - `~20My` because in the case of trees with very shallow ingroups the distance IngroupRoot to Root is almost equal to the tree height. It is correct, due to the fact that we are actually setting the heights of the ingroup tree, and so it corresponds to such values.
