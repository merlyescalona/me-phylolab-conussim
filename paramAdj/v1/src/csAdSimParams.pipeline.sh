<################################################################################
# Generic Directories
# Naming Variables
################################################################################
# pav: ParameterAdjustmentValue
timingNames=(A B C D E F G H I J K L M)
timingCases=("F:20000" "F:25000" "F:200000" "F:2000000" "F:2500000" "F:20000000" "U:20000,2500000" "U:25000,2500000" "U:200000,2500000" "U:20000,20000000" "U:25000,20000000" "U:200000,20000000" "U:2500000,20000000" )
<<TIMINGCASES
a - "F:20000"
b -"F:25000"
c -"F:200000"
d -"F:2000000"
e -"F:2500000"
f -"F:20000000"
g -"U:20000,2500000"
h -"U:25000,2500000"
i -"U:200000,2500000"
j -"U:20000,20000000"
k -"U:25000,20000000"
l -"U:200000,20000000"
m -"U:2500000,20000000"
TIMINGCASES
<<MUCALCULATION
R
getLNlimPlot<-function(mean,sd){
  probs=seq(0,1,length.out = 1000)
  vals=qlnorm(probs,mean,sd)
  plot(probs,vals)
  c(qlnorm(0.1,mean,sd),qlnorm(0.9,mean,sd))
}
getLNlimPlot(-17,2.49)
[1] 1.702678e-09 1.006596e-06
MUCALCULATION
################################################################################
# Fixed parameters
################################################################################
RS="1000"
RL="F:1000"
SU="LN:-17,2.49" # Mutation rate
SB="LN:-13.58,1.85" # Speciation rate - depends on SU and SI (species tree height and number of inds. per taxa/tips)
SL="U:4,20" #Num. taxa
SI="U:2,20" #numIndTaxa
SP="F:100000" # effective population size - SP - Arbiza paper
SO="F:1" # outgroupBranch length - This can be modified (LN:0,1)
SG="F:1" # tree wide generation time
GP="LN:1.4,1"  # Gene-by-lineage-specific rate heterogeneity modifier (HYPER PARAM)
HH="LN:1.2,1" # Gene­-by-­lineage­-specific locus tree parameter
HG="F:GP" # Gene-by-lineage-specific rate heterogeneity modifier

echo "Step    JobID" > $jobsSent
let maxIterations=${#timingCases[@]}-1
for item in $(seq 0 $maxIterations); do

  ST=${timingCases[$item]}
  pipelinesName="pav${timingNames[$item]}"
  command="$simphy -rs $RS -rl $RL -su $SU -sb $SB -sl $SL -si $SI -sp $SP -st $ST -so $SO -sg $SG -gp $GP -hl $HH -hg $HG  -v 1 -o $pipelinesName -cs $RANDOM -om 1 -od 1 -op 1 -oc 1 -on 1"
  echo $command

echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.o
#$ -e $outputFolder/$pipelinesName.e
#$ -N $pipelinesName

cd $PHYLOLAB/$USER/parameterization
$command
" > $scriptsFolder/$pipelinesName.sh
  qsubLine="qsub -l num_proc=1,s_rt=20:00:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.sh"
  jobID=$($qsubLine | awk '{ print $3}')
  echo -e "$pipelinesName\t$jobID" >> $jobsSent
done

# Things are running on cesga-svg
let maxIterations=${#timingCases[@]}-1
for item in $(seq 0 $maxIterations); do
pipelinesName="pav${timingNames[$item]}"
# ls -Rl $WD > $filesFolder/$pipelinesName.files
echo -e "$pipelinesName\t$jobID" >> $usageFolder/$pipelinesName.usage
cat $outputFolder/$pipelinesName.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.usage
cat $outputFolder/$pipelinesName.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.usage
done


################################################################################
# Specific case to be ran with indelible
################################################################################

ST="U:200000,20000000"
SU="LN:-17,2.49" # Mutation rate
SB="LN:-13.58,1.85" # Speciation rate - depends on SU and SI (species tree height and number of inds. per taxa/tips)
SL="U:4,20" #Num. taxa
SI="U:2,20" #numIndTaxa
SP="F:100000" # effective population size - SP - Arbiza paper
SO="F:1" # outgroupBranch length - This can be modified (LN:0,1)
SG="F:1" # tree wide generation time
GP="LN:1.4,1"  # Gene-by-lineage-specific rate heterogeneity modifier (HYPER PARAM)
HH="LN:1.2,1"
HG="F:GP" # Gene-by-lineage-specific rate heterogeneity modifier

pipelinesName="fullRange"
command="$simphy -rs 100 -rl F:10 -su $SU -sb $SB -sl $SL -si $SI -sp $SP -st $ST -so $SO -sg $SG -gp $GP -hh $HH -hg $HG  -v 1 -o $pipelinesName -cs $RANDOM -om 1 -od 1 -op 1 -oc 1 -on 1"
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.o
#$ -e $outputFolder/$pipelinesName.e
#$ -N $pipelinesName

cd $PHYLOLAB/$USER/parameterization
$command
" > $scriptsFolder/$pipelinesName.sh
qsubLine="qsub -l num_proc=1,s_rt=1:00:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.sh"
jobID=$($qsubLine | awk '{ print $3}')
echo -e "$pipelinesName\t$jobID" >> $jobsSent
echo -e "$pipelinesName\t$jobID" >> $usageFolder/$pipelinesName.usage
cat $outputFolder/$pipelinesName.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.usage
cat $outputFolder/$pipelinesName.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.usage

################################################################################
# Need to run INDELIble_wrapper
################################################################################
# Control file

echo -e "
[TYPE] NUCLEOTIDE 1
[SETTINGS]
    [fastaextension] fasta

[SIMPHY-UNLINKED-MODEL] csSim_unlinked
    [submodel] GTR \$(rd:20,2,4,6,8,16)
    [statefreq] \$(d:1,1,1,1)
[rates] 0 \$(e:2) 0

[SIMPHY-PARTITIONS] csSimUnlinked [1 csSim_unlinked 500]

[SIMPHY-EVOLVE] 1 loc

" > $WD/report/controlFiles/indelible.full.range.txt

pipelinesName="fullRange"
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.2.o
#$ -e $outputFolder/$pipelinesName.2.e
#$ -N $pipelinesName.2

cd $PHYLOLAB/$USER/parameterization
perl $wrapper $WD/$pipelinesName $WD/report/controlFiles/indelible.full.range.txt $RANDOM 1 &> $outputFolder/$pipelinesName.2.txt

" >> $scriptsFolder/$pipelinesName.2.sh
qsubLine="qsub -l num_proc=1,s_rt=1:00:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.2.sh"
jobID=$($qsubLine | awk '{ print $3}')
echo -e "$pipelinesName\t$jobID" >> $jobsSent

echo -e "$pipelinesName\t$jobID" >> $usageFolder/$pipelinesName.2.usage
cat $outputFolder/$pipelinesName.2.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.2.usage
cat $outputFolder/$pipelinesName.2.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.2.usage

################################################################################
# Running indelible
################################################################################
for item in $(find /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/ -maxdepth 1 -type d); do
echo $item >> temp.txt
done
tail -n+2 temp.txt > $filesFolder/$pipelinesName.3.files
rm temp.txt
SEEDFILE=$filesFolder/$pipelinesName.3.files


pipelinesName="fullRange"
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.3.o
#$ -e $outputFolder/$pipelinesName.3.e
#$ -N $pipelinesName.3

SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE )
echo \$SEED
cd \$SEED
$indelible

" >> $scriptsFolder/$pipelinesName.3.sh
totalJobs=$(wc -l $SEEDFILE | awk '{print $1}')
jobID=$(qsub -l num_proc=1,s_rt=1:00:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-$totalJobs $scriptsFolder/$pipelinesName.3.sh | awk '{ print $3}')


################################################################################
# STEP 4. Diversity Statistics
################################################################################
# Creating an index file
sgeCounter=1
prefixLoci="data"
pipelinesName="fullRange"
echo -e "#! /bin/bash
#$ -o $outputFolder/$pipelinesName.4.o
#$ -e $outputFolder/$pipelinesName.4.e
#$ -N $pipelinesName.4
" > $scriptsFolder/$pipelinesName.4.sh

for line in $(seq 1 100); do
  st=$(printf "%03g" $line)
  for indexGT in $(seq 1 10); do
    gt=$(printf "%02g" $indexGT)
    echo -e "\r$st/$gt"
    filename="$WD/$pipelinesName/$st/${prefixLoci}_${gt}_TRUE.phy"
    INPUTBASE=$(basename $filename .phy)
    base=$(basename $filename)
    echo "cd $WD/$pipelinesName/$st/" >> $scriptsFolder/$pipelinesName.4.sh
    echo "diversity $base &> $WD/report/stats/ST_${st}_${INPUTBASE}.0.stats" >> $scriptsFolder/$pipelinesName.4.sh
    echo "diversity $base -g &> $WD/report/stats/ST_${st}_${INPUTBASE}.1.stats" >> $scriptsFolder/$pipelinesName.4.sh
    echo "diversity $base -g -m -p 2> $WD/report/stats/ST_${st}_${INPUTBASE}.2.stats" >>  $scriptsFolder/$pipelinesName.4.sh
    echo "cat $WD/report/stats/ST_${st}_${INPUTBASE}.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > $WD/report/stats/ST_${st}_${INPUTBASE}.3.stats" >> $scriptsFolder/$pipelinesName.4.sh
    let counter=counter+1
done
done

jobID=$(qsub -l num_proc=1,s_rt=0:30:00,s_vmem=2G,h_fsize=1G,arch=haswell $scriptsFolder/$pipelinesName.4.sh | awk '{ print $3}')
echo "$pipelinesName"".4    $jobID" >> $jobsSent

echo "$pipelinesName"".4    $jobID" >> $usageFolder/$pipelinesName.4.usage
cat $outputFolder/$pipelinesName.4.o | grep "El consumo de memoria ha sido de" > $usageFolder/$pipelinesName.4.usage
cat $outputFolder/$pipelinesName.4.o | grep "El tiempo de ejecucion ha sido de (segundos)" >> $usageFolder/$pipelinesName.4.usage




################################################################################
# Parsing diversity
################################################################################
# Parsing both distance matrices file and output for summary
# Output file
tmp1="$WD/report/diversity/diversity.parsing.1.tmp"
tmp2="$WD/report/diversity/diversity.parsing.2.tmp"
prefixLoci="data"
for line in $(seq 1 100); do
  st=$(printf "%03g" $line)
  for indexGT in $(seq 1 10); do
    gt=$(printf "%02g" $indexGT)
    elem=$(cat $WD/report/stats/ST_${st}_${prefixLoci}_${gt}_TRUE.0.stats | grep "mean pairwise distance =" | awk -F"=" '{print $2}' )
    echo $elem
    echo "$st,$elem" >>$tmp1
  done
done
cat $outputFolder/$pipelinesName.4.o | grep ^data| paste -d "," - $tmp1 > $tmp2
diversitySummary="$WD/report/diversity/${pipelinesName}.diversity.summary"
echo "file,numTaxa,numSites,NumVarSites,NumInfSites,numMissSites,numGapSites,numAmbiguousSites,freqA,freqC,freqG,freqT,meanPairwiseDistancePerSite,st,meanPairwiseDistance" > $diversitySummary
cat $tmp2 >> $diversitySummary
rm $tmp1 $tmp2

# Distance matrices
# QLOGIN
outFolder="$WD/report/csv"
inputFolder="$WD/report/stats"
prefixLoci="data"
mkdir $outFolder
for line in $(seq 1 100); do
  st=$(printf "%03g" $line)
  for indexGT in $(seq 1 10); do
    gt=$(printf "%02g" $indexGT)
    Rscript --vanilla /home/uvi/be/mef/src/me-phylolab-conussim/diversityMatrices/parseDiversityMatrices.R /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats ST_${st}_${prefixLoci}_${gt} /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/csv
done
done
