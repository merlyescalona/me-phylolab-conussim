#! /bin/bash
#$ -o /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/output/fullRange.4.o
#$ -e /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/output/fullRange.4.e
#$ -N fullRange.4

cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/001/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/001/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/001/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/001/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/001/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/001/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/001/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/001/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/001/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/001/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_001_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/002/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/002/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/002/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/002/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/002/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/002/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/002/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/002/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/002/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/002/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_002_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/003/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/003/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/003/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/003/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/003/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/003/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/003/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/003/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/003/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/003/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_003_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/004/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/004/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/004/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/004/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/004/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/004/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/004/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/004/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/004/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/004/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_004_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/005/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/005/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/005/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/005/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/005/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/005/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/005/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/005/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/005/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/005/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_005_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/006/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/006/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/006/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/006/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/006/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/006/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/006/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/006/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/006/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/006/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_006_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/007/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/007/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/007/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/007/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/007/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/007/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/007/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/007/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/007/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/007/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_007_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/008/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/008/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/008/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/008/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/008/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/008/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/008/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/008/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/008/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/008/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_008_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/009/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/009/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/009/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/009/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/009/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/009/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/009/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/009/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/009/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/009/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_009_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/010/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/010/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/010/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/010/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/010/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/010/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/010/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/010/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/010/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/010/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_010_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/011/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/011/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/011/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/011/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/011/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/011/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/011/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/011/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/011/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/011/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_011_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/012/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/012/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/012/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/012/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/012/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/012/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/012/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/012/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/012/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/012/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_012_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/013/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/013/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/013/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/013/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/013/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/013/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/013/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/013/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/013/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/013/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_013_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/014/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/014/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/014/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/014/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/014/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/014/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/014/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/014/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/014/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/014/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_014_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/015/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/015/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/015/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/015/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/015/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/015/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/015/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/015/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/015/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/015/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_015_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/016/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/016/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/016/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/016/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/016/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/016/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/016/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/016/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/016/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/016/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_016_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/017/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/017/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/017/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/017/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/017/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/017/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/017/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/017/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/017/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/017/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_017_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/018/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/018/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/018/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/018/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/018/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/018/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/018/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/018/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/018/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/018/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_018_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/019/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/019/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/019/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/019/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/019/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/019/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/019/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/019/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/019/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/019/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_019_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/020/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/020/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/020/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/020/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/020/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/020/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/020/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/020/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/020/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/020/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_020_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/021/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/021/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/021/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/021/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/021/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/021/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/021/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/021/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/021/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/021/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_021_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/022/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/022/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/022/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/022/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/022/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/022/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/022/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/022/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/022/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/022/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_022_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/023/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/023/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/023/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/023/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/023/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/023/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/023/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/023/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/023/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/023/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_023_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/024/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/024/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/024/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/024/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/024/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/024/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/024/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/024/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/024/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/024/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_024_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/025/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/025/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/025/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/025/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/025/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/025/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/025/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/025/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/025/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/025/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_025_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/026/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/026/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/026/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/026/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/026/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/026/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/026/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/026/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/026/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/026/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_026_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/027/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/027/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/027/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/027/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/027/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/027/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/027/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/027/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/027/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/027/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_027_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/028/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/028/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/028/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/028/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/028/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/028/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/028/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/028/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/028/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/028/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_028_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/029/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/029/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/029/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/029/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/029/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/029/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/029/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/029/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/029/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/029/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_029_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/030/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/030/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/030/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/030/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/030/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/030/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/030/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/030/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/030/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/030/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_030_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/031/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/031/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/031/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/031/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/031/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/031/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/031/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/031/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/031/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/031/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_031_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/032/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/032/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/032/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/032/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/032/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/032/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/032/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/032/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/032/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/032/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_032_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/033/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/033/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/033/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/033/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/033/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/033/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/033/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/033/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/033/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/033/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_033_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/034/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/034/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/034/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/034/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/034/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/034/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/034/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/034/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/034/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/034/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_034_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/035/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/035/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/035/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/035/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/035/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/035/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/035/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/035/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/035/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/035/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_035_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/036/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/036/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/036/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/036/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/036/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/036/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/036/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/036/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/036/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/036/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_036_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/037/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/037/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/037/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/037/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/037/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/037/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/037/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/037/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/037/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/037/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_037_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/038/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/038/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/038/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/038/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/038/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/038/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/038/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/038/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/038/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/038/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_038_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/039/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/039/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/039/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/039/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/039/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/039/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/039/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/039/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/039/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/039/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_039_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/040/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/040/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/040/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/040/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/040/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/040/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/040/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/040/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/040/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/040/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_040_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/041/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/041/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/041/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/041/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/041/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/041/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/041/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/041/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/041/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/041/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_041_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/042/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/042/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/042/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/042/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/042/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/042/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/042/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/042/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/042/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/042/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_042_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/043/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/043/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/043/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/043/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/043/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/043/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/043/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/043/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/043/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/043/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_043_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/044/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/044/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/044/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/044/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/044/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/044/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/044/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/044/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/044/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/044/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_044_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/045/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/045/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/045/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/045/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/045/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/045/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/045/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/045/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/045/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/045/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_045_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/046/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/046/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/046/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/046/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/046/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/046/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/046/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/046/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/046/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/046/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_046_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/047/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/047/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/047/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/047/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/047/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/047/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/047/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/047/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/047/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/047/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_047_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/048/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/048/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/048/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/048/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/048/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/048/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/048/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/048/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/048/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/048/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_048_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/049/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/049/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/049/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/049/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/049/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/049/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/049/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/049/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/049/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/049/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_049_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/050/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/050/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/050/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/050/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/050/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/050/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/050/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/050/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/050/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/050/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_050_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/051/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/051/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/051/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/051/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/051/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/051/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/051/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/051/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/051/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/051/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_051_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/052/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/052/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/052/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/052/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/052/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/052/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/052/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/052/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/052/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/052/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_052_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/053/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/053/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/053/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/053/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/053/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/053/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/053/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/053/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/053/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/053/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_053_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/054/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/054/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/054/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/054/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/054/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/054/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/054/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/054/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/054/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/054/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_054_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/055/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/055/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/055/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/055/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/055/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/055/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/055/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/055/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/055/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/055/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_055_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/056/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/056/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/056/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/056/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/056/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/056/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/056/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/056/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/056/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/056/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_056_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/057/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/057/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/057/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/057/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/057/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/057/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/057/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/057/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/057/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/057/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_057_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/058/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/058/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/058/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/058/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/058/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/058/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/058/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/058/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/058/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/058/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_058_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/059/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/059/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/059/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/059/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/059/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/059/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/059/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/059/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/059/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/059/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_059_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/060/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/060/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/060/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/060/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/060/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/060/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/060/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/060/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/060/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/060/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_060_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/061/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/061/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/061/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/061/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/061/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/061/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/061/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/061/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/061/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/061/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_061_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/062/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/062/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/062/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/062/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/062/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/062/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/062/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/062/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/062/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/062/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_062_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/063/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/063/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/063/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/063/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/063/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/063/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/063/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/063/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/063/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/063/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_063_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/064/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/064/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/064/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/064/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/064/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/064/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/064/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/064/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/064/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/064/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_064_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/065/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/065/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/065/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/065/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/065/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/065/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/065/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/065/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/065/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/065/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_065_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/066/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/066/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/066/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/066/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/066/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/066/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/066/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/066/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/066/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/066/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_066_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/067/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/067/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/067/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/067/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/067/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/067/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/067/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/067/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/067/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/067/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_067_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/068/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/068/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/068/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/068/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/068/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/068/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/068/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/068/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/068/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/068/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_068_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/069/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/069/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/069/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/069/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/069/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/069/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/069/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/069/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/069/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/069/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_069_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/070/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/070/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/070/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/070/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/070/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/070/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/070/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/070/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/070/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/070/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_070_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/071/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/071/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/071/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/071/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/071/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/071/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/071/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/071/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/071/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/071/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_071_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/072/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/072/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/072/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/072/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/072/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/072/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/072/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/072/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/072/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/072/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_072_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/073/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/073/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/073/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/073/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/073/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/073/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/073/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/073/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/073/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/073/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_073_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/074/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/074/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/074/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/074/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/074/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/074/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/074/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/074/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/074/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/074/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_074_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/075/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/075/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/075/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/075/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/075/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/075/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/075/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/075/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/075/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/075/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_075_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/076/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/076/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/076/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/076/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/076/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/076/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/076/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/076/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/076/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/076/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_076_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/077/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/077/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/077/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/077/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/077/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/077/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/077/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/077/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/077/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/077/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_077_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/078/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/078/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/078/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/078/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/078/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/078/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/078/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/078/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/078/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/078/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_078_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/079/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/079/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/079/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/079/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/079/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/079/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/079/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/079/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/079/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/079/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_079_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/080/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/080/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/080/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/080/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/080/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/080/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/080/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/080/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/080/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/080/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_080_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/081/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/081/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/081/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/081/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/081/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/081/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/081/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/081/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/081/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/081/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_081_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/082/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/082/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/082/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/082/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/082/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/082/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/082/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/082/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/082/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/082/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_082_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/083/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/083/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/083/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/083/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/083/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/083/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/083/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/083/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/083/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/083/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_083_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/084/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/084/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/084/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/084/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/084/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/084/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/084/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/084/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/084/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/084/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_084_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/085/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/085/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/085/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/085/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/085/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/085/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/085/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/085/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/085/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/085/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_085_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/086/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/086/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/086/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/086/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/086/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/086/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/086/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/086/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/086/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/086/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_086_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/087/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/087/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/087/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/087/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/087/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/087/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/087/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/087/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/087/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/087/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_087_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/088/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/088/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/088/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/088/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/088/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/088/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/088/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/088/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/088/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/088/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_088_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/089/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/089/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/089/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/089/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/089/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/089/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/089/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/089/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/089/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/089/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_089_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/090/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/090/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/090/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/090/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/090/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/090/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/090/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/090/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/090/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/090/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_090_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/091/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/091/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/091/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/091/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/091/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/091/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/091/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/091/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/091/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/091/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_091_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/092/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/092/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/092/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/092/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/092/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/092/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/092/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/092/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/092/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/092/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_092_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/093/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/093/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/093/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/093/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/093/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/093/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/093/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/093/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/093/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/093/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_093_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/094/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/094/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/094/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/094/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/094/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/094/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/094/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/094/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/094/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/094/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_094_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/095/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/095/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/095/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/095/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/095/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/095/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/095/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/095/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/095/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/095/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_095_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/096/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/096/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/096/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/096/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/096/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/096/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/096/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/096/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/096/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/096/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_096_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/097/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/097/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/097/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/097/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/097/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/097/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/097/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/097/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/097/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/097/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_097_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/098/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/098/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/098/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/098/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/098/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/098/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/098/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/098/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/098/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/098/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_098_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/099/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/099/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/099/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/099/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/099/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/099/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/099/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/099/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/099/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/099/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_099_data_10_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/100/
diversity data_01_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_01_TRUE.0.stats
diversity data_01_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_01_TRUE.1.stats
diversity data_01_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_01_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_01_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_01_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/100/
diversity data_02_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_02_TRUE.0.stats
diversity data_02_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_02_TRUE.1.stats
diversity data_02_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_02_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_02_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_02_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/100/
diversity data_03_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_03_TRUE.0.stats
diversity data_03_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_03_TRUE.1.stats
diversity data_03_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_03_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_03_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_03_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/100/
diversity data_04_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_04_TRUE.0.stats
diversity data_04_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_04_TRUE.1.stats
diversity data_04_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_04_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_04_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_04_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/100/
diversity data_05_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_05_TRUE.0.stats
diversity data_05_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_05_TRUE.1.stats
diversity data_05_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_05_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_05_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_05_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/100/
diversity data_06_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_06_TRUE.0.stats
diversity data_06_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_06_TRUE.1.stats
diversity data_06_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_06_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_06_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_06_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/100/
diversity data_07_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_07_TRUE.0.stats
diversity data_07_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_07_TRUE.1.stats
diversity data_07_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_07_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_07_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_07_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/100/
diversity data_08_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_08_TRUE.0.stats
diversity data_08_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_08_TRUE.1.stats
diversity data_08_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_08_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_08_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_08_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/100/
diversity data_09_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_09_TRUE.0.stats
diversity data_09_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_09_TRUE.1.stats
diversity data_09_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_09_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_09_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_09_TRUE.3.stats
cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/fullRange/100/
diversity data_10_TRUE.phy &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_10_TRUE.0.stats
diversity data_10_TRUE.phy -g &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_10_TRUE.1.stats
diversity data_10_TRUE.phy -g -m -p 2> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_10_TRUE.2.stats
cat /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_10_TRUE.2.stats | sed 's/\.0000/,/g' | sed 's/-/,-,/g' | sed 's/      ,//g'  | tail -n+15 > /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/stats/ST_100_data_10_TRUE.3.stats
