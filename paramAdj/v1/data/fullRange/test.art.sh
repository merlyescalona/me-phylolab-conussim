#!/bin/bash
# SGE submission options
#$ -l num_proc=1         # number of processors to use
#$ -l h_rt=00:10:00      # Set 10 mins  - Average amount of time for up to 1000bp
#$ -t 1-1000              # Number of jobs/files that will be treated
#$ -N art.sims           # A name for the job
#$ -q compute-1-x.q

module load bio/art/050616

inseed=$(awk 'NR==$SGE_TASK_ID{print $1}' /home/merly/cesga/parameterization/fullRange/seed.file)
outseed=$(awk 'NR==$SGE_TASK_ID{print $2}' /home/merly/cesga/parameterization/fullRange/seed.file)
art_illumina -f 100 -l 150 -p  -m 250 -s 50 -rs $RANDOM -ss HS25 -i $inseed -o $outseed

module unload bio/art/050616
