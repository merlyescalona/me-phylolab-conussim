packages=c("tidyr","RSQLite","ggplot2","ape","phangorn")
for(pkg in packages ){
  suppressMessages(library(pkg,character.only=TRUE,quietly=TRUE))
}
dbfile1<-paste("data/FRNE10K/FRNE10K.db",sep="")
con1 = dbConnect(drv=SQLite(),  dbname=dbfile1)
q8 = dbGetQuery( con1,'select SID,Extra_l from Gene_Trees' )
dbfile2<-paste("data/FRNE50K/FRNE50K.db",sep="")
con2 = dbConnect(drv=SQLite(),  dbname=dbfile2)
qq8 = dbGetQuery( con2,'select SID,Extra_l from Gene_Trees' )
sq8=split(q8,q8$SID)
extraLineagesAvgSTN10K<-data.frame(sapply(sq8,function(x){mean(x$Extra_l)}))
extraLineagesAvgSTN10K$ST=1:100
extraLineagesAvgSTN10K$Ne=rep("10K",100)
colnames(extraLineagesAvgSTN10K)<-c("Extra_l","ST","Ne")
sqq8=split(qq8,qq8$SID)
extraLineagesAvgSTN50K<-data.frame(sapply(sqq8,function(x){mean(x$Extra_l)}))
extraLineagesAvgSTN50K$ST=1:100
extraLineagesAvgSTN50K$Ne=rep("50K",100)
colnames(extraLineagesAvgSTN50K)<-c("Extra_l","ST","Ne")
extraLineagesSummary=data.frame(rbind(extraLineagesAvgSTN10K,extraLineagesAvgSTN50K))

allRFN10K=c()
allRFN50K=c()
for (stIndex in 1:100){
  st=sprintf("%03d",stIndex)
  filename=paste("data/FRNE10K/",st,"/ALL_",st,".trees",sep="")
  trees=read.tree(filename)
  rf=unlist(as.list(RF.dist(trees)))
  allRFN10K=cbind(allRFN10K,rf)
  filename=paste("data/FRNE50K/",st,"/ALL_",st,".trees",sep="")
  trees=read.tree(filename)
  rf=unlist(as.list(RF.dist(trees)))
  allRFN50K=cbind(allRFN50K,rf)
}
allRFN10K=data.frame(allRFN10K)
colnames(allRFN10K)=paste0("ST",sprintf("%03d",1:100))
allRFN50K=data.frame(allRFN50K)
colnames(allRFN50K)=paste0("ST",sprintf("%03d",1:100))


rfDataN10K<- gather(allRFN10K,value = "RFDist",key = "ST",1:100)
rfDataN50K<- gather(allRFN50K,value = "RFDist",key = "ST",1:100)

ggplot(mapping=aes(x=ST))+geom_violin(data=rfDataN10K,aes(x=ST,y=RFDist))+geom_line(data=subset(extraLineagesSummary, Ne=="10K"), aes(x=ST,y=Extra_l), color="cornflowerblue")+  scale_x_discrete(labels = 1:100) +theme_classic()+ theme(axis.text.x = element_text(angle = 90, hjust = 1))+xlab("Species tree index")+ylab("Robinson-Foulds Distance")+ggtitle("Ne=10K")

ggplot(mapping=aes(x=ST))+geom_violin(data=rfDataN50K,aes(x=ST,y=RFDist))+geom_line(data=subset(extraLineagesSummary, Ne=="50K"), aes(x=ST,y=Extra_l), color="springgreen4")+  scale_x_discrete(labels = 1:100)+theme_classic()+ theme(axis.text.x = element_text(angle = 90, hjust = 1))+xlab("Species tree index")+ylab("Robinson-Foulds Distance")+ggtitle("Ne=50K")


meanRD10K<-as.numeric(sapply(split(rfDataN10K, rfDataN10K$ST), function(x){mean(x$RFDist)}))
meanRD50K<-as.numeric(sapply(split(rfDataN50K, rfDataN50K$ST), function(x){mean(x$RFDist)}))
subsetILS10K=subset(extraLineagesSummary, Ne=="10K")$Extra_l
subsetILS50K=subset(extraLineagesSummary, Ne=="50K")$Extra_l

wholeDataset=data.frame(
  Ne=c(rep("10K",100),rep("50K",100)),
  AvgRFDist=c(meanRD10K,meanRD50K),
  ILS=c(subsetILS10K,subsetILS50K)
)


ggplot(wholeDataset,aes(x=AvgRFDist,y=ILS))+geom_point()+facet_wrap(~Ne)+
  stat_smooth(method = "lm", col = "red")+theme_bw()+
  xlab("Average Robinson-Foulds distance per Species tree replicate")+ylab("Average number of extra lineages per Species tree replicate")

cor(meanRD10K,subsetILS10K)
# [1] 0.13722
cor(meanRD50K,subsetILS50K)
# [1] 0.3230542