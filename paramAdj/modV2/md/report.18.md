# Extra lineages

Number of extra lineages per gene trees across species tree replicate. This information is a  direct measure of the incomplete lineage sorting (ILS).

The value of the extra lineages is strongly correlated with the Robinson-Foulds (RF) distance, which is used more often.

Reference values can be checked out on:

Siavash Mirarab and Tandy Warnow, in their mammals and birds datasets. Is possible that they have obtain some values from the empiricals, they for sure have information of gene tree lengths,

> For comparison, mean gene tree length on avian is 0.25 for exons and 0.75 introns; on mammals is .69, and  for 1kp (plants going back 700M years) is 10.7


## Expected values

- We are interested in having extra lineages, but not a high number of it, which means existence of ILS.

- We observed that the distribution of values is skewed to the lower values.

- There's a high frequency of trees with `0` extra lineages (`181/1000`)(`~18%`).

- Trees with extra lineages values lower than `37` are concentrated in the `25%`-`75%` quartiles.
