### Tree-wide effective population size [`SP`]

For previous scenarios we were working with a effective population size (*Ne*) of `100.000`, following frequent estimation of population sizes.?

**Ne** and is directly related to species tree heights, which influences the generation of extra lineages. Year ranges used for the species tree heights will not be changed.

### Tree-wide substitution rate [`SU`]

Substitution rate for some distributions will be:
```
subs.rate=1/SU
```

FWhile for others will be the specific value. For instance, for a specific `subs.rate=10e-7`, we would have to use `SU=F:0.0000001`.  If we want a bit of variation around 10e-7, rate is sampled from an exponential distribution that has to be set around the value (mean) we want: 10e-7, being the rate:  

`Mean subs.rate = 1/EXP(10e7)`

**For previous scenarios, the values chosen for a `lognormal` distribution  that comprised values from `10e-7` to `10e-9` that seemed  to approach better the biological range. These values gave us a lot of variation, hence the selection of lower values (range `10e-8` to `10e-10`), and a `Uniform` distribution, to understand better its influence in the generated data.**
