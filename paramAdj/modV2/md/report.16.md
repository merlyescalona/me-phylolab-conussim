# Length of the branch from the ingroup linking to the species tree root

## Expected values

In our case it can vary between `~200K` - `~20My` because in the case of trees with very shallow ingroups the distance IngroupRoot to Root is almost equal to the tree height.


Observe data is consistent with the expected values.
