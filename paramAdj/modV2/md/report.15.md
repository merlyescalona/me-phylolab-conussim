# Species tree height/length

The coalescent units calculation should be this for diploids:

```
Ne=100000
cu=1/(2*Ne)
```

But, `SimPhy` simulates haploid genomes, therefore, the calculations are:

- For `Ne=10.000`

```
cu=1/Ne=1/10000=1e-04
cu200K=cu*200000=20
cu20M=cu*200000=2000
```

- And, for `Ne=50.000`

```
cu=1/Ne=1/50000=2e-05
cu200K=cu*200000=4
cu20M=cu*200000=400
```

Then, the outgroup is added a posteriori, which in our case the value is set as `1`. Taken into account that, the length of the root ingroup to the root, is the same as the ingroup tree, the quantities multiply by 2. Observed results coincide with the expected ones.

Also, species tree lengths should be larger than the species tree heights, according to the values obtained, this seems consistent too.
