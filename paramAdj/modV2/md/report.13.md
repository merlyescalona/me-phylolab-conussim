# Tree-wide substitution rate

## Expected values


We changed the distribution  of the substitution rate from a `LogNormal` distribution (`LN:-17,2.49`), with:
- Maximum: `10e-7`
- Minimum: `10e-9`

To a `Uniform` distribution with values:

- Maximum: `10e-8`
- Minimum: `10e-10`


## Observation

Values of the distribution should be considered to follow the expected distribution, although there's a strong biased to `10e-9`, probably due to the low number of replicates.

**Note:** It seems that this is not reflected in the variability of the sequences (check Replicates tab).
