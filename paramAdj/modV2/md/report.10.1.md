# Number of individuals per taxa

## Expected values

As introduced in the configuration, both runs are expected to have a values of  number of individuals per taxa following a `Uniform` distribution, between `2`, `20`.

Not exactly what's observed, we can see that for the `Ne=10K` case, values are a bit more variable than for the `Ne=50K` case, where are more uniform. Only explanation I can give, is that is due to the low number of replicates made, since minimum and maximum values are equal.
