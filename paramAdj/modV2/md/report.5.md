
SimPhy simulates three heterogeneity levels: species-specific, gene-family-specific and gene-by-lineage specific. The species-specific is applied at the species tree level (some species evolve faster than others), the gene-family-specific at the locus tree level (some locus trees/loci evolve faster than others), and the gene-by-lineage-specific at the gene tree level (each branch of the gene tree evolves at its own modified speed). All of them are sampled from a **gamma distributions** with `mean=1`, which are therefore only parameterized by `1` parameter. That parameter can be fixed or sampled from a prior distribution **(their multiplication is the final heterogeneity)**.

### Species-specific heterogeneity

Controlled by the `HS` hyperparameter at the species level. When we use the full model, for each species tree (i.e.,replicate) we sample a value from the distribution specified using `HS`. That value, will be the parameter of the gamma distribution used to sample the multipliers for each branch length (i.e., species) in that tree. **For shallow simulations where species should share similar life-history traits (`=traits`) this heterogeneity source can be left out.**

### Gene-family-specific

Controlled at the species level by the `HL` parameter. When we use the full model, for each species tree (i.e.,replicate) we sample a value from the distribution specified using `HL`. That value, will be the parameter of the gamma distribution used to sample a multiplier for each locus tree (i.e., locus) in that replicate. **With the proposed distribution (`LN: 1.2, 1`) we are allowing it to vary around 10x (15x max), which makes sense**.

### Gene-by-lineage-specific rate heterogeneity

This is the complex parameter, since it is applied at the branches of the gene tree, and therefore may be controlled by up to hyperhyperhyperdistributions. This means, you could have a value indicating how heterogeneous is the replicate, the locus within that replicate, and the gene tree within that locus. The value sampled from the GP distribution for each replicate determines how variable will be that replicate, the `HH` parameter is then sampled for each locus, determining how variable is that locus, and at the gene tree level you may simulate different variabilities using the `HG` parameter.

In our simulations, `HH` and `HG` are equivalent (we always have only one gene tree per locus tree) and therefore we only have to use one of those. If we were not specifically interested in this parameter, we should just use a fixed value, like the `F:200` proposed somewhere (which means that a multiplier for each gene tree branch will be sampled from a gamma distribution with `mean=1` and `parameter=200` for all branches of all trees), or used a distribution that allows a bit more of variations as the `LN:1.4,1`.
