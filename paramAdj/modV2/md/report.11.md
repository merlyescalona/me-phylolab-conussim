# Species tree leaves

## Expected values

As introduced in the configuration, we were expecting a `Uniform` distribution, between `4`, `20`.
Since we are asking for an outgroup, we will get the minimum and maximum values plus one.


The results for both runs, show a correct data distribution.
