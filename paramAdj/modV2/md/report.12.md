# Speciation rate

## Expected values

Sampled from a LogNormal distribution (`LN:-13.58,1.85`),

- Maximum: `1.24E-5` [`ln(12)/200000=rate`]
- Minimum: `1.24E-7` [`ln(12)/20000000=rate`]


These values are approximate, we are actually looking for values within the quantiles `0.1` and `0.9` of the distribution.

## Observation

The distribution for both runs is skewed to the left, more skewed for `Ne=50K` case , than for the `Ne=10K` case. (biased to the lower limit of the expected values).

We must take into account that we are fixing the number of species that we want to simulate, and if we get low speciation rate values, we will obtain low species tree lengths. Hence, the relationship within the ingroup species will be shallower. We see that as for a pretty uniform tree height distribution, we get a skewed distribution of species tree length (shorter trees).
