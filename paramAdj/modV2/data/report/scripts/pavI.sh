#! /bin/bash
#$ -o /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/output/pavI.o
#$ -e /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/output/pavI.e
#$ -N pavI

cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization
/home/uvi/be/mef/bin/simphy -rs 1000 -rl F:1000 -su LN:-17,2.49 -sb LN:-13.58,1.85 -sl U:4,20 -si U:2,20 -sp F:100000 -st U:200000,2500000 -so F:1 -sg F:1 -gp LN:1.4,1 -hh LN:1.2,1 -hg F:GP  -v 1 -o pavI -cs 30489 -om 1 -od 1 -op 1 -oc 1 -on 1

