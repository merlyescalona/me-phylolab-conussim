#! /bin/bash
#$ -o /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/output/FRNE50K.o
#$ -e /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/output/FRNE50K.e
#$ -N FRNE50K

cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization
/home/uvi/be/mef/bin/simphy -rs 100 -rl F:10 -su U:100000000,10000000000 -sb LN:-13.58,1.85 -sl U:4,20 -si U:2,20 -sp F:50000 -st U:200000,20000000 -so F:1 -sg F:1 -gp LN:1.4,1 -hh LN:1.2,1 -hg F:GP  -v 1 -o FRNE50K -cs 26974 -om 1 -od 1 -op 1 -oc 1 -on 1

