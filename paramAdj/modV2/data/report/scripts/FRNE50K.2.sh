#! /bin/bash
#$ -o /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/output/FRNE50K.2.o
#$ -e /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/output/FRNE50K.2.e
#$ -N FRNE50K.2

cd /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization
perl /home/uvi/be/mef/bin/INDELIble_wrapper.pl /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/FRNE50K /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/controlFiles/indelible.full.range.txt 11998 1 &> /home/uvi/be/mef/mnt/phylolab/uvibemef/parameterization/report/output/FRNE50K.2.txt


