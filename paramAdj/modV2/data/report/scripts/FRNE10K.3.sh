#! /bin/bash
#$ -o /home/merly/research/parameterization/report/output/FRNE10K.3.o
#$ -e /home/merly/research/parameterization/report/output/FRNE10K.3.e
#$ -N FRNE10K.3

SEED=$(awk "NR==$SGE_TASK_ID" /home/merly/research/parameterization/report/files/FRNE10K.3.files )
echo $SEED
cd $SEED
module load bio/indelible/1.03
indelible


