# Introduction

This repository focuses on the pipeline development for both simulation
of NGS reads from phylogenies and its analyisis.

## Folder structure
- **Consensus**: Python program for generation of consensus sequences from diploid 
individuals
- **LociRefSelection**:  Python scripts for the selection of reference loci from
multiple sequence alignment files
- **mapq**:  
- **monitorization**: bash scripts developed for the monitorization of jobs 
launched into an SGE cluster.
- **papers**:  code from papers for testing purposes
- **pipelines**:  all the pipeline developed so far
- **post-processing**: scripts for generic post-processing steps (cleaning, 
trimming,...)
- **report**:  Rmd/md files for report generation
- **SeqCutter**:  Python program for the fragmentation of sequences
- **simphy.ngs.wrapper**:  Python add-on for SimPhy simulator
- **variants**:  Scripts related to the parsing of VCF files
