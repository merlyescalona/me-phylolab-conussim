import argparse,datetime,logging,os,sys
from MEOutputFormatter import MEOutputFormatter as mof
from MELoggingFormatter import MELoggingFormatter as mlf
from MELogLevels import MELogLevels as mll
import numpy as np
import random as rnd
from select import select

class LociReferenceSelection:
	"""
		LociReferenceSelection:
		This programs selects references loci would have been used to design the probes
		for a capture experiment.
	"""
	def __init__(self, args):
		"""
			Args:
				args: Command-line parameters.

			Returns:
				The whole processing for the selection of reference loci that would
				have been used to design the probes for a capture experiment.
		"""
		self.method=args.method
		self.inputprefix=args.input_prefix
		self.outputprefix=args.output_prefix
		self.numLoci=0
		self.numLociDigits=0
		self.numSpeciesTrees=0
		self.numSpeciesTreesDigits=0
		self.projectName=""
		if (args.simphy_folder[-1]=="/"):
			self.projectName=os.path.basename(args.simphy_folder[0:-1])
		else:
			self.projectName=os.path.basename(args.simphy_folder)
		self.path=os.path.abspath(args.simphy_folder)
		# ----------------------------------------------------------------------
		# Logging
		# ----------------------------------------------------------------------
		self.appLogger=logging.getLogger('')
		loggerFormatter=mlf(fmt="%(asctime)s - %(levelname)s:\t%(message)s",datefmt="%d/%m/%Y %I:%M:%S %p")
		fileHandler=logging.FileHandler(filename="{0}/{1}.lociselection.log".format(self.path, self.projectName))
		fileHandler.setFormatter(loggerFormatter)
		fileHandler.setLevel(level=logging.DEBUG)
		cmdHandler = logging.StreamHandler(sys.stdout)
		cmdHandler.setFormatter(loggerFormatter)
		cmdHandler.setLevel(self.getLogLevel(args.log))
		self.appLogger.addHandler(fileHandler)
		self.appLogger.addHandler(cmdHandler)
		logging.addLevelName(mll.CONFIG_LEVEL, "CONFIG")
		self.appLogger.log(logging.INFO,"Starting")
		self.startTime=datetime.datetime.now()
		self.endTime=None
		# Output folder
		outputFolder=""
		if (args.output[-1]=="/"):
			outputFolder=os.path.basename(args.output[0:-1])
		else:
			outputFolder=os.path.basename(args.output)
		self.output=os.path.abspath("{0}/{1}".format(self.path, outputFolder))
		# Checking output path
		try:
			os.mkdir(self.output)
		except:
			self.appLogger.info("Output folder ({0}) exists. ".format(self.output))

	def checkArgs(self):
		"""
			Checks the correctness of the input parameters.
			Returns:
				Nothing
		"""
		self.appLogger.log(mll.CONFIG_LEVEL,"Checking arguments...")
		simphydir=os.path.exists(self.path)
		if simphydir:
			self.appLogger.log(mll.CONFIG_LEVEL,"SimPhy folder exists:\t{0}".format(simphydir))
		else:
			self.ending(False, "SimPhy folder does not exist.")

		fileList=os.listdir(os.path.abspath(self.path))
		for index in range(0,len(fileList)):
			fileList[index]=os.path.abspath(os.path.join(self.path,fileList[index]))

		command = os.path.join(self.path,"{0}.command".format(self.projectName))
		params = os.path.join(self.path,"{0}.params".format(self.projectName))
		db = os.path.join(self.path,"{0}.db".format(self.projectName))

		self.appLogger.log(mll.CONFIG_LEVEL,"SimPhy files (command, params, db)")
		self.appLogger.log(mll.CONFIG_LEVEL,"{0}:\t{1}".format(os.path.basename(db),db in fileList))
		self.appLogger.log(mll.CONFIG_LEVEL,"{0}:\t{1}".format(os.path.basename(command),command in fileList))
		self.appLogger.log(mll.CONFIG_LEVEL,"{0}:\t{1}".format(os.path.basename(params),params in fileList))

		simphyfiles=((command in fileList) and (params in fileList) and(db in fileList))
		if not simphyfiles:
			self.ending(False, "SimPhy files do not exist.")

		# check how many of them are dirs
		for item in fileList:
			baseitem=os.path.basename(item)
			if (os.path.isdir(os.path.abspath(item)) and  baseitem.isdigit()):
				self.numSpeciesTrees=self.numSpeciesTrees+1
		self.numSpeciesTreesDigits=len(str(self.numSpeciesTrees))
		# check if at least one
		self.appLogger.debug("Num species trees:\t{0}".format(self.numSpeciesTrees))

		if not (self.numSpeciesTrees>0):
			self.ending(False, "Number (at least 1) of species trees replicate/folders:\t{0}".format(self.numSpeciesTrees>0))


	def iterateOverReplicate(self):
		"""
			Iterates over all the species trees replicates of the simphy project.

			Returns:
				Nothing

		"""
		for indexST in range(1, self.numSpeciesTrees+1):
			curReplicatePath="{0}/{1:0{2}d}/".format(self.path,indexST, self.numSpeciesTreesDigits)
			self.appLogger.info("Replicate {0}/{2} ({1})".format(indexST, curReplicatePath,self.numSpeciesTrees))
			prefixLoci=0
			fileList=os.listdir(curReplicatePath)
			for item in fileList:
				if ("{0}_".format(self.inputprefix) in item) and (".fasta" in item):
					prefixLoci+=1

			self.numLoci=prefixLoci
			print(self.numLoci)
			self.numLociDigits=len(str(self.numLoci))
			print(self.numLociDigits)
			self.appLogger.info("Method chosen: {0}")
			if self.method==0:
				self.methodOutgroup(indexST)
			elif self.method==1:
				self.methodRandomIngroup(indexST)
			elif self.method==2:
				self.methodConsensusRandomSpecies(indexST)
			elif self.method==3:
				self.methodConsensusAll(indexST)
			else:
				self.seqPerSpecies(indexST)

	def seqPerSpecies(self,indexST):
		"""
			Method 4 for the selection of reference loci.

			This method selects a random single sequence from each species
			as a reference locus.

			Args:
				indexST: Index of the species tree that is being used.

			Returns:
				Nothing
		"""
		for indexLOC in range(1,self.numLoci+1):
			self.appLogger.info("Loci {0}/{1}".format(indexLOC,self.numLoci))
			lociData=self.parseLocFile(indexST,indexLOC)
			sequences=[] # [(description,seq)]
			keys=lociData.keys()
			for k in keys:
				subkeys=lociData[k].keys() # Getting subkeys
				rndKey1=0;
				try:
					rndKey1=rnd.sample(subkeys,1)[0]
				except:
					rndKey1=0

				selected=lociData[k][rndKey1]
				sequences+=[(selected["description"], selected["sequence"])]
			self.writeSelectedLociMultipleSpecies(indexST,indexLOC,sequences)
		self.appLogger.info("Done Seq Per Species")


	def methodOutgroup(self,indexST):
		"""
			Method 0 for the selection of reference loci.

			This method selects the outgroup as a reference locus.

			Args:
				indexST: Index of the species tree that is being used.

			Returns:
				Nothing

		"""
		for indexLOC in range(1,self.numLoci+1):
			self.appLogger.info("Loci {0}/{1}".format(indexLOC,self.numLoci))
			lociData=self.parseLocFile(indexST,indexLOC)
			selected=lociData["0"]["0"]
			self.writeSelectedLoci(indexST,indexLOC,selected["description"],selected["sequence"])
		self.appLogger.info("Done outgroup sequence")


	def methodRandomIngroup(self,indexST):
		"""
			Method 1 for the selection of reference loci.

			This method selects a random sequence from the ingroup species as
			a reference and from all the loci.

			Args:
				indexST: Index of the species tree that is being used.

			Returns:
				Nothing

		"""
		for indexLOC in range(1,self.numLoci+1):
			self.appLogger.info("Loci {0}/{1}".format(indexLOC,self.numLoci))
			lociData=self.parseLocFile(indexST,indexLOC)
			keys=lociData.keys()
			rndKey1="0"; rndKey2="0"
			try:
				rndKey1=rnd.sample(set(keys)-set("0"),1)[0]
			except:
				rndKey1="0"
			subkeys=lociData[rndKey1]
			try:
				rndKey2=rnd.sample(len(subkeys),1)[0]
			except:
				rndKey2="0"
			selected=lociData[rndKey1][rndKey2]
			self.writeSelectedLoci(indexST,indexLOC,selected["description"],selected["sequence"])
		self.appLogger.info("Done random ingroup sequence")

	def methodConsensusRandomSpecies(self,indexST):
		"""
			Method 2 for the selection of reference loci.

			This method selects a consensus sequence, obtained from the random selection
			of a species, and then computing a consensus from all the sequences
			within the selected species.

			Args:
				indexST: Index of the species tree that is being used.
			Returns:
				Nothing

		"""
		for indexLOC in range(1,self.numLoci+1):
			self.appLogger.info("Loci {0}/{1}".format(indexLOC,self.numLoci))
			lociData=self.parseLocFile(indexST,indexLOC)
			keys=lociData.keys()
			rndKey1=0; rndKey2=0
			try:
				rndKey1=rnd.sample(set(keys)-set("0"),1)[0]
			except:
				rndKey1=0
			subkeys=lociData[rndKey1]
			sequences=[]
			for sk in subkeys:
				sequences+=[lociData[rndKey1][sk]["sequence"]]
			selected=self.computeConsensus(sequences)
			selectedDes=">consensus_sp_{0}".format(rndKey1)
			self.writeSelectedLoci(indexST,indexLOC,selectedDes,selected)
		self.appLogger.info("Done random ingroup consensus")



	def methodConsensusAll(self,indexST):
		"""
			Method 3 for the selection of reference loci.

			Computes the consensus from all the sequences of a gene tree file,
			and uses this sequence as reference loci.

			Args:
				indexST: Index of the species tree that is being used.
			Returns:
				Nothing
		"""
		for indexLOC in range(1,self.numLoci+1):
			self.appLogger.info("Loci {0}/{1}".format(indexLOC,self.numLoci))
			lociData=self.parseLocFile(indexST,indexLOC)
			keys=set(lociData.keys())-set("0")
			sequences=[]
			for mk in keys:
				subkeys=lociData[mk].keys()
				for sk in subkeys:
					sequences+=[lociData[mk][sk]["sequence"]]
			selected=self.computeConsensus(sequences)
			self.writeSelectedLoci(indexST,indexLOC,">consensus_all",selected)
		self.appLogger.info("Done all ingroups consensus")


	def computeConsensus(self, sequences):
		"""
			Method for the computation of a consensus sequence from a set
			of sequences.

			Args:
				sequences: list of the sequences

			Returns:
				Single consensus sequence
		"""
		conseq=""
		# Assume that all the sequences in a file have the same length
		seqSize=len(sequences[0])
		# Need to know how many sequences I have
		numSeqs=len(sequences)
		# Seqs for all nucleotides
		A=np.zeros(seqSize);
		C=np.zeros(seqSize);
		G=np.zeros(seqSize);
		T=np.zeros(seqSize);
		N=np.zeros(seqSize);
		for indexCol in range(0, seqSize):
			for indexRow in range(0,numSeqs):
				if sequences[indexRow][indexCol]=="A": A[indexCol]+=1
				if sequences[indexRow][indexCol]=="C": C[indexCol]+=1
				if sequences[indexRow][indexCol]=="G": G[indexCol]+=1
				if sequences[indexRow][indexCol]=="T": T[indexCol]+=1
				if sequences[indexRow][indexCol]=="N": N[indexCol]+=1

		for indexCol in range(0,seqSize):
			if A[indexCol] > C[indexCol] and A[indexCol] > G[indexCol] and A[indexCol] > T[indexCol] and A[indexCol] > N[indexCol]:
				conseq+="A"
			elif C[indexCol] > A[indexCol] and C[indexCol] > G[indexCol] and C[indexCol] > T[indexCol] and C[indexCol] > N[indexCol]:
				conseq+="C"
			elif G[indexCol] > A[indexCol] and G[indexCol] > C[indexCol] and G[indexCol] > T[indexCol] and G[indexCol] > N[indexCol]:
				conseq+="G"
			elif T[indexCol] > A[indexCol] and T[indexCol] > C[indexCol] and T[indexCol] > G[indexCol] and T[indexCol] > N[indexCol]:
				conseq+="T"
			else: conseq+="N"

		self.appLogger.info("Consensus computed")
		return conseq


	def writeSelectedLociMultipleSpecies(self,indexST,indexLOC,seqs):
		"""
			Writes multiple sequences in a single file.

			Args:
				indexST: Index of the species tree that is being used.
				indexLOC: Index of the locus being used.
				seqs: sequence of the locus to be written.

			Returns:
				Nothing

			Generates a file for all the selected loci.
		"""
		# seqs=[(description,seq), ..., (description,seq)]
		self.appLogger.info("Writing selected loci {1} from ST: {0}", indexST,indexLOC)
		outname="{0}/REF_{5}_{1:0{2}d}_{3:0{4}d}.fasta".format(\
			self.output,\
			indexST,\
			self.numSpeciesTreesDigits,\
			indexLOC,\
			self.numLociDigits,\
			self.outputprefix
		)
		outfile=open(outname,'a')
		for item in range(0,len(seqs)):
			des=seqs[item][0]
			nucSeq=seqs[item][1]
			newDes=">{0}:{1:0{2}d}:REF:{7}:{6}:{3:0{4}d}:{5}".format(\
				self.projectName,\
				indexST,\
				self.numSpeciesTreesDigits,\
				indexLOC,\
				self.numLociDigits,\
				des[1:len(des)],\
				self.inputprefix,\
				self.outputprefix
			)
			outfile.write("{0}\n{1}\n".format(newDes,nucSeq))

		outfile.close()

	def writeSelectedLoci(self,indexST,indexLOC,des,seq):
		"""
			Writes a single sequence per file.

			Args:
				indexST: Index of the species tree that is being used.
				indexLOC: Index of the locus being used.
				des: description of the sequence to be written.
				seq: sequence of the locus to be written.

			Returns:
				Nothing

			Generates a file per selected locus.
		"""
		self.appLogger.info("Writing selected loci {1} from ST: {0}", indexST,indexLOC)
		outname="{0}/REF_{4}_{3}_{1:0{2}d}_{5:0{6}d}.fasta".format(\
			self.output,\
			indexST,\
			self.numSpeciesTreesDigits,\
			self.inputprefix,\
			self.outputprefix,\
			indexLOC,\
			self.numLociDigits
		)
		newDes=">{0}:{1:0{2}d}:REF:{7}:{6}:{3:0{4}d}:{5}".format(\
			self.projectName,\
			indexST,\
			self.numSpeciesTreesDigits,\
			indexLOC,\
			self.numLociDigits,\
			des[1:len(des)],\
			self.inputprefix,\
			self.outputprefix
		)
		# I'm assuming that if the file does not exist it will be created
		outfile=open(outname,'a')
		outfile.write("{0}\n{1}\n".format(newDes,seq))
		outfile.close()

	def parseLocFile(self,indexST,indexLOC):
		"""
			Parses the file of the corresponding locus, from the corresponding
			species tree into a dictionary.

			Args:
				indexST: Index of the species tree that is being used.
				indexLOC: Index of the locus being used.
			Returns:
				A dictionary with all the sequences and their corresponding
				description.
		"""
		self.appLogger.info("Parsing...")
		# 1. Reading the sequences of the file into a dictionary
		fastapath="{0}/{1:0{2}d}/{3}_{4:0{5}d}.fasta".format(\
			self.path,\
			indexST,\
			self.numSpeciesTreesDigits,\
			self.inputprefix,\
			indexLOC,\
			self.numLociDigits)
		fastafile=open(fastapath, 'r')
		lines=fastafile.readlines()
		fastafile.close()
		seqDict=dict()
		description=""; seq=""; tmp="";count=1
		for line in lines:
			if not (line.strip()==''):
				if (count%2==0):
					seq=line[0:-1].strip()
					try:
						test=seqDict[tmp[0]]
					except:
						seqDict[tmp[0]]={}
					try:
						seqDict[tmp[0]].update({tmp[2]:{\
    						'description':description,\
    						'sequence':seq\
							}})
					except:
						seqDict[tmp[0]][tmp[2]]={}
						seqDict[tmp[0]].update({tmp[2]:{\
							'description':description,\
							'sequence':seq\
							}})
					seq=None
					description=None
					tmp=None
				else:
					description=line[0:-1].strip()
					tmp=description[1:len(description)].split("_")
				count=count+1
		return seqDict

	def ending(self, good, message):
		"""
			Ending  (method to properly end the program)
			Args:
				good: Boolean, to represent whether is a correct or wrong ending.
				message: message to be shown.
			Returns:
				Nothing

			Exits the program.
		"""
		# good: Whether there's a good ending or not (error)
		if not good:
			self.appLogger.error(message)
		self.endTime=datetime.datetime.now()
		self.appLogger.info("--------------------------------------------------------------------------------")
		self.appLogger.info("Elapsed time:\t{0}".format(self.endTime-self.startTime))
		self.appLogger.info("Ending at:\t{0}".format(self.endTime.strftime("%a, %b %d %Y. %I:%M:%S %p")))
		sys.exit()

	def log(self, level, message):
		"""
			Identification of the method to generate a log input according
			to the log level sent.

			Args:
				level: Log level selected.
				message: message to be shown.
			Returns:
				Nothing
		"""
		if level==logging.DEBUG:
			self.appLogger.debug(message)
		if level==logging.INFO:
			self.appLogger.info(message)
		if level==logging.WARNING:
			self.appLogger.warning(message)
		if level==logging.ERROR:
			self.appLogger.error(message)
		return None

	def getLogLevel(self,level):
		"""
			Identification of the log level selected within the log levels
			available.

			Args:
				level: Log level selected.
			Returns:
				specific log level from the available ones.
		"""
		if level==mll.LOG_LEVEL_CHOICES[0]:
			loggingLevel=logging.DEBUG
		elif level==mll.LOG_LEVEL_CHOICES[1]:
		# show info
			loggingLevel=logging.INFO
		elif level==mll.LOG_LEVEL_CHOICES[2]:
		# show info, warnings
			loggingLevel=logging.WARNING
		else:
			loggingLevel=logging.ERROR
		# every line goes into a file
		return loggingLevel

	def run(self):
		"""
			Run process of the program.
		"""
		self.checkArgs()
		self.iterateOverReplicate()
		self.ending(True,"Loci Reference Selection finished")
