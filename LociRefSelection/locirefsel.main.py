import argparse,datetime,logging,os,sys
import LociReferenceSelection as lrf
from MEOutputFormatter import MEOutputFormatter as mof
from MELoggingFormatter import MELoggingFormatter as mlf
from MELogLevels import MELogLevels as mll
import numpy as np
import random as rnd
from select import select
################################################################################
# CONSTANTS
VERSION=1
MIN_VERSION=0
FIX_VERSION=9
AUTHOR="Merly Escalona <merlyescalona@uvigo.es>"
################################################################################
# python LociReferenceSelection.py -p <prefix> -SF <simphy_path> -o outout -m method
################################################################################

def handlingParameters():
	"""
		Handling parameters.
		This functiuon takes care of the cmd-line input.
	"""

	parser = argparse.ArgumentParser(\
	prog="locirefsel.main.py (v.{0}.{1}.{2})".format(VERSION,MIN_VERSION,FIX_VERSION),\
		formatter_class=argparse.RawDescriptionHelpFormatter,\
		description=\
'''
Description:
============
This programs selects references loci would have been used to design the probes
for a capture experiment.

Assumptions:
============
- Main assumptions is that I'm working in a SimPhy project, which means folder
structure belongs to a SimPhy output folder.

- This is working module of a Pipeline for the generation of sequencing reads
from a phylogeny. As is, the requirements to launch this is script, one would
need to run it after getting the sequences (INDELible run).

General:
========
Specified method to obtain the reference loci used for the design of probes.
Values range from 0-4 ( Default: (0)), where:

	(0): Considers the outgroup sequence as the reference loci;
	(1): Selects a random sequence from the ingroups;
	(2): Selects randomly a specie and a consensus sequence of the sequences belonging to that species;
	(3): Generates a consensus sequences from all the sequences involved.
	(4): Extracts one (1) sequence per species.

NOTE: The higher the method number, the longer it will take to generate the reference loci.

Output:
=======

- The output will be a directory of FASTA files.
- There should be as many FASTA files as replicates (Species Trees) have been
	generated for the current SimPhy project.
- Each file will contain all the selected loci.

		''',\
			epilog="Version {0}.{1}.{2} (Still under development)".format(VERSION,MIN_VERSION,FIX_VERSION),\
			add_help=False
		)
	requiredGroup= parser.add_argument_group('Required arguments')
	requiredGroup.add_argument('-sf','--simphy-folder',metavar='<simphy_path>', type=str,\
		help='Path of the SimPhy project folder.', required=True)
	requiredGroup.add_argument('-ip','--input-prefix', metavar='<dataset_input_prefix>', type=str,\
		help='Prefix of the filename that the selected gene trees have', required=True)
	requiredGroup.add_argument('-op','--output-prefix', metavar='<dataset_output_prefix>', type=str,\
		help='Prefix for the filename of the referene loci selected', required=True)
	requiredGroup.add_argument('-m','--method', metavar="<method_code>",type=int,\
		choices=range(0,5), default=0,\
		help="Specified method to obtain the reference loci used for the design of probes. Values range from 0-3, where: (0): Considers the outgroup sequence as the reference loci; (1): Selects a random sequence from the ingroups;(2): Selects randomly a specie and a consensus sequence of the sequences belonging to that species; (3): Generates a consensus sequences from all the sequences involved. NOTE: The higher the method number, the longer it will take to generate the reference loci. (4): Extracts one (1) sequence per species. Default: (0)",\
		required=True)
	requiredGroup.add_argument('-o','--output', metavar="<output_folder>",type=str,\
		help="Name of the folder in which the reference loci will be written. This folder will be a child of the current SimPhy project folder.",\
		required=True)


	optionalGroup= parser.add_argument_group('Optional arguments')
	optionalGroup.add_argument('-l','--log',metavar='<log_level>', type=str,\
		choices=mll.LOG_LEVEL_CHOICES, default="INFO",\
		help='Specified level of log that will be shown through the standard output. Entire log will be stored in a separate file. Values:{0}. Default: {1}. '.format(mll.LOG_LEVEL_CHOICES,mll.LOG_LEVEL_CHOICES[1]))
	informationGroup= parser.add_argument_group('Information arguments')
	informationGroup.add_argument('-v', '--version',\
		action='version',\
		version='Mating version {0}.{1}.{2}'.format(VERSION,MIN_VERSION,FIX_VERSION),\
		help="Show program's version number and exit")
	informationGroup.add_argument('-h', '--help',\
		action='store_true',\
		help="Show this help message and exit")
	try:
		tmpArgs = parser.parse_args()
	except:
		sys.stdout.write("\n----------------------------------------------------------------------\n")
		parser.print_help()
		sys.exit()
	return tmpArgs

"""
MAIN
"""
if __name__=="__main__":
	try:
		cmdArgs = handlingParameters()
		print(cmdArgs)
		prog = lrf.LociReferenceSelection(cmdArgs)
		prog.run()
	except KeyboardInterrupt:
		sys.stdout.write("{0}{1}\nInterrupted!{2}\nPlease run again for the expected outcome.\n".format(mof.BOLD,mof.DARKCYAN, mof.END))
		sys.exit()
