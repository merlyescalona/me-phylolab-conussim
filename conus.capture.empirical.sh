numJobs=$(find $HOME/mnt/phylolab/srocha_conus_capture/1_rawdata/ -name *.gz| wc -l)
projectFolders=(\
  "Alan_Lemmon-01-17-2014-Posada1"\
  "Alan_Lemmon-01-17-2014-Posada4"\
  "Alan_Lemmon-01-20-2015-PosadaGray"\
  "Alan_Lemmon-07-24-2015_PosadaA_PosadaB"\
  "Alan_Lemmon-10-20-2015-Posada50"\
  )
find $HOME/mnt/phylolab/srocha_conus_capture/1_rawdata/ -name *.gz > samples.files

for project in ${projectFolders[@]};do
  value=$(cat samples.files | grep -c $project)
  echo -e "$project\t$value"
done
<<RES
Number of files per project
Alan_Lemmon-01-17-2014-Posada1	124
Alan_Lemmon-01-17-2014-Posada4	158
Alan_Lemmon-01-20-2015-PosadaGray	198
Alan_Lemmon-07-24-2015_PosadaA_PosadaB	288
Alan_Lemmon-10-20-2015-Posada50	136
RES

Need to know number of individuals, but folders are not organized similarly

SequencingFolder/ProjectFolder/SampleFolder/

 cat samples.files | awk -F"/" '{print $12}' > Sample names

 cat samples.files | awk -F"/" '{print $12}' | uniq | wc -l
157

\/home\/uvi\/be\/mef\/mnt\/phylolab\/srocha_conus_capture\/1_rawdata

SEEDFILE=/home/uvi/be/mef/mnt/phylolab/uvibemef/conus_empirical/samples.files
echo -e "#! /bin/bash
#$ -o /home/uvi/be/mef/mnt/phylolab/uvibemef/conus_empirical/output.o
#$ -e /home/uvi/be/mef/mnt/phylolab/uvibemef/conus_empirical/output.e
#$ -N fastqc

SEED=\$(awk \"NR==\$SGE_TASK_ID\" $SEEDFILE)
OUTPUTBASE=\$(echo \$SEED | sed 's/srocha_conus_capture\/1_rawdata/uvibemef\/conus_empirical/g' | sed 's/\.fastqc\.gz//g')
echo \"\$OUTPUTBASE\"
mkdir -p \"\$OUTPUTBASE\"
fastqc \$SEED -o \$OUTPUTBASE
" > fastqc.sh

qsub -l num_proc=1,s_rt=0:30:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-$numJobs fastqc.sh
qsub -l num_proc=1,s_rt=0:30:00,s_vmem=2G,h_fsize=1G,arch=haswell -t 1-10 fastqc.sh




for line in $(cat samples.files); do
OUTPUTBASE=$(echo $line | sed 's/srocha_conus_capture\/1_rawdata/uvibemef\/conus_empirical/g' | sed 's/\.fastqc\.gz//g')
echo $OUTPUTBASE
done

$HOME/mnt/phylolab/srocha_conus_capture/1_rawdata/Alan_Lemmon-07-24-2015_PosadaA_PosadaB/Project_P0072/Sample_P0072_PG_I13021/P0072_PG_I13021_AACGAAGT_L001_R1_001.fastq.gz


fastqc $sample --format fastq -o /home/uvi/be/srp/conus_capture_inwork --threads 8 --adapters /home/uvi/be/srp/conus_capture_inwork/barcode
s.new.txt --contaminants /home/uvi/be/srp/conus_capture_inwork/contaminants.txt
con sbatch --array=1-10 ft2XXXX.sh
